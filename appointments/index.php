<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Запись на прием");

use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest();
if ($ajax) $APPLICATION->RestartBuffer();
?>
    <script>
        $(document).ready(function () {
            var flag = <?= $request->get('service') ? $request->get('service') : "false" ?>;
            var flag2 = <?= $request->get('specialist') ? json_encode($request->get('specialist')) : "false" ?>;
            var body = $('body');
            body.on('change', 'select.direction', function (event) {
                event.preventDefault();
                var elem = $(this);
                var direction = elem.find('option:selected').data('direction')
                var specialist = $('select.specialist').find('option:selected').data('specialist');

                var data = {};
                data['DIRECTION'] = direction ? direction : '';
                data['SPECIALIST'] = specialist ? specialist : '';

                if (flag2 || flag === direction) {
                    flag = direction;
                } else {
                    flag = direction;
                    $.ajax({
                        type: "POST",
                        url: window.location.href,
                        data: data,
                        beforeSend: function () {
                        },
                        success: function (data) {
                            var $option = $(data).find('select.specialist').closest('.nice-select-wrapper');
                            $('select.specialist').closest('.nice-select-wrapper').replaceWith($option)
                            $('select.specialist').niceSelect();
                            initNiceSelect();
                        }
                    });
                }
                if (flag && flag2) {
                    getAjaxAppParam(data);
                    flag = false;
                }
                appointmentCalendar(['today'], false);
            });
            body.on('change', 'select.specialist', function (event) {
                event.preventDefault();
                var elem = $(this);
                var direction = $('select.direction').find('option:selected').data('direction');
                var specialist = elem.find('option:selected').data('specialist');

                var data = {};
                data['DIRECTION'] = direction ? direction : '';
                data['SPECIALIST'] = specialist ? specialist : '';

                if (flag || flag2 === specialist) {
                    flag2 = specialist;
                } else {
                    flag2 = specialist;
                    $.ajax({
                        type: "POST",
                        url: window.location.href,
                        data: data,
                        beforeSend: function () {
                        },
                        success: function (data) {
                            var $option = $(data).find('select.direction').closest('.nice-select-wrapper');
                            $('select.direction').closest('.nice-select-wrapper').replaceWith($option)
                            $('select.direction').niceSelect();
                            initNiceSelect();
                        }
                    });
                }

                if (flag && flag2) {
                    getAjaxAppParam(data);
                    flag2 = false;
                }
                appointmentCalendar(['today'], false);
            });
            //Сброс инпутов в исходное положение
            body.on('click', '.step-form__reset-btn', function (event) {
                event.preventDefault();
                resetAppointmentCalendar();
                flag = flag2 = false;
                if (window.location.href.indexOf("?") > -1) {
                    window.history.pushState("{}", "", "/appointments/?steps=2");
                }
                var data = {};
                $.ajax({
                    type: "POST",
                    url: window.location.href,
                    data: data,
                    beforeSend: function () {
                    },
                    success: function (data) {
                        var $optionOne = $(data).find('select.direction').closest('.nice-select-wrapper');
                        $('select.direction').closest('.nice-select-wrapper').replaceWith($optionOne)
                        $('select.direction').niceSelect();

                        var $optionTwo = $(data).find('select.specialist').closest('.nice-select-wrapper');
                        $('select.specialist').closest('.nice-select-wrapper').replaceWith($optionTwo)
                        $('select.specialist').niceSelect();
                        initNiceSelect();
                    }
                });
            });
            //Ближайший приём
            body.on('click', 'a.nearest', function (event) {
                event.preventDefault();
                var elem = $(this);
                var date = elem.data('date');
                var time = elem.data('time');
                if (date.value !== '') {

                    $('input.appointment-datepicker.flatpickr-input').val(date);
                    $('input.datepicker-item__btn.flatpickr-input').val(date);
                    $('.available-time__items').html('<div class="time-item active">' + time + '</div>');

                    $('select.custom-select.time').html('<option value="' + time + '" selected>' + time + '</option>');
                    initTimeCustomSelect();

                    getDataFromForm();
                    $('.form-controls__link').removeClass('active');
                    $('.form-controls__link.step-2').addClass('active');
                    setFormActiveStep(1, true);
                    toggleStepFormHeight();
                }
            });
        });

        function getAjaxAppParam(data) {
            $.ajax({
                type: "POST",
                url: '/local/ajax/getShedule.php',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showFormLoader();
                    var timeItem = '';
                    var timeItemOption = '';
                    var time = [
                        "07:30",
                        "08:00",
                        "08:30",
                        "09:00",
                        "09:30",
                        "10:00",
                        "10:30",
                        "11:00",
                        "11:30",
                        "12:00",
                        "12:30",
                        "13:00",
                        "13:30",
                        "14:00",
                        "14:30",
                        "15:00",
                        "15:30",
                        "16:00",
                        "16:30",
                        "17:00",
                        "17:30",
                        "18:00",
                        "18:30",
                        "19:00",
                        "19:30",
                        "20:00"
                    ]
                    $.each(time, function (index, value) {
                        timeItem += '<div class="time-item">' + value + '</div>';
                        timeItemOption += '<option value="' + value + '">' + value + '</option>';
                    });
                    $('.available-time__items').html(timeItem);
                    $('.available-time').addClass('disabled');
                    $('select.custom-select.time').closest('.timepicker-item').addClass('disabled');

                },
                success: function (res) {
                    if (res['status'] === 'ok') {

                        //Ближайший приём
                        $('a.nearest').attr('data-date', res['result']['date'][0]).attr('data-time', res['result']['time_nearest']).removeClass('disabled');

                        resetDateAndTime();
                        if (res['result']['date'][0]) {
                            appointmentCalendar(Object.values(res['result']['date']), true);
                        } else {
                            appointmentCalendar(['today'], false);
                        }
                        calendarInstance.config.onChange.push(function (selectedDates, dateStr, instance) {
                            data['DATE'] = dateStr + 'T00:00:00';
                            $.ajax({
                                type: "POST",
                                url: '/local/ajax/getShedule.php',
                                data: data,
                                dataType: 'json',
                                beforeSend: function () {
                                    $('.available-time').addClass('disabled');
                                    $('select.custom-select.time').closest('.timepicker-item').addClass('disabled');
                                },
                                success: function (result) {
                                    if (result['status'] === 'ok') {
                                        var timeItem = '';
                                        var timeItemOption = '';
                                        $.each(result['result']['time'], function (index, value) {
                                            timeItem += '<div class="time-item">' + value + '</div>';
                                            timeItemOption += '<option value="' + value + '">' + value + '</option>';
                                        });
                                        $('.available-time__items').html(timeItem);
                                        $('.available-time').removeClass('disabled');

                                        $('select.custom-select.time').html(timeItemOption).closest('.timepicker-item').removeClass('disabled');
                                        timeCustomSelect.html(timeItemOption).trigger('change');
                                    }
                                }
                            });
                        });
                    }
                    hideFormLoader();
                }
            });
        }
    </script>
<?
//Получим направления
$arServices = [];
$arFilterServices = [
    'IBLOCK_ID' => \IL\Settings::SERVICES_IBLOCK_ID,
    "ACTIVE" => "Y",
    "DEPTH_LEVEL" => "1",
];
$arSelectServices = [
    'NAME',
    'ID',
    'XML_ID',
];
\CModule::IncludeModule("iblock");
$rsSectServices = \CIBlockSection::GetList([], $arFilterServices, true, $arSelectServices, false);
while ($arSectServices = $rsSectServices->GetNext()) {
    $arServices[] = $arSectServices;
}
//Получим специалистов
$arSpecialist = \IL\Catalog::getElementList(\IL\Settings::SPECIALISTS_IBLOCK_ID, [
    'PROPERTY_DIRECTION' => $request->getPost('DIRECTION') ? $request->getPost('DIRECTION') : $request->get('service'),
]);
?>
    <div class="row">
        <? if ($request->get('service') || $request->get('specialist') || $request->get('steps') || $ajax): ?>
            <div class="col col-8 col-xl-9 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form class="step-form" action="<?= SITE_AJAX_PATH ?>/form_record.php" method="POST" enctype="multipart/form-data">
                    <div class="step-form__tab-content active">
                        <div class="heading">
                            <div class="heading__inner">
                                <div class="heading__col">
                                    <p class="heading__text heading__step">Шаг 1</p>
                                    <p class="heading__text">Воспользуйтесь фильтрами</p>
                                </div>
                                <div class="heading__col hidden-xs-down">
                                    <div class="step-form__reset-btn">
                                        <svg class="icon__close" width="14" height="14">
                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                        </svg>
                                        <span>Изменить данные</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step-form__main-row">
                            <div class="step-form__unique-grid-row grid grid_two-col">
                                <div class="grid__item">
                                    <div class="grid grid_two-col">
                                        <div class="nice-select-wrapper nice-select-wrapper_activities<?= !empty($request->get('service')) ? ' active' : '' ?>">
                                            <div class="nice-select-wrapper__placeholder">Направления деятельности</div>
                                            <?
                                            if (!empty($request->getPost('SPECIALIST')) || !empty($request->get('specialist'))) {
                                                $arServices = current(\IL\Catalog::getElementList(\IL\Settings::SPECIALISTS_IBLOCK_ID, [
                                                    'XML_ID' => $request->getPost('SPECIALIST') ? $request->getPost('SPECIALIST') : $request->get('specialist'),
                                                    'nPageSize' => 1,
                                                ]))['PROPERTIES']['DIRECTION']['VALUE'];
                                                $arServices = \IL\Catalog::getSectionsByID(\IL\Settings::SERVICES_IBLOCK_ID, $arServices);
                                            }
                                            ?>
                                            <select class="direction nice-select" name="direction" required>
                                                <option value=""></option>
                                                <? foreach ($arServices as $serviceItem): ?>
                                                    <option data-direction="<?= $serviceItem['ID'] ?>"
                                                            value="<?= $serviceItem['NAME'] ?>"
                                                        <?= $request->get('service') === $serviceItem['ID'] ? " selected" : "" ?>
                                                    >
                                                        <?= $serviceItem['NAME'] ?>
                                                    </option>
                                                <? endforeach ?>
                                            </select>
                                        </div>
                                        <div class="nice-select-wrapper nice-select-wrapper_specialists<?= !empty($request->get('specialist')) ? ' active' : '' ?>">
                                            <div class="nice-select-wrapper__placeholder">Выбрать специалиста</div>
                                            <select class="specialist nice-select" name="specialist" required>
                                                <option value=""></option>
                                                <? foreach ($arSpecialist as $specialistItem): ?>
                                                    <? $arService = \IL\Catalog::getSectionsByID($specialistItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $specialistItem['PROPERTIES']['SERVICE']['VALUE']); ?>
                                                    <?
                                                    $arServicesName = implode(', ', array_map(function ($value) {
                                                        return $value['NAME'];
                                                    }, $arService));
                                                    ?>
                                                    <option data-specialist="<?= $specialistItem['XML_ID'] ?>"
                                                            data-service="<?= $arServicesName ?>"
                                                            data-photo="<?= CFile::GetPath($specialistItem['PREVIEW_PICTURE']) ?>"
                                                            value="<?= $specialistItem['XML_ID'] ?>"
                                                        <?= $request->get('specialist') === $specialistItem['XML_ID'] ? " selected" : "" ?>
                                                    >
                                                        <?= $specialistItem['NAME'] ?>
                                                    </option>
                                                <? endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid__item">
                                    <div class="step-form__unique-btn-wrapper">
                                        <a class="btn btn_light nearest"
                                           href="javascript:void(0);"
                                           data-date=""
                                           data-time=""
                                        >
                                            <span>Ближайший прием</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="grid grid_two-col">
                                <div class="grid__item">
                                    <div class="appointment-datepicker-wrapper disabled hidden-xs-down">
                                        <div class="appointment-datepicker-date"></div>
                                        <input class="appointment-datepicker" type="text" name="visit-date[]" required>
                                    </div>
                                    <div class="datepicker-item disabled hidden-sm-up">
                                        <div class="custom-picker select-item">
                                            <div class="custom-picker__placeholder">Дата посещения</div>
                                            <select class="custom-select date"></select>
                                        </div>
                                        <input class="datepicker-item__btn" type="date" name="visit-date[]">
                                    </div>
                                </div>
                                <div class="grid__item">
                                    <div class="available-time disabled hidden-xs-down">
                                        <div class="available-time__items">
                                            <?
                                            $start = '07:30';
                                            $end = '20:00';
                                            $st = strtotime($start);
                                            $et = strtotime($end);
                                            echo '<div class="time-item">' . date("H:i", $st) . '</div>';
                                            while ($st < $et) {
                                                $t = strtotime("+30 minute", $st);
                                                echo '<div class="time-item">' . date("H:i", $t) . '</div>';
                                                $st = $t;
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="timepicker-item disabled hidden-sm-up">
                                        <div class="custom-picker select-item">
                                            <div class="custom-picker__placeholder">Время посещения</div>
                                            <select class="custom-select time" name="visit-time"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step-form__bottom-row">
                            <span class="btn btn_primary disabled">
                                <span>Указать контакты</span>
                            </span>
                        </div>
                    </div>
                    <div class="step-form__tab-content">
                        <div class="heading">
                            <div class="heading__inner">
                                <div class="heading__col">
                                    <p class="heading__text heading__step">Шаг 2</p>
                                    <p class="heading__text">Заполните контактные данные</p>
                                </div>
                                <div class="heading__col hidden-xs-down">
                                    <div class="step-form__reset-btn">
                                        <svg class="icon__close" width="14" height="14">
                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                        </svg>
                                        <span>Изменить данные</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step-form__main-row">
                            <div class="grid grid_two-col">
                                <div class="grid__item">
                                    <div class="step-form__short-col">
                                        <div class="default-form__input-wrapper default-form__input-wrapper_surname">
                                            <input class="default-form__input" type="text" name="surname" required>
                                            <span class="default-form__input-placeholder">Ваша фамилия *</span>
                                            <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                                            </svg>
                                        </div>
                                        <div class="default-form__input-wrapper default-form__input-wrapper_name">
                                            <input class="default-form__input" type="text" name="name" required>
                                            <span class="default-form__input-placeholder">Ваше имя *</span>
                                            <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                                            </svg>
                                        </div>
                                        <div class="default-form__input-wrapper default-form__input-wrapper_phone">
                                            <input class="default-form__input" type="tel" name="phone" required>
                                            <span class="default-form__input-placeholder">Ваш телефон *</span>
                                            <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                                            </svg>
                                        </div>
                                        <div class="step-form__toggle">
                                            <p class="step-form__toggle-title">Это ваш первый визит клиники?</p>
                                            <div class="grid grid_two-col">
                                                <label class="checkbox-item">
                                                    <input type="radio" name="first-visit" required>
                                                    <span class="input-mask"></span>
                                                    <span class="checkbox-item__text">Да</span>
                                                </label>
                                                <label class="checkbox-item">
                                                    <input type="radio" name="first-visit" required checked>
                                                    <span class="input-mask"></span>
                                                    <span class="checkbox-item__text">Нет</span>
                                                </label>
                                            </div>
                                        </div>
                                        <p class="step-form__short-text">Вам необходимо прийти в клинику за 10 минут до приема с паспортом для регистрации</p>
                                    </div>
                                </div>
                                <div class="grid__item">
                                    <div class="step-form__card">
                                        <div class="specialist-item specialist-item_simple item">
                                            <div class="specialist-item__img-wrapper">
                                                <!-- resize-->
                                                <img class="specialist-item__img" src="" alt="">
                                            </div>
                                            <div class="specialist-item__content">
                                                <div class="specialist-item__row main">
                                                    <div class="specialist-item__row-inner">
                                                        <div class="specialist-item__col">
                                                            <div class="specialist-item__name"></div>
                                                            <div class="specialist-item__position"></div>
                                                        </div>
                                                    </div>
                                                    <ul class="specialist-item__list">
                                                        <li class="specialist-item__list-item">
                                                            <span class="specialist-item__list-property">Дата и время:</span>
                                                            <span class="specialist-item__list-value specialist-item__datetime-value"></span>
                                                        </li>
                                                        <li class="specialist-item__list-item">
                                                            <span class="specialist-item__list-property">Услуга:</span>
                                                            <span class="specialist-item__list-value specialist-item__activity-value"></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="specialist-item__row bottom">
                                                    <ul class="specialist-item__list">
                                                        <li class="specialist-item__list-item">
                                                            <span class="specialist-item__list-property">Адрес:</span>
                                                            <span class="specialist-item__list-value specialist-item__address-value"></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step-form__bottom-row">
                            <button class="btn btn_primary disabled">
                                <span>Подтвердить запись</span>
                            </button>
                        </div>
                    </div>
                    <div class="step-form__tab-content">
                        <div class="heading">
                            <div class="heading__inner">
                                <div class="heading__col">
                                    <p class="heading__text">Вы успешно записались на прием</p>
                                </div>
                            </div>
                        </div>
                        <div class="step-form__main-row">
                            <div class="grid grid_two-col">
                                <div class="grid__item d-flex ai-center">
                                    <div class="success-item">
                                        <div class="success-item__icon-wrapper">
                                            <div class="success-item__icon-bg bg-linen"></div>
                                            <svg class="icon__patient-reviews" width="73px" height="73px">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#patient-reviews"></use>
                                            </svg>
                                        </div>
                                        <p class="success-item__description">Если вы не сможете посетить специалиста, пожалуйста предупредите нас об этом</p>
                                        <ul class="success-item__list">
                                            <li class="success-item__list-item">
                                                <a class="success-item__list-link"
                                                   href="tel:<?= preg_replace('~\D+~', '', \Bitrix\Main\Config\Option::get("askaron.settings", "UF_CALL_CENTER")) ?>">
                                                    <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_CALL_CENTER"); ?>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="grid__item">
                                    <div class="specialist-item specialist-item_simple item">
                                        <div class="specialist-item__img-wrapper">
                                            <!-- resize-->
                                            <img class="specialist-item__img" src="" alt="">
                                        </div>
                                        <div class="specialist-item__content">
                                            <div class="specialist-item__row main">
                                                <div class="specialist-item__row-inner">
                                                    <div class="specialist-item__col">
                                                        <div class="specialist-item__name"></div>
                                                        <div class="specialist-item__position"></div>
                                                    </div>
                                                </div>
                                                <ul class="specialist-item__list">
                                                    <li class="specialist-item__list-item">
                                                        <span class="specialist-item__list-property">Дата и время:</span>
                                                        <span class="specialist-item__list-value specialist-item__datetime-value"></span>
                                                    </li>
                                                    <li class="specialist-item__list-item">
                                                        <span class="specialist-item__list-property">Услуга:</span>
                                                        <span class="specialist-item__list-value specialist-item__activity-value"></span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="specialist-item__row bottom">
                                                <ul class="specialist-item__list">
                                                    <li class="specialist-item__list-item">
                                                        <span class="specialist-item__list-property">Адрес:</span>
                                                        <span class="specialist-item__list-value specialist-item__address-value"></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step-form__bottom-row">
                            <a class="btn btn_primary" href="/">
                                <span>Перейти на главную</span>
                            </a>
                        </div>
                    </div>
                    <? $arAddress = current(\IL\Catalog::getElementList(\IL\Settings::CONTACTS_IBLOCK_ID))['PROPERTIES']['ADDRESS']['VALUE']; ?>
                    <input type="hidden" name="address" value="<?= $arAddress ?>">
                    <input type="hidden" name="utm_source" value="<?= $_GET['utm_source'] ?>">
                    <input type="hidden" name="utm_medium" value="<?= $_GET['utm_medium'] ?>">
                    <input type="hidden" name="utm_campaign" value="<?= $_GET['utm_campaign'] ?>">
                    <input type="hidden" name="utm_content" value="<?= $_GET['utm_content'] ?>">
                    <input type="hidden" name="utm_term" value="<?= $_GET['utm_term'] ?>">
                    <div class="loader-wrapper loader-wrapper_full">
                        <svg class="loader" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 199.998 96.615" enable-background="new 0 0 199.998 96.615">
                            <g>
                                <path fill="none" stroke="#E84849" stroke-width="2.5" stroke-linecap="round" d="M198.746,59.841                                              h-14.537c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371                                              c-0.33,1.036-1.843,0.876-1.948-0.206l-3.747-53.409c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31                                              c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993                                              c-0.031,0.074-0.104,0.122-0.184,0.122h-16.937l16.904-0.02h-32.537c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522                                              c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371c-0.33,1.036-1.843,0.876-1.948-0.206l-3.747-53.409                                              c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504                                              c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993c-0.031,0.074-0.104,0.122-0.184,0.122H68.452l16.182-0.02H52.096                                              c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371                                              c-0.33,1.036-1.843,0.876-1.948-0.206L37.114,2.938c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31                                              c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993                                              c-0.031,0.074-0.104,0.122-0.184,0.122H2.034"></path>
                            </g>
                        </svg>
                    </div>
                </form>
            </div>
            <div class="col col-3 col-xl-3 col-lg-12 col-md-12 hidden-sm-down offset-1 offset-xl-0 offset-lg-0 offset-md-0">
                <div class="modal-card-wrapper">
                    <div class="modal-card">
                        <div class="modal-card__icon-wrapper">
                            <div class="modal-card__icon-bg bg-linen"></div>
                            <svg class="icon__feedback" width="90px" height="90px">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#feedback"></use>
                            </svg>
                        </div>
                        <div class="modal-card__title">Не знаете что вам нужно, оставьте только свои контакты</div>
                        <a class="btn btn_primary modal-open" href="#callback">
                            <span>Обратный звонок</span>
                        </a>
                    </div>
                </div>
            </div>
            <? $APPLICATION->AddViewContent('popup_fixed', '
                <div class="modal-card-wrapper hidden-md-up">
                    <div class="form-controls">
                        <div class="form-controls__titles">
                            <div class="form-controls__title form-controls__step step-1 active">После выбора даты и времени</div>
                            <div class="form-controls__title form-controls__step step-2">После заполнения контактной информации</div>
                            <div class="form-controls__title form-controls__step step-3">Вы успешно записались</div>
                        </div>
                        <div class="form-controls__btns">
                            <a class="form-controls__link form-controls__step step-1 active disabled" href="javascript:void(0);">Указать контакты</a>
                            <a class="form-controls__link form-controls__step step-2 disabled" href="javascript:void(0);">Подтвердить запись</a>
                            <a class="form-controls__link form-controls__step step-3 disabled" href="/">Перейти на главную</a>
                        </div>
                    </div>
                </div>
            '); ?>
        <? else: ?>
            <div class="col col-8 col-xl-9 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="content-container">
                    <div class="heading scrl fadeInUp">
                        <div class="heading__inner">
                            <div class="heading__col">
                                <p class="heading__text">Выберите
                                    <a class='anchor-link' href='#activities'>услугу</a>
                                    или
                                    <a class='anchor-link' href='#specialists'>специалиста</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <section class="section s-activities-grid mb-less" id="activities">
                        <div class="section__top-row">
                            <div class="section__top-row-inner scrl fadeInUp">
                                <h3>Направления деятельности</h3>
                            </div>
                        </div>
                        <div class="grid grid_three-col scrl fadeInUp">
                            <? foreach ($arServices as $serviceItem): ?>
                                <div class="activity-item">
                                    <div class="activity-item__title">
                                        <?= $serviceItem['NAME'] ?>
                                    </div>
                                    <div class="activity-item__bottom-row">
                                        <a class="arrow-link" href="?service=<?= $serviceItem['ID'] ?>">
                                            <span class="arrow-link__title">Запись на прием</span>
                                        </a>
                                    </div>
                                </div>
                            <? endforeach ?>
                        </div>
                        <a class="btn btn_primary" href="javascript:void(0);">
                            <span>Показать еще</span>
                        </a>
                    </section>
                    <section class="section s-specialists-grid mb-less" id="specialists">
                        <div class="section__top-row">
                            <div class="section__top-row-inner scrl fadeInUp">
                                <h3>Специалисты</h3>
                            </div>
                        </div>
                        <div class="grid grid_two-col scrl fadeInUp">
                            <? foreach ($arSpecialist as $specialistItem): ?>
                                <?
                                $arService = \IL\Catalog::getSectionNameByID($specialistItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $specialistItem['PROPERTIES']['SERVICE']['VALUE'][0]);
                                $arReviews = count(\IL\Catalog::getElementList(\IL\Settings::REVIEWS_IBLOCK_ID, ['PROPERTY_SPECIALIST' => $specialistItem['ID']]));
                                ?>
                                <div class="specialist-item item">
                                    <a class="specialist-item__img-wrapper" href="<?= $specialistItem['DETAIL_PAGE_URL'] ?>">
                                        <? $thumb = CFile::ResizeImageGet($specialistItem['PREVIEW_PICTURE'], [
                                            'width' => 200,
                                            'height' => 200,
                                        ], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []); ?>
                                        <img class="specialist-item__img lazy" data-src="<?= $thumb['src'] ?>" alt="<?= $specialistItem['NAME'] ?>"/>
                                    </a>
                                    <div class="specialist-item__direction"></div>
                                    <div class="specialist-item__content">
                                        <div class="specialist-item__row top">
                                            <div class="specialist-item__row-inner">
                                                <div class="specialist-item__col unique">
                                                    <div class="specialist-item__category"><?= $arService ?></div>
                                                    <? if (count($specialistItem['PROPERTIES']['SERVICE']['VALUE']) > 1): ?>
                                                        <button class="show-more">
                                                            <div class="show-more__btn">
                                                                <svg class="three-dot" width="2" height="10">
                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#three-dot"></use>
                                                                </svg>
                                                                <span>Еще</span>
                                                            </div>
                                                            <div class="show-more__list" data-simplebar="data-simplebar">
                                                                <? foreach ($specialistItem['PROPERTIES']['SERVICE']['VALUE'] as $keyService => $arService): ?>
                                                                    <? if ($keyService == 0) continue ?>
                                                                    <? $arServiceName = \IL\Catalog::getSectionNameByID($specialistItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arService); ?>
                                                                    <div class="show-more__list-item">
                                                                        <span><?= $arServiceName ?></span>
                                                                    </div>
                                                                <? endforeach ?>
                                                            </div>
                                                        </button>
                                                    <? endif ?>
                                                </div>
                                                <? if ($arReviews > 0): ?>
                                                    <div class="specialist-item__col">
                                                        <a class="specialist-item__link" href="<?= $specialistItem['DETAIL_PAGE_URL'] ?>#reviews">
                                                            Отзывы: <?= $arReviews ?>
                                                        </a>
                                                    </div>
                                                <? endif ?>
                                            </div>
                                        </div>
                                        <div class="specialist-item__row main">
                                            <div class="specialist-item__row-inner">
                                                <div class="specialist-item__col">
                                                    <a class="specialist-item__name" href="<?= $specialistItem['DETAIL_PAGE_URL'] ?>">
                                                        <?= $specialistItem['NAME'] ?>
                                                    </a>
                                                    <div class="specialist-item__position">
                                                        <?= !empty($specialistItem['PROPERTIES']['PROFESSION']['VALUE']) ? $specialistItem['PROPERTIES']['PROFESSION']['VALUE'] : '&nbsp;' ?>
                                                    </div>
                                                </div>
                                                <div class="specialist-item__col">
                                                    <? if (!empty($specialistItem['PROPERTIES']['YOUTUBE']['VALUE'])): ?>
                                                        <a class="specialist-item__play specialist-video-open" href="javascript:void(0)" data-modal-class="specialist-video-modal" data-type="center">
                                                            <div class="specialist-item__play-icon-wrapper">
                                                                <svg class="play" width="10" height="12">
                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#play"></use>
                                                                </svg>
                                                            </div>
                                                            <span class="specialist-item__play-label">Видео о специалисте</span>
                                                        </a>
                                                        <div class="specialist-video-modal modal__inner modal_center mfp-hide">
                                                            <button class="modal__close-btn js-modal-close">
                                                                <svg class="icon__close" width="20" height="20">
                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                                </svg>
                                                            </button>
                                                            <div class="modal__content">
                                                                <iframe class="modal__video" src="https://www.youtube.com/embed/<?= $specialistItem['PROPERTIES']['YOUTUBE']['VALUE'] ?>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen"></iframe>
                                                            </div>
                                                        </div>
                                                    <? endif ?>
                                                </div>
                                            </div>
                                            <? if (!empty($specialistItem['PROPERTIES']['DEGREE']['VALUE']) || !empty($specialistItem['PROPERTIES']['STANDING']['VALUE'])): ?>
                                                <ul class="specialist-item__list">
                                                    <? if (!empty($specialistItem['PROPERTIES']['DEGREE']['VALUE'])): ?>
                                                        <li class="specialist-item__list-item">
                                                            <span class="specialist-item__list-property">
                                                                <?= $specialistItem['PROPERTIES']['DEGREE']['NAME'] ?>:
                                                            </span>
                                                            <span class="specialist-item__list-value">
                                                                <?= $specialistItem['PROPERTIES']['DEGREE']['VALUE'] ?>
                                                            </span>
                                                        </li>
                                                    <? endif ?>
                                                    <? if (!empty($specialistItem['PROPERTIES']['STANDING']['VALUE'])): ?>
                                                        <li class="specialist-item__list-item">
                                                            <span class="specialist-item__list-property">
                                                                <?= $specialistItem['PROPERTIES']['STANDING']['NAME'] ?>:
                                                            </span>
                                                            <span class="specialist-item__list-value">
                                                                <?= $specialistItem['PROPERTIES']['STANDING']['VALUE'] ?>
                                                            </span>
                                                        </li>
                                                    <? endif ?>
                                                </ul>
                                            <? endif ?>
                                        </div>
                                        <div class="specialist-item__row bottom">
                                            <a class="arrow-link" href="?specialist=<?= $specialistItem['XML_ID'] ?>">
                                                <span class="arrow-link__title">Запись на прием</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach ?>
                        </div>
                        <a class="btn btn_primary" href="javascript:void(0);">
                            <span>Показать еще</span>
                        </a>
                    </section>
                </div>
            </div>
            <div class="col col-3 col-xl-3 col-lg-12 col-md-12 hidden-sm-down offset-1 offset-xl-0 offset-lg-0 offset-md-0 offset-sm-0 offset-xs-0">
                <div class="modal-card-wrapper">
                    <div class="modal-card">
                        <div class="modal-card__icon-wrapper">
                            <div class="modal-card__icon-bg bg-linen"></div>
                            <svg class="icon__feedback" width="90px" height="90px">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#feedback"></use>
                            </svg>
                        </div>
                        <div class="modal-card__title">После заполнения контактной информации</div>
                        <a class="btn btn_primary modal-open" href="#callback">
                            <span>Обратный звонок</span>
                        </a>
                    </div>
                </div>
            </div>
            <? $APPLICATION->AddViewContent('popup_fixed', '
                <div class="modal-card-wrapper hidden-md-up">
                    <div class="modal-card">
                        <div class="modal-card__icon-wrapper">
                            <div class="modal-card__icon-bg bg-linen"></div>
                            <svg class="icon__feedback" width="90px" height="90px">
                                <use xlink:href="' . SITE_STYLE_PATH . '/img/general/svg-symbols.svg#feedback"></use>
                            </svg>
                        </div>
                        <div class="modal-card__title">Не знаете что вам нужно, оставьте свои контакты</div>
                        <a class="btn btn_primary modal-open" href="#callback">
                            <span>Обратный звонок</span>
                        </a>
                    </div>
                </div>
            '); ?>
        <? endif ?>
    </div>

<? $APPLICATION->AddViewContent('header_desc', '
    <div class="hidden-md-up">
        <p class="page-heading__text">Не знаете что вам нужно, оставьте только свои контакты</p>
        <a class="arrow-link modal-open" href="#callback">
            <span class="arrow-link__title">Обратный звонок</span>
        </a>
    </div>
'); ?>


<? if ($ajax) die;
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>