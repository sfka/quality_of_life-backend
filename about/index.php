<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О клинике");
?>
<section class="section s-company-promo scrl fadeInUp">
    <div class="row">
        <div class="col col-4 col-xs-12">
            <div class="s-company-promo__column">
                <div class="s-company-promo__main-info">
                    <div class="s-company-promo__logo">
                        <svg class="logo-path-colored" width="100" height="76">
                            <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#logo-path-colored"></use>
                        </svg>
                        <div class="s-company-promo__logo-title"><?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_SITE_NAME") ?></div>
                    </div>
                    <p class="s-company-promo__description"><?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_SITE_TITLE"); ?></p>
                </div>
            </div>
        </div>
        <div class="col col-8 col-xs-12">
            <div class="s-company-promo__column">
                <div class="s-company-promo__img-wrapper">
                    <? $img = CFile::ResizeImageGet( \Bitrix\Main\Config\Option::get("askaron.settings", "UF_CLINIC_IMG"), ['width' => 600, 'height' => 600], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []) ?>
                    <img class="s-company-promo__img s-company-promo__background lazy" data-src="<?=$img['src'] ?>" alt="">
                    <img class="s-company-promo__img s-company-promo__foreground lazy" data-src="<?=$img['src'] ?>" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section s-promo-info scrl fadeInUp">
    <div class="row">
        <div class="col col-10 col-sm-12 col-xs-12 offset-1 offset-sm-0 offset-xs-0">
            <div class="content-container">
                <div class="important-info">
                    <div class="important-info__inner">
                        <div class="important-info__title">Миссия клиники</div>
                        <p class="important-info__description"><?$APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/content/about_mission.php') ?></p>
                    </div>
                </div>
                <div class="s-promo-info__paragraph-wrapper">
                    <div class="item">
                        <div class="item__text-wrapper">
                            <p class="item__text"><?$APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/content/about_text.php') ?></p><span class="item__show-all-text c-carmine-pink"><span>Показать полностью</span><span>Свернуть</span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<? // подключение слайдера направлений
$APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "sections_slider",
    array(
        "ADD_SECTIONS_CHAIN" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "COUNT_ELEMENTS" => "N",
        "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
        "FILTER_NAME" => "sectionsFilter",
        "IBLOCK_ID" => "3",
        "IBLOCK_TYPE" => "services",
        "SECTION_CODE" => "",
        "SECTION_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "SECTION_ID" => "",
        "SECTION_URL" => "",
        "SECTION_USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "SHOW_PARENT_NAME" => "Y",
        "TOP_DEPTH" => "1",
        "VIEW_MODE" => "LINE",
        "WRAP_TITLE" => "Направления деятельности",
        "COMPONENT_TEMPLATE" => "sections_slider"
    ),
    false
);?>
<? // подключение блока ценности
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "tabs_value",
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => "NAME",
            1 => "PREVIEW_TEXT",
            2 => "PREVIEW_PICTURE",
            3 => "",
        ),
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "20",
        "IBLOCK_TYPE" => "widgets",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "6",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N",
        "COMPONENT_TEMPLATE" => "tabs_value"
    ),
    false
);?>
<?// подключение блока специалистов ?>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/main/experts_main.php", ['IBLOCK_ID' => 4], ["SHOW_BORDER" => false]) ?>
<?// подключение вакансий
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "vacancies",
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => "NAME",
            1 => "",
        ),
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "21",
        "IBLOCK_TYPE" => "widgets",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "6",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "EXPERIENCE",
            1 => "REQUIREMENTS",
            2 => "DUTIES",
            3 => "TERMS",
            4 => "",
        ),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ID",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N",
        "COMPONENT_TEMPLATE" => "vacancies"
    ),
    false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>