$(document).ready(function () {
    initPreloader();
    initForms();
    initAjaxFilter();
    initAjaxPagen();
    initAjaxFilterSlider();
    scrollToAnchor();
});

//Ajax-фильтрация в слайдере
function initAjaxFilterSlider() {
    var body = $('body');
    body.on('click', '[data-filter-slider]', function (event) {
        event.preventDefault();
        var elem = $(this);

        if (elem.hasClass('active')) return false;

        var section = elem.attr('data-filter-slider');
        var block_id = elem.closest('section');

        $(block_id).fadeTo(200, 0.1);

        var data = {};
        if ($(block_id).hasClass('s-before-after-slider')) {
            data['BEFORE_AFTER'] = 'Y';
        } else if ($(block_id).hasClass('s-reviews-slider')) {
            data['REVIEWS'] = 'Y';
        } else if ($(block_id).hasClass('section-price__acordeon')) {
            data['PRICE_ACC'] = 'Y';
        }
        data['SECTION'] = section ? section : '';
        $.ajax({
            type: "GET",
            url: window.location.href,
            data: data,
            beforeSend: function () {
            },
            success: function (data) {
                var $text;
                if ($(block_id).hasClass('s-before-after-slider')) {
                    $text = $(data).find('.s-before-after-slider');
                } else if ($(block_id).hasClass('s-reviews-slider')) {
                    $text = $(data).find('.s-reviews-slider');
                } else if ($(block_id).hasClass('section-price__acordeon')) {
                    $text = $(data).find('.section-price__acordeon');
                }
                $(block_id).replaceWith($text);
                //$(block_id).replaceWith(data);
                $(block_id).fadeTo(200, 1);

                if ($(block_id).hasClass('s-before-after-slider')) {
                    initBeforeAfterSlider();
                    $('.s-before-after-slider').addClass('show');
                } else if ($(block_id).hasClass('s-reviews-slider')) {
                    $('.s-reviews-slider').addClass('show');
                    initReviewsSlider();
                } else if ($(block_id).hasClass('section-price__acordeon')) {
                    $('.section-price__acordeon').addClass('show');
                    catalogItemToggle();
                }

                dynamicModal();
                initImgLazyLoad();
            }
        });
    });
}

//Ajax-пагинация
function initAjaxPagen() {
    var load_more = false;
    var winSize;

    var calcWinSize = function calcWinSize() {
        return winSize = {
            width: window.innerWidth,
            height: window.innerHeight
        };
    };
    calcWinSize();

    window.addEventListener('resize', calcWinSize);

    window.addEventListener('scroll', function () {
        var btn = document.querySelector('[data-show-more]');
        if (btn && !load_more) {

            var page = btn.getAttribute('data-next-page');
            var id = btn.getAttribute('data-show-more');
            var block_id = ".ajax_list";

            var direction = $('.sidebar__tags-item.active').attr('data-direction');
            var service = $('.category-items__item.active').attr('data-service');
            var section = $('.sidebar__tags-item.active').attr('data-section');

            var data = {};
            data['PAGEN_' + id] = page;

            data['DIRECTION'] = direction ? direction : '';
            data['SERVICE'] = service ? service : '';
            data['SECTION'] = section ? section : '';

            if (winSize.height - btn.getBoundingClientRect().top > 0) {
                load_more = true;
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    data: data,
                    beforeSend: function () {
                        /*preloader*/
                        $('.loader-wrapper').css('opacity', '1');
                    },
                    success: function (data) {
                        $('.show_more').remove();
                        var $butt = $(data).find('.show_more');
                        $('.dynamic-content-wrapper').after($butt);

                        var elem = $(data).find('.ajax_item');
                        $(block_id).append(elem);
                        load_more = false;

                        answerOpen();
                        dynamicModal();
                        initImgLazyLoad();
                        initTitleDots();
                    }
                });
            }
        }
    });
}

//Ajax-фильтрация
function initAjaxFilter() {
    var body = $('body');
    body.on('click', '.sidebar__tags-item, .category-items__item:not([data-filter-slider])', function (event) {
        event.preventDefault();
        var elem = $(this);
        getAjaxParam(elem);
    });
    body.on('change', '.ajax__filter', function (event) {
        event.preventDefault();
        var elem = $(this).find(":selected");
        getAjaxParam(elem);
    });
}

//Ajax-параметры для фильтрации
function getAjaxParam(elem) {

    if (elem.hasClass('active')) return false;

    var direction = elem.attr('data-direction');
    var service = elem.attr('data-service');
    var section = elem.attr('data-section');

    var block_id = ".dynamic-content-wrapper";
    $(block_id).fadeTo(200, 0.1);

    var data = {};
    data['DIRECTION'] = direction ? direction : '';
    data['SERVICE'] = service ? service : '';
    data['SECTION'] = section ? section : '';

    $('.sidebar__tags-item, .category-items__item').removeClass('active');
    $('.sidebar__tags-item[data-direction=' + direction + ']').addClass('active');
    $('.sidebar__tags-item[data-section=' + section + ']').addClass('active');

    $.ajax({
        type: "GET",
        url: window.location.href,
        data: data,
        beforeSend: function () {
        },
        success: function (data) {
            var $text = $(data).find('.dynamic-content');
            $(block_id).html($text);
            $(block_id).fadeTo(200, 1);

            $('.show_more').remove();
            var $butt = $(data).find('.show_more');
            $('.dynamic-content-wrapper').append($butt);

            catalogItemToggle();
            dynamicModal();
            initImgLazyLoad();
            initTitleDots();
            answerOpen()
        }
    });
}

//Прелоадер при первом посещении сайта
function initPreloader() {
    if (getCookie('preloader') !== 'y') {
        $('.preloader').addClass('show');
        setCookie('preloader', 'y', '', '/');
    }
}

// Инициализация форм
function initForms() {
    var body = $('body');
    /*
    .default-form - типовые формы
    .feedback-form__main - оставить отзыв
     */
    body.on('submit', '.default-form', function (event) {
        event.preventDefault();
        var elem = $(this);
        var url = elem.attr('action');
        var email = elem.find('input[type=email]').attr('value');
        if (email === undefined) {
            email = 'test@test.ru';
        }
        var data = elem.serialize();
        if (isValidEmailAddress(email)) {
            if (elem.hasClass('feedback-form')) {
                data = new FormData(elem[0]);
                $.ajaxSetup({
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'script'
                });
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                beforeSend: function () {
                },
                success: function (res) {
                    if (res['status'] === 'ok') {
                        elem.trigger('reset');
                        $('.file-item').remove();
                        thxModal(res['title'], res['message']);
                        ym(66780991,'reachGoal','sitelead');
                        gtag('event', 'lead', { 'event_category': 'lead', 'event_action': 'sitelead', });
                    } else {
                        errModal(res['message']);
                    }
                }
            });
        } else {
            errModal('Пожалуйста, введите действительный адрес электронной почты');
        }
    });
    //Запись на прием
    body.on('click', 'a.step-2', function () {
        $('.step-form').trigger('submit');
    });
    body.on('submit', '.step-form', function (event) {
        event.preventDefault();
        var elem = $(this);
        var url = elem.attr('action');
        var data = elem.serialize();
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            beforeSend: function () {
            },
            success: function (res) {
                if (res['status'] === 'ok') {
                    elem.trigger('reset');
                    setFormActiveStep(2, true);
                    $('.form-controls__link.step-3').removeClass('disabled');
                    $('.step-form__tab-content:last .btn').removeClass('disabled');
                    ym(66780991,'reachGoal','sitelead');
                    gtag('event', 'lead', { 'event_category': 'lead', 'event_action': 'sitelead', });
                } else {
                    errModal(res['message']);
                }
            }
        });
    });
}

//Открытие модалки "Спасибо"
function thxModal(title = false, message = false) {
    if (title) {
        $('#popup-thx .modal__title').html(title);
        if (message) {
            $('#popup-thx .modal__content-row_unique').html('<p class="modal__paragraph">' + message + '</p>');
        }
        $.magnificPopup.open({
            items: {src: '#popup-thx'},
            type: 'inline',
            removalDelay: 800,
            mainClass: 'animated modal fadeIn',
            midClick: !0,
            callbacks: {
                open: function () {
                    let mfpWrap = $('.mfp-wrap');
                    let type = mfpWrap.find('.modal_center');
                    if (type.length) {
                        mfpWrap.addClass('modal_center');
                        mfpWrap[0].setAttribute('data-scroll-lock-scrollable', '');
                    } else {
                        mfpWrap.addClass('modal_right');
                        $('.mfp-container')[0].setAttribute('data-scroll-lock-scrollable', '');
                    }
                    disablePageScroll();
                },
                beforeClose: function () {
                    $('.mfp-bg').addClass('fadeOut');
                },
                close: function () {
                    $('.item').removeClass('item_active');
                    enablePageScroll();
                }
            },
            tLoading: "Загрузка...",
            tClose: "Закрыть форму",
            closeMarkup: "",
            ajax: {tError: "Ошибка запроса."}
        }, 0);
    } else {
        $('#popup-thx .modal__title').html('Спасибо за обращение!');
        $('#popup-thx .modal__content-row_unique').html('<p class="modal__paragraph">Мы не оставим его без внимания и ответим вам в ближайшее время</p>');
        $.magnificPopup.open({
            items: {src: '#popup-thx'},
            type: 'inline',
            removalDelay: 800,
            mainClass: 'animated modal fadeIn',
            midClick: !0,
            callbacks: {
                open: function () {
                    let mfpWrap = $('.mfp-wrap');
                    let type = mfpWrap.find('.modal_center');
                    if (type.length) {
                        mfpWrap.addClass('modal_center');
                        mfpWrap[0].setAttribute('data-scroll-lock-scrollable', '');
                    } else {
                        mfpWrap.addClass('modal_right');
                        $('.mfp-container')[0].setAttribute('data-scroll-lock-scrollable', '');
                    }
                    disablePageScroll();
                },
                beforeClose: function () {
                    $('.mfp-bg').addClass('fadeOut');
                },
                close: function () {
                    $('.item').removeClass('item_active');
                    enablePageScroll();
                }
            },
            tLoading: "Загрузка...",
            tClose: "Закрыть форму",
            closeMarkup: "",
            ajax: {tError: "Ошибка запроса."}
        }, 0);
    }
}

//Открытие модалки "Ошибка"
function errModal(message = false) {
    if (message) {
        $('#popup-error .modal__content-row_unique').html('<p class="modal__paragraph">' + message + '</p>');
        $.magnificPopup.open({
            items: {src: '#popup-error'},
            type: 'inline',
            removalDelay: 800,
            mainClass: 'animated modal fadeIn',
            midClick: !0,
            callbacks: {
                open: function () {
                    let mfpWrap = $('.mfp-wrap');
                    let type = mfpWrap.find('.modal_center');
                    if (type.length) {
                        mfpWrap.addClass('modal_center');
                        mfpWrap[0].setAttribute('data-scroll-lock-scrollable', '');
                    } else {
                        mfpWrap.addClass('modal_right');
                        $('.mfp-container')[0].setAttribute('data-scroll-lock-scrollable', '');
                    }
                    disablePageScroll();
                },
                beforeClose: function () {
                    $('.mfp-bg').addClass('fadeOut');
                },
                close: function () {
                    $('.item').removeClass('item_active');
                    enablePageScroll();
                }
            },
            tLoading: "Загрузка...",
            tClose: "Закрыть форму",
            closeMarkup: "",
            ajax: {tError: "Ошибка запроса."}
        }, 0);
    } else {
        $('#popup-error .modal__content-row_unique').html('<p class="modal__paragraph">Что-то пошло не так! Попробуйте еще раз</p>');
        $.magnificPopup.open({
            items: {src: '#popup-error'},
            type: 'inline',
            removalDelay: 800,
            mainClass: 'animated modal fadeIn',
            midClick: !0,
            callbacks: {
                open: function () {
                    let mfpWrap = $('.mfp-wrap');
                    let type = mfpWrap.find('.modal_center');
                    if (type.length) {
                        mfpWrap.addClass('modal_center');
                        mfpWrap[0].setAttribute('data-scroll-lock-scrollable', '');
                    } else {
                        mfpWrap.addClass('modal_right');
                        $('.mfp-container')[0].setAttribute('data-scroll-lock-scrollable', '');
                    }
                    disablePageScroll();
                },
                beforeClose: function () {
                    $('.mfp-bg').addClass('fadeOut');
                },
                close: function () {
                    $('.item').removeClass('item_active');
                    enablePageScroll();
                }
            },
            tLoading: "Загрузка...",
            tClose: "Закрыть форму",
            closeMarkup: "",
            ajax: {tError: "Ошибка запроса."}
        }, 0);
    }
}

//Number_format js
function number_format(number, decimals, dec_point, thousands_sep) {
    var i, j, kw, kd, km;
    if (isNaN(decimals = Math.abs(decimals))) {
        decimals = 2;
    }
    if (dec_point == undefined) {
        dec_point = ",";
    }
    if (thousands_sep == undefined) {
        thousands_sep = ".";
    }
    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";
    if ((j = i.length) > 3) {
        j = j % 3;
    } else {
        j = 0;
    }
    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


    return km + kw + kd;
}

//Валидация e-mail
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

//Возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

//Устанавливает cookie
function setCookie(name, value, options, path) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    if (path) {
        updatedCookie += "; path=" + path;
    }

    document.cookie = updatedCookie;
}

//Удаляет cookie
function deleteCookie(name) {
    setCookie(name, "", "", "/", {
        expires: -1
    });
}

//Склонятор слов  число, слова - "минута", "минуты", "минут"
function declension(num, expressions) {
    var result,
        count;
    count = num % 100;
    if (count >= 5 && count <= 20) {
        result = expressions['2'];
    } else {
        count = count % 10;
        if (count === 1) {
            result = expressions['0'];
        } else if (count >= 2 && count <= 4) {
            result = expressions['1'];
        } else {
            result = expressions['2'];
        }
    }
    return result;
}

//Проверка на историю переходов
function checkReffer() {
    if (document.referrer === "") {
        return false;
    } else {
        window.history.back();
    }
}

//Сравнение двух массивов
function getArrayDiff(a, b) {
    var ret = [],
        merged = [];
    merged = a.concat(b);
    for (var i = 0; i < merged.length; i++) {
        if (merged.indexOf(merged[i]) === merged.lastIndexOf(merged[i])) {
            ret.push(merged[i]);
        }
    }
    return ret;
}

function changeCommentBA(parent, number) {
    setTimeout(function () {
        var photoKey = (typeof number === "undefined" ? parseInt($('.swiper-slide-active').attr('data-slide')) : 1);
        if (typeof parent !== "undefined") {
            $('.answer-item__description[data-parent="' + parent + '"]').removeClass('active');
            $('.answer-item__description[data-parent="' + parent + '"][data-id="' + photoKey + '"]').addClass('active').fadeTo(200, 1);
        } else {
            $('.answer-item__description').removeClass('active');
            $('.answer-item__description[data-id="1"]').addClass('active');
        }
    },600)

}

function showPriceOnStock(number) {
    $('.catalog-item__row[data-section!="' + number + '"]').fadeOut(200, 0);
    $('.catalog-item__row[data-section="' + number + '"]').fadeTo(200, 1);
}

function scrollToAnchor() {
    var margin = 100,
        hash = window.location.hash;
    if (typeof hash != "undefined" && hash != '') {
        window.location.hash = "";
        $("html, body").animate({
            scrollTop: $(hash).offset().top - margin + "px"
        }, {
            duration: 1600,
            easing: "swing"
        });
        return false;
    }
}

function showTab(tabId, tabClass) {
    if (typeof tabClass == "undefined") tabClass = '.js__show_tab';
    if (!$('#' + tabId).is(':visible')) {
        $('.js__category-items__item').removeClass('active')
        $(tabClass).fadeOut(200, 0);
        $('#' + tabId).fadeTo(200, 1);
    }
}