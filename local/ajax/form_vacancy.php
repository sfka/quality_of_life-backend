<?
if ($_POST && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
    $obForm = new \IL\FormVacancy(\IL\Settings::VACANCY_FORM_ID);
    $arFields = $_POST;
    if (!empty($_FILES['resume']['name'])) {
        $arFields['FILES'] = $_FILES;
    }
    $arResult = $obForm->add($arFields, $arFiles);
    if ($arResult === true) {
        echo json_encode([
            'status' => 'ok',
            'title' => 'Спасибо, за ваш интерес и отклик',
            'message' => '',
        ]);
    } else {
        echo json_encode([
            'status' => 'error',
            'message' => $arResult
        ]);
    }
}