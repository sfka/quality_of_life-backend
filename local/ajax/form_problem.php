<?
if ($_POST && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
    $obForm = new \IL\FormProblem(\IL\Settings::PROBLEM_FORM_ID);
    $arFields = $_POST;
    $arResult = $obForm->add($arFields);
    if ($arResult === true) {
        echo json_encode([
            'status' => 'ok',
            'title' => 'Успешно отправлена',
            'message' => 'Администратор клиники «Качество жизни» свяжется с вами в ближайшее время.',
        ]);
    } else {
        echo json_encode([
            'status' => 'error',
            'message' => $arResult
        ]);
    }
}