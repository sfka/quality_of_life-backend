<?
if ($_POST && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
    if ($_POST['VALUE']) {// Подписка или отписка в ЛК (при его наличии)
        \IL\Subscribed::subscribe(\CUser::GetEmail(), $_POST['VALUE']);
    } else {
        $arResult = \IL\Subscribed::add($_POST['email']);
        if ($arResult === true) {
            echo json_encode([
                'status' => 'ok',
                'title' => 'Спасибо, что подписались на нашу рассылку',
                'message' => ''
            ]);
        } else {
            echo json_encode([
                'status' => 'error',
                'message' => $arResult
            ]);
        }
    }
}