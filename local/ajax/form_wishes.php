<?
if ($_POST && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
    $obForm = new \IL\FormWishes(\IL\Settings::WISHES_FORM_ID);
    $arFields = $_POST;
    $arResult = $obForm->add($arFields);
    if ($arResult === true) {
        echo json_encode([
            'status' => 'ok',
            'title' => 'Спасибо за ваше мнение!',
            'message' => 'Директор лично читает все письма и принимает решение по ним.',
        ]);
    } else {
        echo json_encode([
            'status' => 'error',
            'message' => $arResult
        ]);
    }
}