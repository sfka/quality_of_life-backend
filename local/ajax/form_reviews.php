<?
if ($_POST && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
    $obForm = new \IL\FormReviews(\IL\Settings::REVIEWS_IBLOCK_ID);
    $arFields = $_POST;
    if (!empty($_FILES['photos']['name'][0])) {
        $arFields['FILES'] = $_FILES;
    }
    $arResult = $obForm->add($arFields);
    if ($arResult === true) {
        echo json_encode([
            'status' => 'ok',
            'title' => 'Спасибо за ваш отзыв!',
            'message' => 'Администратор клиники «Качество жизни» свяжется с вами в ближайшее время.',
        ]);
    } else {
        echo json_encode([
            'status' => 'error',
            'message' => $arResult
        ]);
    }
}