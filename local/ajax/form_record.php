<? if ($_POST && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
    $arFields = $_POST;
    $obSoap = new \IL\Soap();
    $params = [
        'Specialization' => $arFields['direction'],
        'Date' => $arFields['visit-date'][0] ? date($arFields['visit-date'][0] . '\T00:00:00') : date($arFields['visit-date'][1] . '\T00:00:00'),
        'TimeBegin' => date('0001-01-01\T' . $arFields['visit-time'] . ':00'),
        'EmployeeID' => $arFields['specialist'],
        'Clinic' => '25b65c0d-a956-11e9-885e-001dd8bb06d7',
    ];
    $strQueryText = $obSoap->soapCall('/ws/ws1.1cws?wsdl', 'GetReserve', [$params]);
    function object2array($object) {
        return @json_decode(@json_encode($object), 1);
    }

    $xml = object2array(simplexml_load_string($strQueryText));

    if ($xml['Результат'] === 'true') {
        $params2 = [
            'EmployeeID' => $arFields['specialist'],
            'PatientSurname' => $arFields['surname'],
            'PatientName' => $arFields['name'],
            'PatientFatherName' => '',
            'Date' => $arFields['visit-date'][0] ? date($arFields['visit-date'][0] . '\T00:00:00') : date($arFields['visit-date'][1] . '\T00:00:00'),
            'TimeBegin' => date('0001-01-01\T' . $arFields['visit-time'] . ':00'),
            'Comment' => '',
            'Phone' => $arFields['phone'],
            'Email' => '',
            'Address' => $arFields['address'],
            'Clinic' => '25b65c0d-a956-11e9-885e-001dd8bb06d7',
            'GUID' => $xml['УИД'] ? $xml['УИД'] : '',
        ];
        $strQueryText2 = $obSoap->soapCall('/ws/ws1.1cws?wsdl', 'BookAnAppointment', [$params2]);

        $xml2 = object2array(simplexml_load_string($strQueryText2));

        if ($xml2['Результат'] === 'true') {
            $obFormRecord = new \IL\FormRecord(\IL\Settings::RECORD_FORM_ID);
            $obFormRecord->add($arFields);
            echo json_encode([
                'status' => 'ok',
            ]);
        } else {
            echo json_encode([
                'status' => 'error',
                //'message' => $xml2['ОписаниеОшибки'],
                'message' => 'Не удалось произвести запись',
            ]);
        }
    } else {
        echo json_encode([
            'status' => 'error',
            //'message' => $xml['ОписаниеОшибки'],
            'message' => 'Не удалось зарезервировать время',
        ]);
    }
}