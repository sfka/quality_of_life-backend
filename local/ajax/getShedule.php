<? if ($_POST && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

    if ($_POST['SPECIALIST']) {
        $startDate = date('Y-m-d\TH:i:s');
        $finishDate = new DateTime(date('Y-m-d\TH:i:s'));

        $finishDate->modify('+1 month');

        //Получим XML
        $obSoap = new \IL\Soap();
        $params = [
            'StartDate' => $startDate,
            'FinishDate' => $finishDate->format('Y-m-d\TH:i:s'),
        ];
        $strQueryText = $obSoap->soapCall('/ws/ws1.1cws?wsdl', 'GetSchedule', [$params]);

        //Спарсим XML и преобразуем в массив
        function object2array($object) {
            return @json_decode(@json_encode($object), 1);
        }

        $xml = object2array(simplexml_load_string($strQueryText));

        //Получим свободное время специалиста
        $arDateJs = [];
        $arTimes = [];
        foreach ($xml['ГрафикДляСайта'] as $arGraph) {
            //Найдем сотрудника по XML_ID
            if ($_POST['SPECIALIST'] === $arGraph['СотрудникID']) {
                //Сформируем массив доступных и отфармотированных дат для js-плагина
                if (count($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика'], COUNT_RECURSIVE) - count($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика']) > 1) {
                    foreach ($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика'] as $arData) {
                        $arDateJs[] = date_format(date_create($arData['Дата']), "Y-m-d");
                        $arTimes[] = date_format(date_create($arData['ВремяНачала']), "H:i");
                    }
                } else {
                    $arDateJs[] = date_format(date_create($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика']['Дата']), "Y-m-d");
                    $arTimes[] = date_format(date_create($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика']['ВремяНачала']), "H:i");
                }
                //Получим ближайшее свободное время
                $arResult['time_nearest'] = current($arTimes);

                if (!empty($_POST['DATE'])) {
                    $arTime = [];
                    //Найдем ключи в массиве графика свободного времени
                    //Ищем в многомерном массиве по ключу "Дата" значения $_POST['DATE']
                    if (count($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика'], COUNT_RECURSIVE) - count($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика']) > 1) {
                        $keyDate = array_keys(array_column($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика'], 'Дата'), $_POST['DATE']);
                        //Сформируем интервалы между временем начала и окончания с учетом занятого времени
                        foreach ($keyDate as $itemDate) {
                            $start = date_format(date_create($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика'][$itemDate]['ВремяНачала']), "H:i:s");
                            $end = date_format(date_create($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика'][$itemDate]['ВремяОкончания']), "H:i:s");
                            $st = strtotime($start);
                            $et = strtotime($end);
                            while ($st < $et) {
                                $t = strtotime("+30 minute", $st);
                                $arTime[] = date("H:i", $st);
                                $arTime[] = date("H:i", $t);
                                $st = $t;
                            }
                        }
                    } else {
                        //Сформируем интервалы между временем начала и окончания с учетом занятого времени
                        $start = date_format(date_create($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика']['ВремяНачала']), "H:i:s");
                        $end = date_format(date_create($arGraph['ПериодыГрафика']['СвободноеВремя']['ПериодГрафика']['ВремяОкончания']), "H:i:s");
                        $st = strtotime($start);
                        $et = strtotime($end);
                        while ($st < $et) {
                            $t = strtotime("+30 minute", $st);
                            $arTime[] = date("H:i", $st);
                            $arTime[] = date("H:i", $t);
                            $st = $t;
                        }
                    }
                    $arTime = array_values(array_unique($arTime));
                }
            }
        }
        $arDateJs = array_values(array_unique($arDateJs));

        $arResult['date'] = $arDateJs;
        $arResult['time'] = $arTime;
        echo json_encode([
            'status' => 'ok',
            'result' => $arResult,
        ]);
    }
}