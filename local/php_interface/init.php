<?
define("SITE_STYLE_PATH", "/local/styles");
define("SITE_INCLUDE_PATH", "/local/include");
define("SITE_USER_CLASS_PATH", "/local/php_interface/user_class");
define("SITE_AJAX_PATH", "/local/ajax");
define("SITE_SERVICE_IBLOCK_ID", "3");
define("SITE_PRICE_IBLOCK_ID", "19");
define("SITE_STOCK_IBLOCK_ID", "14");
define("SITE_REVIEWS_IBLOCK_ID", "2");
define("SITE_PROGRAMS_IBLOCK_ID", "18");
define("SITE_VACANCIES_IBLOCK_ID", "21");
define("SITE_SPECIALISTS_IBLOCK_ID", "4");

define("SITE_DIRECTION_PROP", "DIRECTION");
define("SITE_SERVICE_PROP", "SERVICE");
define("SITE_PRICELIST_PROP", "PRICELIST");

CModule::AddAutoloadClasses("", array(
    '\IL\Iblock' => SITE_USER_CLASS_PATH . '/classIblock.php',
    '\IL\Utilities' => SITE_USER_CLASS_PATH . '/classUtilities.php',
    '\IL\Settings' => SITE_USER_CLASS_PATH . '/classSettings.php',
    '\IL\Catalog' => SITE_USER_CLASS_PATH . '/classCatalog.php',
    '\IL\FormReviews' => SITE_USER_CLASS_PATH . '/classFormReviews.php',
    '\IL\FormQuestions' => SITE_USER_CLASS_PATH . '/classFormQuestions.php',
    '\IL\FormFeedback' => SITE_USER_CLASS_PATH . '/classFormFeedback.php',
    '\IL\FormCallback' => SITE_USER_CLASS_PATH . '/classFormCallback.php',
    '\IL\FormConsultation' => SITE_USER_CLASS_PATH . '/classFormConsultation.php',
    '\IL\FormRecord' => SITE_USER_CLASS_PATH . '/classFormRecord.php',
    '\IL\FormProblem' => SITE_USER_CLASS_PATH . '/classFormProblem.php',
    '\IL\FormStock' => SITE_USER_CLASS_PATH . '/classFormStock.php',
    '\IL\FormWishes' => SITE_USER_CLASS_PATH . '/classFormWishes.php',
    '\IL\FormVacancy' => SITE_USER_CLASS_PATH . '/classFormVacancy.php',
    '\IL\Locations' => SITE_USER_CLASS_PATH . '/classLocations.php',
    '\IL\Soap' => SITE_USER_CLASS_PATH . '/classSoap.php',
    '\IL\Handlers' => SITE_USER_CLASS_PATH . '/classHandlers.php',
    'IL\Subscribed' => SITE_USER_CLASS_PATH . '/classSubscribed.php',
    'IL\Price' => SITE_USER_CLASS_PATH . '/classPrice.php',
));

\Bitrix\Main\EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockElementAdd", ["\IL\Handlers", "changeSectionPropByElement"]);
\Bitrix\Main\EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockElementUpdate", ["\IL\Handlers", "changeSectionPropByElement"]);

if (CSite::InDir('/problems/index.php')) {
    LocalRedirect('/');
}