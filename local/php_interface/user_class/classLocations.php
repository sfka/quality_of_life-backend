<?

namespace IL;

/**
 * Класс для работы с местоположениями
 * Class Locations
 * @package IL
 */
class Locations {

    /**
     * Возвращает города и страны по маске
     * @param $arFields
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function SearchLocationByName($arFields) {
        $arResult = false;
        if ($arFields['NAME'] == 'COUNTRY' || $arFields['NAME'] == 'PERSONAL_COUNTRY') {
            $typeCode = 'COUNTRY';
        } else {
            $typeCode = 'CITY';
        }
        if (\Bitrix\Main\Loader::includeModule("sale")) {
            $res = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array(
                    '=NAME.LANGUAGE_ID' => LANGUAGE_ID,
                    '%=NAME.NAME' => $arFields['SEARCH'] . '%',
                    'TYPE_CODE' => $typeCode
                ),
                'select' => array(
                    '*',
                    'NAME_RU' => 'NAME.NAME',
                    'TYPE_CODE' => 'TYPE.CODE'
                ),
                'limit' => 5
            ));
            while ($item = $res->fetch()) {
                if ($arFields['NAME'] == 'COUNTRY' || $arFields['NAME'] == 'PERSONAL_COUNTRY') {
                    $item['PROFILE_ID'] = self::getCountryIdFromUserInfo($item['NAME_RU']);
                }
                if ($arFields['NAME'] == 'PERSONAL_CITY' || $arFields['NAME'] == 'WORK_CITY') {
                    $id = self::GetLocationByName($item['NAME_RU']);
                    $item['PROFILE_ID'] = $id['ID'];
                }
                $arResult[] = $item;
            }
            return $arResult;
        } else {
            return false;
        }
    }

    /**
     * Сверяет название города со списком городов в ЛК пользователя и возвращает его ID
     * @param $countryName
     * @return bool
     */
    public static function getCountryIdFromUserInfo($countryName) {
        $country = GetCountryArray();
        if ($key = array_search($countryName, $country['reference'])) {
            return $country['reference_id'][$key];
        } else {
            return false;
        }
    }

    /**
     * Ищем информацию о местоположении по названию
     * @param $name
     * @return bool|mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function GetLocationByName($name) {
        if (\Bitrix\Main\Loader::includeModule("sale")) {
            // ищем город
            $res = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array(
                    '=NAME.LANGUAGE_ID' => 'ru',
                    '=NAME.NAME' => $name,
                    'TYPE_CODE' => 'CITY'
                ),
                'select' => array(
                    '*',
                    'NAME_RU' => 'NAME.NAME',
                    'TYPE_CODE' => 'TYPE.CODE'
                ),
                'limit' => 1
            ));
            if ($item = $res->fetch()) {
                return $item;
            }
            // ищем деревню
            $res = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array(
                    '=NAME.LANGUAGE_ID' => 'ru',
                    '=NAME.NAME' => $name,
                    'TYPE_CODE' => 'VILLAGE'
                ),
                'select' => array(
                    '*',
                    'NAME_RU' => 'NAME.NAME',
                    'TYPE_CODE' => 'TYPE.CODE'
                ),
                'limit' => 1
            ));
            if ($item = $res->fetch()) {
                return $item;
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * Возвращает название местоположения по ID
     * @param $id
     * @return bool|mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function GetLocationById($id) {
        if (\Bitrix\Main\Loader::includeModule("sale")) {
            // ищем город
            $res = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array(
                    '=ID' => $id,
                    '=NAME.LANGUAGE_ID' => 'ru'
                ),
                'select' => array(
                    '*',
                    'NAME_RU' => 'NAME.NAME',
                    'TYPE_CODE' => 'TYPE.CODE'
                ),
                'limit' => 1
            ));
            if ($item = $res->fetch()) {
                return $item;
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * Определение города по IP
     * Вызов \IL\Locations::GeoIP()->cityName
     * @return string
     */
    public static function GeoIP() {
        $arResult = false;
        if($_COOKIE['cityPopup']){
            $arCity = json_decode($_COOKIE['cityPopup'], true);
            $arCityes = Catalog::getSectionList(Settings::CONTACTS_IBLOCK_ID);
            $arResult = 'г. ' . $arCityes[$arCity['city']]['NAME'];
        } else {
            $ipAddress = \Bitrix\Main\Service\GeoIp\Manager::getRealIp();
            \Bitrix\Main\Service\GeoIp\Manager::useCookieToStoreInfo(true);
            $geoResult = \Bitrix\Main\Service\GeoIp\Manager::getDataResult($ipAddress, "ru");
            if ($geoResult) {
                if ($geoResult->isSuccess()) {
                    $arRes = $geoResult->getGeoData();
                    $arCity = Catalog::getSectionList(Settings::CONTACTS_IBLOCK_ID);
                    foreach ($arCity as $key => $city){
                        if($city['NAME'] !== $arRes->cityName) continue;
                        $arResult = 'г. ' . $arRes->cityName;
                        $arJson = ['city' => $key];
                        setCookie("cityPopup", json_encode($arJson));
                    }
                } elseif (!$geoResult->isSuccess()) {
                    $arResult = false;
                }
            } else {
                $arResult = false;
            }
        }
        return $arResult;
    }

}