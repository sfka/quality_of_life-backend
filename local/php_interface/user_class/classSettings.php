<?

namespace IL;

/**
 * Класс настроек
 * Class Settings
 * @package IL
 */
class Settings {

    const CACHE_TIME = 3600;
    const REVIEWS_IBLOCK_ID = 2;
    const SERVICES_IBLOCK_ID = 3;
    const SPECIALISTS_IBLOCK_ID = 4;
    const QUESTIONS_IBLOCK_ID = 5;
    const FEEDBACK_FORM_ID = 9;
    const CALLBACK_FORM_ID = 25;
    const CONSULTATION_FORM_ID = 23;
    const PROBLEM_FORM_ID = 24;
    const VACANCY_FORM_ID = 22;
    const PROBLEMS_SOLUTIONS_IBLOCK_ID = 10;
    const EQUIPMENT_IBLOCK_ID = 11;
    const CONTACTS_IBLOCK_ID = 12;
    const WISHES_FORM_ID = 13;
    const STOCK_IBLOCK_ID = 14;
    const STOCK_FORM_ID = 15;
    const PROGRAM_FORM_ID = 16;
    const RECORD_FORM_ID = 26;

}