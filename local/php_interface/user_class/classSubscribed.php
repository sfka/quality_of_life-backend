<?

namespace IL;

use \Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;

/**
 * Класс для работы с подпиской пользователей
 * Class Subscribed
 * @package IL
 */
class Subscribed {

    /**
     * id рассылки
     */
    const SUBSC_ID = [1];

    /**
     * Подписка/отписка пользователя в ЛК
     * @param $userEmail
     * @param $active
     */
    public static function subscribe($userEmail, $active) {
        if ($active == "Y") {
            global $USER;
            self::add($userEmail, $USER->GetID());
        } else {
            self::delete($userEmail);
        }
    }

    /**
     * Добавляет подписчика
     * @param $userEmail
     * @param $userID
     * @param $FORMAT
     * @return mixed|string
     * @throws LoaderException
     */
    public static function add($userEmail, $userID = false, $FORMAT = false) {
        if (!Loader::includeModule('subscribe')) return false;

        global $USER;
        $arFields = [
            "USER_ID" => ($USER->GetID() ? $USER->GetID() : $userID),
            "EMAIL" => $userEmail,
            "FORMAT" => ($FORMAT ? "text" : "html"),
            "SEND_CONFIRM" => "N",
            "ACTIVE" => "Y",
            "RUB_ID" => self::SUBSC_ID,
            "ALL_SITES" => "Y"
        ];
        $subscr = new \CSubscription;
        $ID = $subscr->Add($arFields);
        if ($ID > 0) {
            return $subscr->Update($ID, ["CONFIRMED" => "N"]);
        } else {
            return $subscr->LAST_ERROR;
        }
    }

    /**
     * @param $userEmail
     * @return bool
     * @throws LoaderException
     */
    public static function delete($userEmail) {
        if (!Loader::includeModule('subscribe')) return false;

        $rs = \CSubscription::GetByEmail($userEmail);
        $f = $rs->Fetch();
        \CSubscription::Delete($f['ID']);
    }

    /**
     * Возвращает список подписок пользователя
     * @param $userEmail
     * @return mixed
     * @throws LoaderException
     */
    public static function check($userEmail) {
        if (!Loader::includeModule('subscribe')) return false;

        $res = \CSubscription::GetByEmail($userEmail)->Fetch();
        return $res['EMAIL'];
    }
}