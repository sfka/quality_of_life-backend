<?

namespace IL;

use \Bitrix\Iblock\ElementTable;
use \Bitrix\Iblock\SectionTable;
use \Bitrix\Main\ArgumentException;
use \Bitrix\Main\Loader;
use \Bitrix\Main\LoaderException;
use \Bitrix\Main\ObjectPropertyException;
use \Bitrix\Main\SystemException;

/**
 * Класс для работы с каталогорм
 * Class Catalog
 * @package IL
 */
class Catalog {

    /**
     * Возвращает список разделов в виде дерева
     * @param $iblockID
     * @return bool|mixed
     * @throws LoaderException
     */
    public static function getSectTreeList($iblockID) {
        if (!Loader::includeModule('iblock')) return false;

        $arFilter = [
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => IntVal($iblockID),
            'GLOBAL_ACTIVE' => 'Y',
        ];
        $arSelect = [
            'IBLOCK_ID',
            'ID',
            'NAME',
            'DEPTH_LEVEL',
            'IBLOCK_SECTION_ID',
        ];
        $arOrder = [
            'DEPTH_LEVEL' => 'ASC',
            'SORT' => 'ASC',
        ];
        $rsSections = \CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
        $sectionLinc = [];
        $arResult['ROOT'] = [];
        $sectionLinc[0] = &$arResult['ROOT'];
        while ($arSection = $rsSections->GetNext()) {
            $sectionLinc[(int)$arSection['IBLOCK_SECTION_ID']]['CHILD'][$arSection['ID']] = $arSection;
            $sectionLinc[$arSection['ID']] = &$sectionLinc[(int)$arSection['IBLOCK_SECTION_ID']]['CHILD'][$arSection['ID']];
        }
        unset($sectionLinc);
        $arResult['ROOT'] = $arResult['ROOT']['CHILD'];
        return $arResult['ROOT'];
    }

    /**
     * Возвращает информацию о разделе по ID
     * @param $iblockID
     * @param $sectionID
     * @param array $arOrder
     * @return array|bool
     * @throws LoaderException
     */
    public static function getSectionsByID($iblockID, $sectionID, $arOrder = []) {
        if (!Loader::includeModule('iblock')) return false;

        $arRes = [];
        $arFilter = [
            'ACTIVE ' => 'Y',
            'GLOBAL_ACTIVE' => 'Y',
            'IBLOCK_ID' => $iblockID,
            '=ID' => $sectionID,
        ];
        $arSelect = [];
        $rsSect = \CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect, false);
        while ($obSect = $rsSect->GetNext()) {
            $arRes[] = $obSect;
        }
        return $arRes;
    }

    /**
     * Возвращает название раздела по ID
     * @param $iblockID
     * @param $sectionID
     * @return array|bool
     * @throws LoaderException
     */
    public static function getSectionNameByID($iblockID, $sectionID) {
        if (!Loader::includeModule('iblock')) return false;

        $arFilter = [
            'ACTIVE ' => 'Y',
            'GLOBAL_ACTIVE' => 'Y',
            'IBLOCK_ID' => $iblockID,
            '=ID' => $sectionID,
        ];
        $arSelect = ['NAME'];
        $arRes = \CIBlockSection::GetList([], $arFilter, false, $arSelect, false)->Fetch();
        return $arRes['NAME'];
    }

    /**
     * Возвращает параметры родительского раздела
     * @param $url
     * @return array|bool
     * @throws LoaderException
     */
    public static function getParentSectParams($url) {
        if (!Loader::includeModule('iblock')) return false;

        $arResult = [];

        $arLinks = array_diff(explode('/', $url), ['']);
        $arLink = array_pop($arLinks);
        if (count($arLinks) > 0) {
            $sectUrl = implode('/', $arLinks);
        } else {
            $sectUrl = $arLink;
        }

        //Получим название раздела
        $sSectionName = '';
        $sPath = $_SERVER['DOCUMENT_ROOT'] . '/' . $sectUrl . '/' . '.section.php';
        @include($sPath);//Теперь в переменной $sSectionName содержится название раздела
        $arResult['NAME'] = $sSectionName;
        $arResult['URL'] = SITE_DIR . $sectUrl . '/';
        $arResult['PARENT_SECT'] = current($arLinks);
        $arResult['CHILD_SECT'] = end(array_diff(explode('/', $url), ['']));

        return $arResult;
    }

    /**
     * Возвращает разделы инфоблока с элементами
     * @param $iblockID
     * @param bool $active
     * @param array $params
     * @return array|bool
     * @throws LoaderException
     */
    public static function getSectionList($iblockID, $active = true, $params = []) {
        if (!Loader::includeModule('iblock')) return false;

        $arResult = [];
        $arOrder = ['SORT' => 'ASC'];
        $arFilter = [
            'IBLOCK_ID' => $iblockID,
            "ACTIVE" => "Y",
            "INCLUDE_SUBSECTIONS" => "Y",
        ];
        $arFilter = array_merge($arFilter, $params);
        $arSelect = [];
        $rsSect = \CIBlockSection::GetList($arOrder, $arFilter, true, $arSelect, false);
        while ($arSect = $rsSect->GetNext()) {
            if ($active) {
                if ($arSect['ELEMENT_CNT'] > 0) {
                    $arResult[$arSect['ID']] = $arSect;
                    $arResult[$arSect['ID']]['ITEMS'] = self::getElementList($iblockID, ["IBLOCK_SECTION_ID" => $arSect['ID']]);
                }
            } else {
                $arResult[$arSect['ID']] = $arSect;
                $arResult[$arSect['ID']]['ITEMS'] = self::getElementList($iblockID, ["IBLOCK_SECTION_ID" => $arSect['ID']]);
            }
        }
        return $arResult;
    }

    /**
     * @param $elementId
     * @return bool
     * @throws LoaderException
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public static function getRootSectionElementById($elementId) {
        if (!Loader::includeModule('iblock')) return false;

        $element = ElementTable::getRow([
            'select' => [
                'ID',
                'IBLOCK_ID',
                'IBLOCK_SECTION_ID',
            ],
            'filter' => [
                '=ID' => $elementId,
            ],
        ]);
        if (!$element || !$element['IBLOCK_SECTION_ID']) return false;

        $currentElementSection = SectionTable::getRow([
            'select' => [
                'ID',
                'CODE',
                'LEFT_MARGIN',
                'RIGHT_MARGIN',
                'DEPTH_LEVEL',
            ],
            'filter' => [
                '=IBLOCK_ID' => $element['IBLOCK_ID'],
                '=ID' => $element['IBLOCK_SECTION_ID'],
            ],
        ]);
        if (!$currentElementSection) return false;
        if ($currentElementSection['DEPTH_LEVEL'] == 1) return $currentElementSection;

        $rootSection = SectionTable::getRow([
            'select' => ['ID', 'CODE'],
            'filter' => [
                '=IBLOCK_ID' => $element['IBLOCK_ID'],
                '<LEFT_MARGIN' => $currentElementSection['LEFT_MARGIN'],
                '>RIGHT_MARGIN' => $currentElementSection['RIGHT_MARGIN'],
                '=DEPTH_LEVEL' => 1,
            ],
        ]);

        return $rootSection;
    }

    /**
     * Возвращает список элементов со свойствами
     * @param $iblockID
     * @param $arParams
     * @param array $order
     * @return array|bool
     * @throws LoaderException
     */
    public static function getElementList($iblockID, $arParams = [], $order = ['SORT' => 'ASC']) {
        if (!Loader::includeModule('iblock')) return false;

        $arResult = [];
        $arOrder = $order;
        $arSelect = [];
        $arFilter = [
            "IBLOCK_ID" => IntVal($iblockID),
            "ACTIVE" => "Y",
        ];
        $arFilter = array_merge($arFilter, $arParams);
        $res = \CIBlockElement::GetList($arOrder, $arFilter, false, [], $arSelect);
        while ($ob = $res->GetNextElement()) {

            $arFields = $ob->GetFields();
            $arFields['PROPERTIES'] = [];
            $arProps = $ob->GetProperties();

            $arFields['PROPERTIES'] = $arProps;
            array_push($arResult, $arFields);
        }
        return $arResult;
    }

    /**
     * Получим название элемента по ID
     * @param $iblockID
     * @param $elementID
     * @param array $arProps
     * @return array|bool
     * @throws LoaderException
     */
    public static function getElementNameByID($iblockID, $elementID, $arProps = []) {
        if (!Loader::includeModule('iblock')) return false;

        $arSelect = ["NAME"];
        $arFilter = [
            "IBLOCK_ID" => IntVal($iblockID),
            "ID" => $elementID,
            "ACTIVE" => "Y",
        ];
        $arFilter = array_merge($arFilter, $arProps);
        $res = \CIBlockElement::GetList([], $arFilter, $arSelect, false, [])->Fetch();
        return $res['NAME'];
    }

    /**
     * Получим название элемента по XML_ID
     * @param $iblockID
     * @param $elementXML_ID
     * @param array $arProps
     * @return array|bool
     * @throws LoaderException
     */
    public static function getElementNameByXML_ID($iblockID, $elementXML_ID, $arProps = []) {
        if (!Loader::includeModule('iblock')) return false;

        $arSelect = ["NAME"];
        $arFilter = [
            "IBLOCK_ID" => IntVal($iblockID),
            "XML_ID" => $elementXML_ID,
            "ACTIVE" => "Y",
        ];
        $arFilter = array_merge($arFilter, $arProps);
        $res = \CIBlockElement::GetList([], $arFilter, $arSelect, false, [])->Fetch();
        return $res['NAME'];
    }

    /**
     * Сформируем левое меню из свойств и разделов
     * @param $iblockID
     * @return array|bool
     * @throws LoaderException
     */
    public static function getLeftMenu($iblockID) {
        if (!Loader::includeModule('iblock')) return false;

        $arResult = [];

        // Получим направления деятельности(разделы ИБ услуги)
        $arSect = [];
        $arOrderSect = ['SORT' => 'ASC'];
        $arFilterSect = [
            'IBLOCK_ID' => Settings::SERVICES_IBLOCK_ID,
            "ACTIVE" => "Y",
            "DEPTH_LEVEL" => 1,
        ];
        $rsSect = \CIBlockSection::GetList($arOrderSect, $arFilterSect, true, [
            'NAME',
            'ID',
        ], false);
        while ($dbSect = $rsSect->GetNext()) {
            $arSect[] = $dbSect;
        }

        // Получим элементы с проверкой "привязка к разделу"
        $arFilter = [
            "IBLOCK_ID" => IntVal($iblockID),
            "ACTIVE" => "Y",
        ];
        foreach ($arSect as $keySect => $itemSect) {
            $arFilter["=PROPERTY_DIRECTION"] = $itemSect['ID'];
            if ($res = \CIBlockElement::GetList([], $arFilter, false, ['nPageSize' => 1], [
                'IBLOCK_ID',
                'ID',
                'PROPERTY_DIRECTION',
            ])->Fetch()) {
                if (!empty($res['PROPERTY_DIRECTION_VALUE'])) {
                    $arResult['SECTIONS'][] = $itemSect;
                }
            }
        }

        // Получим разделы(теги)
        $arOrderSectTags = ['SORT' => 'ASC'];
        $arFilterSectTags = [
            'IBLOCK_ID' => IntVal($iblockID),
            "ACTIVE" => "Y",
            "DEPTH_LEVEL" => 1,
        ];
        $rsSectTags = \CIBlockSection::GetList($arOrderSectTags, $arFilterSectTags, true, [
            'NAME',
            'ID',
        ], false);
        while ($dbSectTags = $rsSectTags->GetNext()) {
            if ($dbSectTags['ELEMENT_CNT'] > 0) {
                $arResult['TAGS'][] = $dbSectTags;
            }
        }
        return $arResult;
    }

    /**
     * Сформируем табы из подразделов
     * @param $iblockID
     * @param $sectionID
     * @return array|bool
     * @throws LoaderException
     */
    public static function getTabsMenu($iblockID, $sectionID) {
        if (!Loader::includeModule('iblock')) return false;

        $arResult = [];

        // Получим направления деятельности(разделы ИБ услуги)
        $arSect = [];
        $arOrderSect = ['SORT' => 'ASC'];
        $arFilterSect = [
            'IBLOCK_ID' => Settings::SERVICES_IBLOCK_ID,
            "SECTION_ID" => $sectionID,
            "ACTIVE" => "Y",
            "DEPTH_LEVEL" => 2,
        ];
        $rsSect = \CIBlockSection::GetList($arOrderSect, $arFilterSect, true, [
            'NAME',
            'ID',
        ], false);
        while ($dbSect = $rsSect->GetNext()) {
            $arSect[] = $dbSect;
        }

        // Получим элементы с проверкой "привязка к разделу"
        $arFilter = [
            "IBLOCK_ID" => IntVal($iblockID),
            "ACTIVE" => "Y",
        ];
        foreach ($arSect as $keySect => $itemSect) {
            $arFilter["=PROPERTY_SERVICE"] = $itemSect['ID'];
            if ($res = \CIBlockElement::GetList([], $arFilter, false, ['nPageSize' => 1], [
                'IBLOCK_ID',
                'ID',
                'PROPERTY_SERVICE',
            ])->Fetch()) {
                if (!empty($res['PROPERTY_SERVICE_VALUE'])) {
                    $arResult[] = $itemSect;
                }
            }
        }
        return $arResult;
    }

    /**
     * Сгруппируем массив
     * @param $arElements
     * @return array|bool
     * @throws LoaderException
     */
    public static function getGroupItems($arElements) {
        if (!Loader::includeModule('iblock')) return false;

        $arResult = [];
        foreach ($arElements as $arItem) {
            //Если свойство "Направление деятельности" не пустое
            if (!empty($arItem['PROPERTIES']['DIRECTION']['VALUE'])) {
                //Получим информацию о привязанном свойстве "Направление деятельности"
                $arDirectionName = self::getSectionNameByID($arItem['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['DIRECTION']['VALUE']);
                $arResult[$arDirectionName]['ITEMS'][] = $arItem;//Сгруппируем по направлению деятельности и элементам
                //Если свойство "Услуга" не пустое
                if (!empty($arItem['PROPERTIES']['SERVICE']['VALUE'])) {
                    //Получим информацию о привязанном свойстве "Услуга"
                    $arService = self::getSectionsByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['SERVICE']['VALUE']);
                    foreach ($arService as $key => $item) {
                        $arResult[$arDirectionName]['SERVICE'][$key] = $item;//Сгруппируем по направлению деятельности + услуге
                    }
                }
            }
            //Если элемент лежит в разделе
            if (!empty($arItem['IBLOCK_SECTION_ID'])) {
                $arSectName = self::getSectionNameByID($arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID']);
                $arResult[$arSectName]['ITEMS'][] = $arItem;//Сгруппируем по разделам(тегам) и элементам
            }
        }
        return $arResult;
    }


}