<?

namespace IL;

use \Bitrix\Main\Loader;
use \Bitrix\Main\LoaderException;

/**
 * Класс для работы с формой заявка на услугу
 * Class FormFeedback
 * @package IL
 */
class FormVacancy extends Iblock {

    /**
     * Добавляет элемент с данными из формы в ИБ
     * @param $arFields
     * @param $arFiles
     * @return bool|string
     * @throws LoaderException
     */
    public function add($arFields, $arFiles) {
        if (!Loader::includeModule('iblock')) return false;

        $el = new \CIBlockElement;
        $arLoadProductArray = [
            "IBLOCK_ID" => $this->iblockID,
            "DATE_ACTIVE_FROM" => date('d.m.Y H:i:s'),
            "PROPERTY_VALUES" => [
                'PHONE' => $arFields['phone'],
                'NAME' => $arFields['name'],
                'VACANCY' => $arFields['vacancy'],
                'UTM_SOURCE' => $arFields['utm_source'],
                'UTM_MEDIUM' => $arFields['utm_medium'],
                'UTM_CAMPAIGN' => $arFields['utm_campaign'],
                'UTM_CONTENT' => $arFields['utm_content'],
                'UTM_TERM' => $arFields['utm_term'],
            ],
            "NAME" => "Заявка от " . date('d.m.Y H:i:s'),
            "ACTIVE" => "N",
            "TYPE" => "html",
        ];
        if (isset($arFields['FILES'])) {
            $error = false;
            $ext = pathinfo($_FILES["photos"]['name'], PATHINFO_EXTENSION);
            $allowed = explode(', ', $arFields['type_file']);
            if (!in_array($ext, $allowed)) {
                $error = true;
            } else {
                $file = [
                    'name' => $_FILES["resume"]['name'],
                    'size' => $_FILES["resume"]['size'],
                    'tmp_name' => $_FILES["resume"]['tmp_name'],
                    'type' => $_FILES["resume"]['type']
                ];
                $arFiles[] = [
                    'VALUE' => $file,
                    'DESCRIPTION' => ''
                ];
            }
            if ($error) {
                return 'Неверный тип файлов, требуется ' . $arFields['type_file'];
            }
            $arLoadProductArray["PROPERTY_VALUES"]["RESUME"] = $arFiles;
        }
        $fieldText = '';
        foreach ($arLoadProductArray['PROPERTY_VALUES'] as $key => $values) {
            if ($values != '' && $key != 'RESUME') {
                $resName = \CIBlockProperty::GetByID($key, $this->iblockID, false)->GetNext();
                $name = $resName['NAME'];
                $fieldText .= $name . ': ' . $values . '<br>';
            }
        }
        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            $arEventFields = [
                "FORM_NAME" => "Заявка на услугу",
                "TEXT" => $fieldText,
            ];
            \CEvent::Send("FORMS", SITE_ID, $arEventFields, 'N', 31, $_FILES["photos"]['tmp_name']);
            return true;
        } else {
            return $el->LAST_ERROR;
        }
    }

}