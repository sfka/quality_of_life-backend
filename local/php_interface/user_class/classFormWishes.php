<?

namespace IL;

use \Bitrix\Main\Loader;
use \Bitrix\Main\LoaderException;

/**
 * Класс для работы с формой вопросов в контактах
 * Class FormWishes
 * @package IL
 */
class FormWishes extends Iblock {

    /**
     * Добавляет элемент с данными из формы в ИБ
     * @param $arFields
     * @return bool|string
     * @throws LoaderException
     */
    public function add($arFields) {
        if (!Loader::includeModule('iblock')) return false;

        $el = new \CIBlockElement;
        $arLoadProductArray = [
            "IBLOCK_ID" => $this->iblockID,
            "DATE_ACTIVE_FROM" => date('d.m.Y H:i:s'),
            "PROPERTY_VALUES" => [
                'NAME' => $arFields['name'],
                'EMAIL' => $arFields['email'],
                'UTM_SOURCE' => $arFields['utm_source'],
                'UTM_MEDIUM' => $arFields['utm_medium'],
                'UTM_CAMPAIGN' => $arFields['utm_campaign'],
                'UTM_CONTENT' => $arFields['utm_content'],
                'UTM_TERM' => $arFields['utm_term'],
            ],
            "NAME" => "Вопрос от " . date('d.m.Y H:i:s'),
            "ACTIVE" => "N",
            "TYPE" => "html",
        ];
        $fieldText = '';
        foreach ($arLoadProductArray['PROPERTY_VALUES'] as $key => $values) {
            if ($values != '') {
                $resName = \CIBlockProperty::GetByID($key, $this->iblockID, false)->GetNext();
                $name = $resName['NAME'];
                $fieldText .= $name . ': ' . $values . '<br>';
            }
        }
        if (!empty($arFields['message'])) {
            $fieldText .= 'Вопрос: ' . $arFields['message'] . '<br>';
            $arLoadProductArray['PREVIEW_TEXT'] = $arFields['message'];
        }
        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            $arEventFields = [
                "FORM_NAME" => 'Вопрос',
                "TEXT" => $fieldText,
            ];
            \CEvent::Send("FORMS", SITE_ID, $arEventFields, 'N', 31);
            return true;
        } else {
            return $el->LAST_ERROR;
        }
    }

}