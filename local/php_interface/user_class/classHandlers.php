<?

namespace IL;

/**
 * Класс для работы с обработчиками событий
 * Class Handlers
 * @package IL
 */
class Handlers {

    public static function changeSectionPropByElement(&$arFields) {
        //if($arFields['IBLOCK_ID'] == SITE_PRICE_IBLOCK_ID || $arFields['IBLOCK_ID'] == SITE_STOCK_IBLOCK_ID || $arFields['IBLOCK_ID'] == SITE_PROGRAMS_IBLOCK_ID) {
        if($arFields['IBLOCK_ID'] == SITE_PRICE_IBLOCK_ID || $arFields['IBLOCK_ID'] == SITE_PROGRAMS_IBLOCK_ID) {
            $resPropService = \CIBlockProperty::GetByID(SITE_SERVICE_PROP, $arFields['IBLOCK_ID']);
            if($arPropService = $resPropService->GetNext()) $service = $arPropService;

            $resPropDirection = \CIBlockProperty::GetByID(SITE_DIRECTION_PROP, $arFields['IBLOCK_ID']);
            if($arPropDirection = $resPropDirection->GetNext()) $direction = $arPropDirection;

            $sectionIDs = [];
            foreach ($arFields['PROPERTY_VALUES'][$service['ID']] as $key => $arService) {
                if ($arService['VALUE']) {
                    $db_old_groups = \CIBlockElement::GetElementGroups($arService['VALUE'], true);
                    while ($ar_group = $db_old_groups->Fetch()) {
                        $nav = \CIBlockSection::GetNavChain(false, $ar_group['ID']);
                        while ($arSectionPath = $nav->GetNext()) {
                            $sectionIDs[]['VALUE'] = $arSectionPath['ID'];
                        }
                    }
                    $arFields['PROPERTY_VALUES'][$direction['ID']] = $sectionIDs;

                    $resPropPrice = \CIBlockProperty::GetByID(SITE_PRICELIST_PROP, $arFields['IBLOCK_ID']);
                    if($arPropPrice = $resPropPrice->GetNext()) {
                        $resPrice = \CIBlockElement::GetList([], ['IBLOCK_ID' => SITE_PRICE_IBLOCK_ID, 'ACTIVE' => 'Y', 'ACTIVE_DATE' => 'Y', 'PROPERTY_SERVICE' => $arService['VALUE']]);
                        while ($obPrice = $resPrice->GetNextElement()) {
                            $arFieldsPrice = $obPrice->GetFields();
                            $priceIDs[]['VALUE'] = $arFieldsPrice['ID'];
                        }
                    }
                    $arFields['PROPERTY_VALUES'][$arPropPrice['ID']] = $priceIDs;
                }
            }
        }
        if($arFields['IBLOCK_ID'] == SITE_SPECIALISTS_IBLOCK_ID) {
            $resPropService = \CIBlockProperty::GetByID(SITE_SERVICE_PROP, $arFields['IBLOCK_ID']);
            if($arPropService = $resPropService->GetNext()) $service = $arPropService;

            $resPropDirection = \CIBlockProperty::GetByID(SITE_DIRECTION_PROP, $arFields['IBLOCK_ID']);
            if($arPropDirection = $resPropDirection->GetNext()) $direction = $arPropDirection;

            $sectionIDs = [];
            foreach ($arFields['PROPERTY_VALUES'][$service['ID']] as $key => $arService) {
                if ($arService['VALUE']) {
                    $nav = \CIBlockSection::GetNavChain(false, $arService['VALUE']);
                    while ($arSectionPath = $nav->GetNext()) {
                        $sectionIDs[]['VALUE'] = $arSectionPath['ID'];
                    }
                }
            }
            $arFields['PROPERTY_VALUES'][$direction['ID']] = $sectionIDs;
        }
    }

}