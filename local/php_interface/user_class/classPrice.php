<?

namespace IL;

/**
 * Класс для получения массива прайса
 * Class Locations
 * @package IL
 */
class Price {

    /**
     * @param $sectionIDs //массив разделов 1 уровня
     * @return mixed
     */
    public static function getPriceBySectionID($sectionIDs) {
        foreach ($sectionIDs as $section) {
            $resSubSection = \CIBlockSection::GetList([], ['IBLOCK_ID' => SITE_SERVICE_IBLOCK_ID, 'SECTION_ID' => $section, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y'], false, ['ID', 'NAME']);
            while ($rowSubSection = $resSubSection->GetNext()) {
                $priceArray[$rowSubSection['ID']] = $rowSubSection;
                $resElement = \CIBlockElement::GetList([], ['IBLOCK_ID' => SITE_SERVICE_IBLOCK_ID, 'IBLOCK_SECTION_ID' => $rowSubSection['ID'], 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y'], false, false, ['ID', 'NAME', 'DETAIL_PAGE_URL']);
                if ($resElement->SelectedRowsCount() > 0) {
                    while ($obElement = $resElement->GetNextElement()) {
                        $arFieldsElement = $obElement->GetFields();
                        $priceArray[$rowSubSection['ID']]['ELEMENTS'][$arFieldsElement['ID']] = $arFieldsElement;
                        $resPrice = \CIBlockElement::GetList([], ['IBLOCK_ID' => SITE_PRICE_IBLOCK_ID, 'PROPERTY_SERVICE' => $arFieldsElement['ID'], 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y'], false, false, ['ID', 'NAME', 'PROPERTY_PRICE', 'PROPERTY_OLD_PRICE']);
                        if ($resPrice->SelectedRowsCount() > 0) {
                            while ($obPrice = $resPrice->GetNextElement()) {
                                $arFieldsPrice = $obPrice->GetFields();
                                $priceArray[$rowSubSection['ID']]['ELEMENTS'][$arFieldsElement['ID']]['PRICE_LIST'][$arFieldsPrice['ID']] = $arFieldsPrice;
                            }
                        }
                        else {
                            unset($priceArray[$rowSubSection['ID']]['ELEMENTS'][$arFieldsElement['ID']]);
                        }
                    }
                }
            }
        }
        foreach ($priceArray as $key => $arPrice) {
            if (!is_array($arPrice['ELEMENTS']) || count($arPrice['ELEMENTS']) == 0) {
                unset($priceArray[$key]);
            }
        }
        return $priceArray;
    }

    /**
     * @param $iblockID //id ИБ
     * @return mixed
     */
    public static function getFullPrice($iblockID) {
        $res = \CIBlockSection::GetList([], ['IBLOCK_ID' => $iblockID, 'ACTIVE' => 'Y'], true, ['ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL'], false);
        while ($ob = $res->GetNext()) {
            if($ob['ELEMENT_CNT'] > 0 && $ob['DEPTH_LEVEL'] == 1) {
                $fullPrice[$ob['ID']] = $ob;
                $fullPrice[$ob['ID']]['SUBSECTIONS'] = self::getPriceBySectionID([$ob['ID']]);
            }
        }
        return $fullPrice;
    }
}