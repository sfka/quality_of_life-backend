<?

namespace IL;

use SoapClient;
use SoapFault;

/**
 * Class Soap
 * @package IL
 */
class Soap {

    protected $soapClient;

    private $wsdlServer;
    private $user;
    private $password;

    /**
     * Soap constructor.
     */
    public function __construct() {
        //ini_set("soap.wsdl_cache_enabled", "0");
        $this->wsdlServer = \Bitrix\Main\Config\Option::get("askaron.settings", "UF_SOAP_SERVER");
        $this->user = \Bitrix\Main\Config\Option::get("askaron.settings", "UF_SOAP_USER");
        $this->password = \Bitrix\Main\Config\Option::get("askaron.settings", "UF_SOAP_PASSWORD");
    }

    /**
     * Soap call
     * @param $wsdlPath
     * @param $wsdlMethod
     * @param array $params
     * @return mixed|string
     */
    public function soapCall($wsdlPath, $wsdlMethod, $params = []) {
        try {
            $this->soapClient = new SoapClient($this->wsdlServer . $wsdlPath, [
                'login' => $this->user,
                'password' => $this->password,
                'exceptions' => true,
                'trace' => true,
                'features' => SOAP_USE_XSI_ARRAY_TYPE,
            ]);
            $result = $this->soapClient->__soapCall($wsdlMethod, $params);
            return $result->return;
            //return $this->soapClient->__getFunctions();
        } catch (SoapFault $fault) {
            return $this->soapClient = 'ERROR: ' . $fault->faultcode . '-' . $fault->faultstring;
        }
    }

}