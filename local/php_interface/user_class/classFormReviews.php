<?

namespace IL;

use \Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;

/**
 * Класс для работы с формой отзывов
 * Class FormReviews
 * @package IL
 */
class FormReviews extends Iblock {

    /**
     * Добавляет элемент с данными из формы в ИБ
     * @param $arFields
     * @return bool|string
     * @throws LoaderException
     */
    public function add($arFields) {
        if (!Loader::includeModule('iblock')) return false;

        $arFiles = [];
        $el = new \CIBlockElement;
        $arLoadProductArray = [
            "IBLOCK_ID" => $this->iblockID,
            "DATE_ACTIVE_FROM" => date('d.m.Y H:i:s'),
            "PROPERTY_VALUES" => [
                'PHONE' => $arFields['phone'],
                'DATE' => $arFields['date'],
                'UTM_SOURCE' => $arFields['utm_source'],
                'UTM_MEDIUM' => $arFields['utm_medium'],
                'UTM_CAMPAIGN' => $arFields['utm_campaign'],
                'UTM_CONTENT' => $arFields['utm_content'],
                'UTM_TERM' => $arFields['utm_term'],
            ],
            "NAME" => $arFields['name'],
            "ACTIVE" => "N",
            "PREVIEW_TEXT" => $arFields['review'],
            "TYPE" => "html"
        ];
        if (isset($arFields['FILES'])) {
            for ($i = 0; $i < count($_FILES["photos"]['name']); $i++) {
                $error = false;
                $ext = pathinfo($_FILES["photos"]['name'][$i], PATHINFO_EXTENSION);
                $allowed = explode(', ', $arFields['type_file']);
                if (!in_array($ext, $allowed)) {
                    $error = true;
                } else {
                    $file = [
                        'name' => $_FILES["photos"]['name'][$i],
                        'size' => $_FILES["photos"]['size'][$i],
                        'tmp_name' => $_FILES["photos"]['tmp_name'][$i],
                        'type' => $_FILES["photos"]['type'][$i]
                    ];
                    $arFiles[] = [
                        'VALUE' => $file,
                        'DESCRIPTION' => ''
                    ];
                }
                if ($error) {
                    return 'Неверный тип файлов, требуется ' . $arFields['type_file'];
                }
                $arLoadProductArray["PROPERTY_VALUES"]["RESUME"] = $arFiles;
            }
        }
        $fieldText = '';
        foreach ($arLoadProductArray['PROPERTY_VALUES'] as $key => $values) {
            if ($values != '' && $key != 'PHOTO') {
                $resName = \CIBlockProperty::GetByID($key, $this->iblockID, false)->GetNext();
                $name = $resName['NAME'];
                $fieldText .= $name . ': ' . $values . '<br>';
            }
        }
        $fieldText .= 'Отзыв: ' . $arFields['review'] . '<br>';
        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            $arEventFields = [
                "FORM_NAME" => "Новый отзыв",
                "TEXT" => $fieldText
            ];
            \CEvent::Send("FORMS", SITE_ID, $arEventFields, 'N', 31, $_FILES["photos"]['tmp_name']);
            return true;
        } else {
            return $el->LAST_ERROR;
        }
    }

}