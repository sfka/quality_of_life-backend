<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/head.php", [], ["SHOW_BORDER" => false]); ?>
    <? $APPLICATION->ShowHead(); ?>
</head>
<body>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/header.php", [], ["SHOW_BORDER" => false]); ?>

    <div class="main-wrapper">
        <div class="main-wrapper__inner">
            <div class="inner-page">
                <div class="inner-page__top">
                    <div class="inner-page__top-inner">
                        <div class="common-container">
                            <div class="row">
                                <div class="col col-3 col-xl-3 col-lg-3">
                                    <div class="return-link-wrapper">
                                        <a class="return-link" href="#" onclick="checkReffer();">
                                            <svg class="arrowRightDefault" width="20" height="15">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                            </svg>
                                            <span class="return-link__title c-white-aluminum">Вернуться назад</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? if (CSite::InDir('/blog/') || CSite::InDir('/stock/') || CSite::InDir('/program/') || $_SERVER['REAL_FILE_PATH'] === '/services.php' || CSite::InDir('/problems/')): ?>
                    <? else: ?>
                        <div class="page-heading">
                            <div class="common-container">
                                <div class="row">
                                    <div class="col col-12">
                                        <div class="page-heading__title">
                                            <?= $APPLICATION->ShowTitle(false) ?>
                                        </div>
                                        <? $APPLICATION->ShowViewContent('header_desc'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endif ?>
                </div>
                <div class="inner-page__main">
                    <? $APPLICATION->ShowViewContent('detail_promo'); ?>
                    <div class="inner-page__container common-container">
                        <div class="inner-page__content<?= !\Bitrix\Main\Context::getCurrent()->getRequest()->isAjaxRequest() ? $APPLICATION->ShowViewContent('detail_promo_class') : '' ?>">