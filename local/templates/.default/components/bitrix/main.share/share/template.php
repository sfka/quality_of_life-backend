<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if ($arResult["PAGE_URL"]) : ?>
    <div class="share__list">
        <? if (is_array($arResult["BOOKMARKS"]) && count($arResult["BOOKMARKS"]) > 0) : ?>
            <? foreach (array_reverse($arResult["BOOKMARKS"]) as $name => $arBookmark): ?>
                <?= $arBookmark["ICON"] ?>
            <? endforeach; ?>
        <? endif; ?>
    </div>
<? else : ?>
    <?= GetMessage("SHARE_ERROR_EMPTY_SERVER") ?>
<? endif; ?>