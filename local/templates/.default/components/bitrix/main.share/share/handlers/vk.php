<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

__IncludeLang(dirname(__FILE__) . "/lang/" . LANGUAGE_ID . "/vk.php");
$name = "vk";
$title = GetMessage("BOOKMARK_HANDLER_VK");
$svg = '<svg class="vk" width="21" height="12"><use xlink:href="' . SITE_STYLE_PATH . '/img/general/svg-symbols.svg#vk"></use></svg>';
$icon_url_template = "
<a
	href=\"http://vkontakte.ru/share.php?url=#PAGE_URL_ENCODED#&title=#PAGE_TITLE_UTF_ENCODED#\"
	onclick=\"window.open(this.href,'','toolbar=0,status=0,width=626,height=436');return false;\"
	target=\"_blank\"
	class=\"share__list-item\"
	title=\"" . $title . "\"
>" . $svg . "</a>\n";
$sort = 300;
