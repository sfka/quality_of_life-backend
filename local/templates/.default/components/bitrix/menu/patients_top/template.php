<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
?>

<? if (!empty($arResult)): ?>
    <nav class="nav-menu nav-menu_main">
        <? foreach ($arResult as $arItem): ?>
            <? if ($arItem["DEPTH_LEVEL"] === 1) continue; ?>
            <div class="nav-menu__item">
                <a class="nav-menu__link<?= ($arItem["SELECTED"]) ? ' selected' : '' ?>" href="<?= $arItem["LINK"] ?>">
                    <?= $arItem["TEXT"] ?>
                </a>
            </div>
        <? endforeach ?>
    </nav>
<? endif ?>