<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
?>

<? if (!empty($arResult)): ?>
    <div class="show-more__list" data-simplebar="data-simplebar">
        <? foreach ($arResult as $arItem): ?>
            <? if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>
            <div class="show-more__list-item">
                <a class="show-more__list-link<?= ($arItem["SELECTED"]) ? ' active' : '' ?>" href="<?= $arItem["LINK"] ?>">
                    <?= $arItem["TEXT"] ?>
                </a>
            </div>
        <? endforeach ?>
    </div>
<? endif ?>