<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
?>

<? if (!empty($arResult)): ?>
    <section class="section s-for-patients">
        <div class="row section__top-row">
            <div class="col col-12">
                <div class="section__top-row-inner scrl fadeInUp">
                    <div class="section-title"><?= $arResult[0]['TEXT'] ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <? foreach ($arResult as $key => $arItem): ?>
                <? if ($key == 0) continue; ?>
                <div class="col col-3 col-sm-6 col-xs-12 scrl fadeInUp">
                    <a class="default-card page" href="<?=$arItem['LINK'] ?>">
                        <div class="default-card__inner">
                            <? if ($arItem['PARAMS']['ICO']): ?>
                                <div class="default-card__icon-wrapper">
                                    <div class="default-card__icon-bg bg-linen"></div>
                                    <svg class="<?= $arItem['PARAMS']['ICO'] ?>" width="90px" height="90px">
                                        <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#<?= str_replace("icon__", "", $arItem['PARAMS']['ICO']) ?>"></use>
                                    </svg>
                                </div>
                            <? endif; ?>
                            <div class="default-card__content">
                                <div class="default-card__title dotdotdot-text"><?= ($arItem['PARAMS']['TITLE'] ? $arItem['PARAMS']['TITLE'] : $arItem['TEXT']) ?></div>
                                <? if ($arItem['PARAMS']['DESCRIPTION']): ?>
                                    <p class="default-card__description c-white-aluminum"><?=$arItem['PARAMS']['DESCRIPTION'] ?></p>
                                <? endif; ?>
                                <div class="default-card__moving-arrow">
                                    <svg class="arrowRightLong" width="46" height="24">
                                        <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightLong"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </section>
<? endif ?>