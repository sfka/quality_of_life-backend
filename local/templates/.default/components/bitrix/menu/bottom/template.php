<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <nav class="footer-menu two-col">
        <? foreach ($arResult as $arItem): ?>
            <? if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>
            <div class="footer-menu__item">
                <a class="footer-menu__link<?= ($arItem["SELECTED"]) ? ' selected' : '' ?>" href="<?= $arItem["LINK"] ?>">
                    <?= $arItem["TEXT"] ?>
                </a>
            </div>
        <? endforeach ?>
    </nav>
<? endif ?>