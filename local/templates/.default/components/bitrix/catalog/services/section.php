<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arCurSection */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use \Bitrix\Main\Context;

$this->setFrameMode(true);

$sectionListParams = [
    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
    "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
    "CACHE_TIME" => $arParams["CACHE_TIME"],
    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
    "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
    "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
    "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
    "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
    "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
];
if ($sectionListParams["COUNT_ELEMENTS"] === "Y") {
    $sectionListParams["COUNT_ELEMENTS_FILTER"] = "CNT_ACTIVE";
    if ($arParams["HIDE_NOT_AVAILABLE"] == "Y") {
        $sectionListParams["COUNT_ELEMENTS_FILTER"] = "CNT_AVAILABLE";
    }
}

?>

<? $this->SetViewTarget('detail_promo_class'); ?> pb-less<? $this->EndViewTarget(); ?>

<? $this->SetViewTarget('detail_promo'); ?>
<div class="detail-promo">
    <div class="common-container">
        <div class="row">
            <div class="col col-6 col-sm-12 col-xs-12">
                <div class="detail-promo__col detail-promo__col_offset-mobile">
                    <div class="detail-promo__main-row sm-down-mb-0">
                        <div class="detail-promo__title">
                            <?= $arResult['SECTION']['NAME'] ?>
                        </div>
                        <div class="detail-promo__text">
                            <?= $arResult['SECTION']["UF_ANONS"] ?>
                        </div>
                        <div class="item hidden-lg-up">
                            <a class="other-pages-link other-pages-list-open" href=""
                               data-modal-class="other-pages-list-modal" data-type="right">
                                <span>Список услуг</span>
                                <svg class="three-dot" width="10" height="10">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#plus"></use>
                                </svg>
                            </a>
                            <div class="other-pages-list-modal modal__inner modal_right mfp-hide">
                                <button class="modal__close-btn js-modal-close">
                                    <svg class="icon__close" width="20" height="20">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                    </svg>

                                </button>
                                <div class="modal__content">
                                    <div class="modal__content-row">
                                        <div class="sidebar__inner">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:catalog.section.list",
                                                "service_detail_modal",
                                                $sectionListParams,
                                                $component,
                                                ["HIDE_ICONS" => "Y"]
                                            ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? if (!empty($arResult['SECTION']["UF_BENEFITS"])): ?>
                            <ul class="detail-promo__list">
                                <? foreach ($arResult['SECTION']["UF_BENEFITS"] as $arSectUF): ?>
                                    <li class="detail-promo__list-item">
                                        <?= $arSectUF ?>
                                    </li>
                                <? endforeach ?>
                            </ul>
                        <? endif ?>
                    </div>
                    <div class="detail-promo__bottom-row hidden-sm-down">
                        <a class="btn btn_primary" href="/appointments/?service=<?=$arResult['SECTION']['ID']?>">
                            <span>Запись на прием</span>
                        </a>
                    </div>
                </div>
            </div>
            <? if (!empty($arResult['SECTION']['PICTURE'])): ?>
                <div class="col col-6 col-sm-12 col-xs-12">
                    <div class="detail-promo__img-wrapper">
                        <img class="detail-promo__img lazy" data-src="<?= CFile::GetPath($arResult['SECTION']['PICTURE']) ?>">
                    </div>
                    <div class="detail-promo__bg-img" style="background-image: url(<?= CFile::GetPath($arResult['SECTION']['PICTURE']) ?>);"></div>
                </div>
            <? endif ?>
        </div>
    </div>
</div>
<? $this->EndViewTarget(); ?>

<div class="row">
    <div class="col col-3 col-xl-3 col-lg-3 hidden-md-down sidebar-col">
        <div class="sidebar-wrapper">
            <aside class="sidebar" data-simplebar>
                <div class="sidebar__inner">
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section.list",
                        "",
                        $sectionListParams,
                        $component,
                        ["HIDE_ICONS" => "Y"]
                    );
                    unset($sectionListParams);

                    // Необходимо для установки 404
                    $intSectionID = $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "clear",
                        [
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                            "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                            "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                            "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                            "PROPERTY_CODE" => (isset($arParams["LIST_PROPERTY_CODE"]) ? $arParams["LIST_PROPERTY_CODE"] : []),
                            "PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
                            "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                            "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                            "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                            "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                            "BASKET_URL" => $arParams["BASKET_URL"],
                            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                            "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                            "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                            "FILTER_NAME" => $arParams["FILTER_NAME"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "SET_TITLE" => $arParams["SET_TITLE"],
                            "MESSAGE_404" => $arParams["~MESSAGE_404"],
                            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                            "SHOW_404" => $arParams["SHOW_404"],
                            "FILE_404" => $arParams["FILE_404"],
                            "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                            "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
                            "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                            "PRICE_CODE" => $arParams["~PRICE_CODE"],
                            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                            "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                            "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                            "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                            "PRODUCT_PROPERTIES" => (isset($arParams["PRODUCT_PROPERTIES"]) ? $arParams["PRODUCT_PROPERTIES"] : []),

                            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                            "LAZY_LOAD" => $arParams["LAZY_LOAD"],
                            "MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
                            "LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

                            "OFFERS_CART_PROPERTIES" => (isset($arParams["OFFERS_CART_PROPERTIES"]) ? $arParams["OFFERS_CART_PROPERTIES"] : []),
                            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                            "OFFERS_PROPERTY_CODE" => (isset($arParams["LIST_OFFERS_PROPERTY_CODE"]) ? $arParams["LIST_OFFERS_PROPERTY_CODE"] : []),
                            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                            "OFFERS_LIMIT" => (isset($arParams["LIST_OFFERS_LIMIT"]) ? $arParams["LIST_OFFERS_LIMIT"] : 0),

                            "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                            "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                            "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                            "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                            'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

                            'LABEL_PROP' => $arParams['LABEL_PROP'],
                            'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                            'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                            'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
                            'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                            'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
                            'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
                            'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
                            'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                            'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                            'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

                            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                            'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
                            'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                            'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
                            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                            'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
                            'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                            'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
                            'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
                            'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                            'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                            'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
                            'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                            'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                            'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

                            'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                            'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                            'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

                            'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                            "ADD_SECTIONS_CHAIN" => "N",
                            'ADD_TO_BASKET_ACTION' => $basketAction,
                            'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                            'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                            'COMPARE_NAME' => $arParams['COMPARE_NAME'],
                            'USE_COMPARE_LIST' => 'Y',
                            'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                            'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
                            'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                        ],
                        $component
                    );
                    ?>
                    <? // Выведем проблемы и их решения ?>
                    <? if (!empty($arResult['PROBLEMS_SOLUTIONS'])): ?>
                        <div class="sidebar__col">
                            <div class="sidebar__title">Проблемы и их решения</div>
                            <div class="sidebar__nav sidebar__nav_unique">
                                <? foreach ($arResult['PROBLEMS_SOLUTIONS'] as $arProblem): ?>
                                    <a class="sidebar__nav-link" href="<?= $arProblem['DETAIL_PAGE_URL'] ?>">
                                        <?= $arProblem['NAME'] ?>
                                    </a>
                                <? endforeach ?>
                            </div>
                        </div>
                    <? endif ?>
                </div>
            </aside>
        </div>
    </div>
    <?$request = Context::getCurrent()->getRequest();
    $ajax = $request->isAjaxRequest();
    if (($ajax && $request->get('PRICE_ACC') == 'Y')) $APPLICATION->RestartBuffer(); ?>
    <div class="col col-8 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div class="content-container">
            <section class="section mb-less">
                <div class="content-container">
                    <?= $arResult['SECTION']["DESCRIPTION"] ?>
                    <div class="important-info">
                        <div class="important-info__inner">
                            <div class="important-info__title">Прейскурант</div>
                            <p class="important-info__description"><?$APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/specialist_detail_price.php') ?></p>
                        </div>
                    </div>
                </div>
            </section>
            <? // подключение акордеона прайса ?>
            <?$APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/price_accordeon.php', [
                'TITLE' => 'Прейскурант',
                "DOP_FILTER" => ($request->get('SECTION') ? $request->get('SECTION') : false),
                'PRICE_LIST_ARRAY' => $arResult['PRICE_LIST_ARRAY']], ['SHOW_BORDER' => false]); ?>
            <?
            // Выведем специалистов по услуге
            $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/specialists.php", [
                "TEMPLATE" => "specialists_slider",
                "FILTER" => ['PROPERTY_DIRECTION' => $arResult['SECTION']["ID"]],
            ], ["SHOW_BORDER" => false]);
            // Выведем вопросы-ответы по услуге
            $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/questions_answers.php", [
                "TEMPLATE" => "questions_answers_slider",
                "FILTER" => ['PROPERTY_DIRECTION' => $arResult['SECTION']["ID"]],
            ], ["SHOW_BORDER" => false]);
            ?>

            <?// подключение акции
            $res = \CIBlockElement::GetList(['DATE_ACTIVE_FROM' => 'DESC'], ['IBLOCK_ID' => SITE_STOCK_IBLOCK_ID, 'PROPERTY_DIRECTION' => $arResult['SECTION']['ID']], false, ['nPageSize' => 1], ['ID', 'NAME', 'DATE_ACTIVE_FROM', 'PREVIEW_TEXT'])->Fetch();
            $arResult['LAST_STOCK'] = $res;
            if ($arResult['LAST_STOCK']):?>
                <?$APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/last_stock_form.php', ['LAST_STOCK' => $arResult['LAST_STOCK']], ['SHOW_BORDER' => false]) ?>
            <?endif; ?>
        </div>
    </div>
    <? if (($ajax && $request->get('PRICE_ACC') == 'Y')) die(); ?>
</div>