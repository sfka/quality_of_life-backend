<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */

/** @var array $APPLICATION */

use \IL\Catalog;

// Получим информацию о разделе
$arResult['SECTION'] = \CIBlockSection::GetList([], [
    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
    'CODE' => $arResult['VARIABLES']['SECTION_CODE'],
], false, ['UF_*'], ['nPageSize' => 1])->Fetch();

// Получим "Проблемы и их решения"
if (empty($arResult['VARIABLES']['ELEMENT_CODE'])) {// Для раздела и его элементов
    $arElements = [];
    $arResSect = \CIBlockElement::GetList([], [
        "IBLOCK_ID" => $arParams['IBLOCK_ID'],
        'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
        "INCLUDE_SUBSECTIONS" => "Y",
    ], false, false, ['ID']);
    while ($arRes = $arResSect->GetNext()) {
        $arElements[] = $arRes['ID'];
    }

    $arProps = [];
    if (!empty($arElements)) {
        $res = \CIBlockElement::GetList([], [
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            'ID' => $arElements,
        ], false, false, [
            'ID',
            'IBLOCK_ID',
            'PROPERTY_PROBLEMS_SOLUTIONS',
        ]);

        while ($resEl = $res->GetNext()) {
            if (is_numeric($resEl['PROPERTY_PROBLEMS_SOLUTIONS_VALUE'])) {
                $arProps[] = $resEl['PROPERTY_PROBLEMS_SOLUTIONS_VALUE'];
            }
        }
    }
    if (!empty($arProps)) {
        $arResult['PROBLEMS_SOLUTIONS'] = array_unique($arProps);
    } else {
        if (!empty($arResult['SECTION']['UF_PROBLEMS'])) {
            $arPropsSect = [];
            $resSect = \CIBlockElement::GetList([], [
                'IBLOCK_ID' => \IL\Settings::PROBLEMS_SOLUTIONS_IBLOCK_ID,
                'SECTION_ID' => $arResult['SECTION']['UF_PROBLEMS'],
            ], false, false, [
                'ID',
            ]);
            while ($rsSect = $resSect->GetNext()) {
                $arPropsSect[] = $rsSect['ID'];
            }
            $arResult['PROBLEMS_SOLUTIONS'] = $arPropsSect;
        }
    }
} else {// Для элемента
    $res = \CIBlockElement::GetList([], [
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        'CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
    ], false, ['nPageSize' => 1], [])->GetNextElement();
    $arProps = [];
    if ($res) {
        $arProps = $res->GetProperties();
    }
    $arResult['PROBLEMS_SOLUTIONS'] = $arProps['PROBLEMS_SOLUTIONS']['VALUE'];
}
if (!empty($arResult['PROBLEMS_SOLUTIONS'])) {
    $arResult['PROBLEMS_SOLUTIONS'] = \IL\Catalog::getElementList(\IL\Settings::PROBLEMS_SOLUTIONS_IBLOCK_ID, ['ID' => $arResult['PROBLEMS_SOLUTIONS']]);
}
// Получим "Оборудования"
$arResult['EQUIPMENT'] = $arProps['EQUIPMENT']['VALUE'];
$arResult['EQUIPMENT'] = \IL\Catalog::getElementList(\IL\Settings::EQUIPMENT_IBLOCK_ID, ['ID' => $arResult['EQUIPMENT']]);

$sectionIDs[] = $arResult['SECTION']['ID'];

$arResult['PRICE_LIST_ARRAY'] = \IL\Price::getPriceBySectionID($sectionIDs);
