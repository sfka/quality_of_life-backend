<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest();
if (($ajax && $request->get('BEFORE_AFTER') == 'Y') || ($ajax && $request->get('REVIEWS') == 'Y') || ($ajax && $request->get('PRICE_ACC') == 'Y')) $APPLICATION->RestartBuffer();
?>
<div class="row">
    <div class="col col-3 col-xl-3 col-lg-3 hidden-md-down sidebar-col">
        <div class="sidebar-wrapper">
            <aside class="sidebar" data-simplebar>
                <div class="sidebar__inner">
                    <?
                    $nav = CIBlockSection::GetNavChain(false, $arResult['SECTION']['ID'], ['ID', 'CODE', 'NAME', 'DEPTH_LEVEL']);
                    while($arSectionPath = $nav->GetNext()){
                        if ($arSectionPath['DEPTH_LEVEL'] == 1) {
                            $sectionListParamsMain = $arSectionPath;
                        }
                    }
                    $sectionListParams = [
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "SECTION_ID" => $sectionListParamsMain['ID'],
                        "SECTION_CODE" => $sectionListParamsMain['CODE'],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                        "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
                        "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
                        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                        "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                        "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                        "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
                        "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
                    ];
                    if ($sectionListParams["COUNT_ELEMENTS"] === "Y") {
                        $sectionListParams["COUNT_ELEMENTS_FILTER"] = "CNT_ACTIVE";
                        if ($arParams["HIDE_NOT_AVAILABLE"] == "Y") {
                            $sectionListParams["COUNT_ELEMENTS_FILTER"] = "CNT_AVAILABLE";
                        }
                    }
                    $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section.list",
                        "",
                        $sectionListParams,
                        $component,
                        ["HIDE_ICONS" => "Y"]
                    );
                    //unset($sectionListParams);
                    ?>

                    <? // Выведем проблемы и их решения ?>
                    <? if (!empty($arResult['PROBLEMS_SOLUTIONS'])): ?>
                        <div class="sidebar__col">
                            <div class="sidebar__title">Проблемы и их решения</div>
                            <div class="sidebar__nav sidebar__nav_unique">
                                <? foreach ($arResult['PROBLEMS_SOLUTIONS'] as $arProblem): ?>
                                    <a class="sidebar__nav-link" href="<?= $arProblem['DETAIL_PAGE_URL'] ?>">
                                        <?= $arProblem['NAME'] ?>
                                    </a>
                                <? endforeach ?>
                            </div>
                        </div>
                    <? endif ?>
                </div>
            </aside>
        </div>
    </div>
    <div class="col col-8 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div class="content-container">
            <?
            if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y') {
                $basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? [$arParams['COMMON_ADD_TO_BASKET_ACTION']] : []);
            } else {
                $basketAction = (isset($arParams['DETAIL_ADD_TO_BASKET_ACTION']) ? $arParams['DETAIL_ADD_TO_BASKET_ACTION'] : []);
            }
            $componentElementParams = [
                'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                'PROPERTY_CODE' => (isset($arParams['DETAIL_PROPERTY_CODE']) ? $arParams['DETAIL_PROPERTY_CODE'] : []),
                'META_KEYWORDS' => $arParams['DETAIL_META_KEYWORDS'],
                'META_DESCRIPTION' => $arParams['DETAIL_META_DESCRIPTION'],
                'BROWSER_TITLE' => $arParams['DETAIL_BROWSER_TITLE'],
                'SET_CANONICAL_URL' => $arParams['DETAIL_SET_CANONICAL_URL'],
                'BASKET_URL' => $arParams['BASKET_URL'],
                'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
                'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
                'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
                'CHECK_SECTION_ID_VARIABLE' => (isset($arParams['DETAIL_CHECK_SECTION_ID_VARIABLE']) ? $arParams['DETAIL_CHECK_SECTION_ID_VARIABLE'] : ''),
                'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
                'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                'CACHE_TIME' => $arParams['CACHE_TIME'],
                'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                'SET_TITLE' => $arParams['SET_TITLE'],
                'SET_LAST_MODIFIED' => $arParams['SET_LAST_MODIFIED'],
                'MESSAGE_404' => $arParams['~MESSAGE_404'],
                'SET_STATUS_404' => $arParams['SET_STATUS_404'],
                'SHOW_404' => $arParams['SHOW_404'],
                'FILE_404' => $arParams['FILE_404'],
                'PRICE_CODE' => $arParams['~PRICE_CODE'],
                'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
                'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
                'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
                'PRICE_VAT_SHOW_VALUE' => $arParams['PRICE_VAT_SHOW_VALUE'],
                'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                'PRODUCT_PROPERTIES' => (isset($arParams['PRODUCT_PROPERTIES']) ? $arParams['PRODUCT_PROPERTIES'] : []),
                'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['ADD_PROPERTIES_TO_BASKET']) ? $arParams['ADD_PROPERTIES_TO_BASKET'] : ''),
                'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
                'LINK_IBLOCK_TYPE' => $arParams['LINK_IBLOCK_TYPE'],
                'LINK_IBLOCK_ID' => $arParams['LINK_IBLOCK_ID'],
                'LINK_PROPERTY_SID' => $arParams['LINK_PROPERTY_SID'],
                'LINK_ELEMENTS_URL' => $arParams['LINK_ELEMENTS_URL'],

                'OFFERS_CART_PROPERTIES' => (isset($arParams['OFFERS_CART_PROPERTIES']) ? $arParams['OFFERS_CART_PROPERTIES'] : []),
                'OFFERS_FIELD_CODE' => $arParams['DETAIL_OFFERS_FIELD_CODE'],
                'OFFERS_PROPERTY_CODE' => (isset($arParams['DETAIL_OFFERS_PROPERTY_CODE']) ? $arParams['DETAIL_OFFERS_PROPERTY_CODE'] : []),
                'OFFERS_SORT_FIELD' => $arParams['OFFERS_SORT_FIELD'],
                'OFFERS_SORT_ORDER' => $arParams['OFFERS_SORT_ORDER'],
                'OFFERS_SORT_FIELD2' => $arParams['OFFERS_SORT_FIELD2'],
                'OFFERS_SORT_ORDER2' => $arParams['OFFERS_SORT_ORDER2'],

                'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
                'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
                'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
                'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
                'SECTION_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['section'],
                'DETAIL_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['element'],
                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
                'HIDE_NOT_AVAILABLE_OFFERS' => $arParams['HIDE_NOT_AVAILABLE_OFFERS'],
                'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
                'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
                'USE_MAIN_ELEMENT_SECTION' => $arParams['USE_MAIN_ELEMENT_SECTION'],
                'STRICT_SECTION_CHECK' => (isset($arParams['DETAIL_STRICT_SECTION_CHECK']) ? $arParams['DETAIL_STRICT_SECTION_CHECK'] : ''),
                'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                'LABEL_PROP' => $arParams['LABEL_PROP'],
                'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
                'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                'DISCOUNT_PERCENT_POSITION' => (isset($arParams['DISCOUNT_PERCENT_POSITION']) ? $arParams['DISCOUNT_PERCENT_POSITION'] : ''),
                'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
                'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
                'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
                'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
                'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),
                'MESS_PRICE_RANGES_TITLE' => (isset($arParams['~MESS_PRICE_RANGES_TITLE']) ? $arParams['~MESS_PRICE_RANGES_TITLE'] : ''),
                'MESS_DESCRIPTION_TAB' => (isset($arParams['~MESS_DESCRIPTION_TAB']) ? $arParams['~MESS_DESCRIPTION_TAB'] : ''),
                'MESS_PROPERTIES_TAB' => (isset($arParams['~MESS_PROPERTIES_TAB']) ? $arParams['~MESS_PROPERTIES_TAB'] : ''),
                'MESS_COMMENTS_TAB' => (isset($arParams['~MESS_COMMENTS_TAB']) ? $arParams['~MESS_COMMENTS_TAB'] : ''),
                'MAIN_BLOCK_PROPERTY_CODE' => (isset($arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE']) ? $arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE'] : ''),
                'MAIN_BLOCK_OFFERS_PROPERTY_CODE' => (isset($arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE']) ? $arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE'] : ''),
                'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
                'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
                'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
                'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
                'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
                'BLOG_EMAIL_NOTIFY' => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
                'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
                'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
                'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
                'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
                'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
                'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
                'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
                'IMAGE_RESOLUTION' => (isset($arParams['DETAIL_IMAGE_RESOLUTION']) ? $arParams['DETAIL_IMAGE_RESOLUTION'] : ''),
                'PRODUCT_INFO_BLOCK_ORDER' => (isset($arParams['DETAIL_PRODUCT_INFO_BLOCK_ORDER']) ? $arParams['DETAIL_PRODUCT_INFO_BLOCK_ORDER'] : ''),
                'PRODUCT_PAY_BLOCK_ORDER' => (isset($arParams['DETAIL_PRODUCT_PAY_BLOCK_ORDER']) ? $arParams['DETAIL_PRODUCT_PAY_BLOCK_ORDER'] : ''),
                'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
                'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                'ADD_SECTIONS_CHAIN' => (isset($arParams['ADD_SECTIONS_CHAIN']) ? $arParams['ADD_SECTIONS_CHAIN'] : ''),
                'ADD_ELEMENT_CHAIN' => (isset($arParams['ADD_ELEMENT_CHAIN']) ? $arParams['ADD_ELEMENT_CHAIN'] : ''),
                'DISPLAY_PREVIEW_TEXT_MODE' => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
                'DETAIL_PICTURE_MODE' => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : []),
                'ADD_TO_BASKET_ACTION' => $basketAction,
                'ADD_TO_BASKET_ACTION_PRIMARY' => (isset($arParams['DETAIL_ADD_TO_BASKET_ACTION_PRIMARY']) ? $arParams['DETAIL_ADD_TO_BASKET_ACTION_PRIMARY'] : null),
                'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                'DISPLAY_COMPARE' => (isset($arParams['USE_COMPARE']) ? $arParams['USE_COMPARE'] : ''),
                'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                'USE_COMPARE_LIST' => 'Y',
                'BACKGROUND_IMAGE' => (isset($arParams['DETAIL_BACKGROUND_IMAGE']) ? $arParams['DETAIL_BACKGROUND_IMAGE'] : ''),
                'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
                'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                'SET_VIEWED_IN_COMPONENT' => (isset($arParams['DETAIL_SET_VIEWED_IN_COMPONENT']) ? $arParams['DETAIL_SET_VIEWED_IN_COMPONENT'] : ''),
                'SHOW_SLIDER' => (isset($arParams['DETAIL_SHOW_SLIDER']) ? $arParams['DETAIL_SHOW_SLIDER'] : ''),
                'SLIDER_INTERVAL' => (isset($arParams['DETAIL_SLIDER_INTERVAL']) ? $arParams['DETAIL_SLIDER_INTERVAL'] : ''),
                'SLIDER_PROGRESS' => (isset($arParams['DETAIL_SLIDER_PROGRESS']) ? $arParams['DETAIL_SLIDER_PROGRESS'] : ''),
                'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

                'USE_GIFTS_DETAIL' => $arParams['USE_GIFTS_DETAIL'] ?: 'Y',
                'USE_GIFTS_MAIN_PR_SECTION_LIST' => $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST'] ?: 'Y',
                'GIFTS_SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
                'GIFTS_SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
                'GIFTS_DETAIL_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
                'GIFTS_DETAIL_HIDE_BLOCK_TITLE' => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
                'GIFTS_DETAIL_TEXT_LABEL_GIFT' => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
                'GIFTS_DETAIL_BLOCK_TITLE' => $arParams['GIFTS_DETAIL_BLOCK_TITLE'],
                'GIFTS_SHOW_NAME' => $arParams['GIFTS_SHOW_NAME'],
                'GIFTS_SHOW_IMAGE' => $arParams['GIFTS_SHOW_IMAGE'],
                'GIFTS_MESS_BTN_BUY' => $arParams['~GIFTS_MESS_BTN_BUY'],
                'GIFTS_PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                'GIFTS_SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                'GIFTS_SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                'GIFTS_SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

                'GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
                'GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],
                'GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE'],
                "PRICE_SECTION" => ($request->get('PRICE_ACC') == 'Y' ? $request->get('SECTION') : false),
                "SECTIONlISTPARAMS" => $sectionListParams,
            ];
            $elementId = $APPLICATION->IncludeComponent(
                'bitrix:catalog.element',
                '',
                $componentElementParams,
                $component
            );
            $GLOBALS['CATALOG_CURRENT_ELEMENT_ID'] = $elementId;
            ?>

            <? // Выведем оборудование ?>
            <? if (!empty($arResult['EQUIPMENT'])): ?>
                <section class="section s-equipment-slider mb-less scrl fadeInUp">
                    <div class="row section__top-row">
                        <div class="col col-12">
                            <div class="section__top-row-inner">
                                <div class="section-title">Оборудование</div>
                                <div class="swiper-controls not-in-modal">
                                    <div class="swiper-buttons">
                                        <div class="swiper-button-prev">
                                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                            </svg>
                                        </div>
                                        <div class="swiper-button-next">
                                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-12">
                            <div class="default-slider">
                                <div class="swiper-container not-in-modal">
                                    <div class="swiper-wrapper">
                                        <? foreach ($arResult['EQUIPMENT'] as $arEquipment): ?>
                                            <div class="swiper-slide">
                                                <div class="equipment-card">
                                                    <a class="equipment-card__img-wrapper gallery-img" href="<?= CFile::GetPath($arEquipment['PREVIEW_PICTURE']) ?>">
                                                        <img class="equipment-card__img swiper-lazy" data-src="<?= CFile::GetPath($arEquipment['PREVIEW_PICTURE']) ?>" alt="<?= $arEquipment['NAME'] ?>"/>
                                                    </a>
                                                    <div class="equipment-card__content">
                                                        <div class="equipment-card__title">
                                                            <?= $arEquipment['NAME'] ?>
                                                        </div>
                                                        <?= $arEquipment['PREVIEW_TEXT'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endforeach ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-12 hidden-md-up">
                            <div class="mobile-swiper-controls">
                                <div class="swiper-controls not-in-modal">
                                    <div class="swiper-buttons">
                                        <div class="swiper-button-prev">
                                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                            </svg>
                                        </div>
                                        <div class="swiper-button-next">
                                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <? endif ?>

            <?
            // Выведем отзывы по услуге
            $elementInfo = \CIBlockElement::GetByID($elementId)->Fetch();
            $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/reviews.php", [
                "TEMPLATE" => "reviews_slider",
                "FILTER" => ['PROPERTY_SERVICE' => $elementInfo['IBLOCK_SECTION_ID']],
            ], ["SHOW_BORDER" => false]);
            unset($elementInfo);

            // Выведем вопросы-ответы по услуге
            $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/questions_answers.php", [
                "TEMPLATE" => "questions_answers_slider",
                "FILTER" => ['PROPERTY_DIRECTION' => $arResult['SECTION']["ID"]],
            ], ["SHOW_BORDER" => false]);
            ?>
            <?$APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/sign_up.php',
                ['BLOCK_TITLE' => 'Оставить заявку на услугу', 'BTN_TITLE' => 'Оставить заявку', 'FORM_ID' => 'feedback'], ['SHOW_BORDER' => false]) ?>
            
            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/contraindications_all.php", ['ELEMENT_ID' => $elementId], ["SHOW_BORDER" => false]); ?>

        </div>
    </div>
</div>
<div class="modal__inner modal_right modal-form mfp-hide" id="consultation">
    <button class="modal__close-btn js-modal-close">
        <svg class="icon__close" width="20" height="20">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </button>
    <div class="modal__content">
        <div class="modal__content-row">
            <div class="modal__title">Получить консультацию</div>
            <div class="modal__text">Администратор клиники свяжется с вами в ближайшее время</div>
        </div>
        <div class="modal__content-row">
            <form class="default-form" action="<?= SITE_AJAX_PATH ?>/form_consultation.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="stock" value="<?= $elementId ?>">
                <input type="hidden" name="utm_source" value="<?= $_GET['utm_source'] ?>">
                <input type="hidden" name="utm_medium" value="<?= $_GET['utm_medium'] ?>">
                <input type="hidden" name="utm_campaign" value="<?= $_GET['utm_campaign'] ?>">
                <input type="hidden" name="utm_content" value="<?= $_GET['utm_content'] ?>">
                <input type="hidden" name="utm_term" value="<?= $_GET['utm_term'] ?>">
                <div class="default-form__one-column">
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="text" name="name" required>
                        <span class="default-form__input-placeholder">Ваше имя *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="tel" name="phone" required>
                        <span class="default-form__input-placeholder">Ваш телефон *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                </div>
                <div class="default-form__btn-wrapper">
                    <button class="btn btn_primary">
                        <span>Отправить заявку</span>
                    </button>
                </div>
                <p class="default-form__text default-form__text_one-column">Я даю согласие на обработку,
                    <a href='/terms-of-use/'>персональных данных</a>
                </p>
            </form>
        </div>
    </div>
</div>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/form_modal.php', ['TITLE' => 'Оставить заявку', 'ELEMENT' => $elementId, 'ACTION' => SITE_AJAX_PATH . '/form_feedback.php', 'FORM_ID' => 'feedback'], ['SHOW_BORDER' => false]) ?>