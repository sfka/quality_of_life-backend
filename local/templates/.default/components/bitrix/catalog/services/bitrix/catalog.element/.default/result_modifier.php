<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();





function getPricelistElements()
{
    $arSelect = ["*", 'PROPERTY_PRICE', 'PROPERTY_OLD_PRICE', 'PROPERTY_SERVICE'];
    $arFilter = ["IBLOCK_ID" => '19', "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_SERVICE_VALUE" => $arResult['ID']];
    $res = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
    while ($element = $res->GetNext()) {
        $ibElements[$element['ID']] = $element;
        $services[$element['ID']][] = $element['PROPERTY_SERVICE_VALUE'];
    }

    foreach ($ibElements as $key => $value) {
        $ibElements[$value['ID']]['SERVICES'] = $services[$value['ID']];
    }

    return $ibElements;
}

function getServiceElements($arSectionsID)
{

    $pricelistElements = getPricelistElements();

    $arSelect = [];
    $arFilter = ["IBLOCK_ID" => '3', 'SECTION_ID' => $arSectionsID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"];
    $res = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
    while ($element = $res->GetNext()) {
        foreach ($pricelistElements as $key => $value) {
            foreach ($value['SERVICES'] as $sKey => $sValue) {
                if ($element['ID'] == $sValue) {
                    $element['PRICELIST'][] = $value;
                }
            }
        }

        $ibElements[$element['IBLOCK_SECTION_ID']][] = $element;

    }

    return $ibElements;
}

$arSelect = ["*"];
$arFilter = ["IBLOCK_ID" => 3, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_ID" => $arResult['SECTION']['PATH'][0]['ID']];
$res = CIBlockSection::GetList([], $arFilter, false, $arSelect, false);
while ($section = $res->GetNext()) {
    $ibSections[$section['ID']] = $section;
    $arSectionsID[] = $section['ID'];
}

$serviceElements = getServiceElements($arSectionsID);

foreach ($ibSections as $key => $value) {
    $ibSections[$value['ID']]['ELEMENTS'] = $serviceElements[$value['ID']];
}

foreach ($ibSections as $key => $value) {

    if (empty($value['IBLOCK_SECTION_ID'])) {
        $leftMenu[$value['ID']] = $value;

        $arItems[$value['ID']] = $value;
    }

}

foreach ($ibSections as $key => $value) {

    if (!empty($value['IBLOCK_SECTION_ID']) && !empty($value['ELEMENTS'])) {

        $arItems[$value['IBLOCK_SECTION_ID']]['SUBSECTIONS'][] = $value;

    }

}
$arResult['ITEMS'] = $arItems;

$res = \CIBlockElement::GetList(['DATE_ACTIVE_FROM' => 'DESC'], ['IBLOCK_ID' => SITE_STOCK_IBLOCK_ID, 'PROPERTY_SERVICE' => $arResult['ID']], false, ['nPageSize' => 1], ['ID', 'NAME', 'DATE_ACTIVE_FROM', 'PREVIEW_TEXT'])->Fetch();
$arResult['LAST_STOCK'] = $res;

$arResult['PRICE_LIST_ARRAY'] = \IL\Price::getPriceBySectionID([$arResult['SECTION']['PATH'][0]['ID']]);