<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(true);
?>
<? $this->SetViewTarget('detail_promo_class'); ?> pb-less<? $this->EndViewTarget(); ?>

<? $this->SetViewTarget('detail_promo'); ?>
<div class="detail-promo detail-promo_unique">
    <div class="common-container">
        <div class="row">
            <div class="col col-6 col-sm-12 col-xs-12">
                <div class="detail-promo__col">
                    <div class="detail-promo__main-row">
                        <div class="detail-promo__title">
                            <?= $arResult['NAME'] ?>
                        </div>
                        <?/*<div class="detail-promo__text">
                            <?= $arResult['PREVIEW_TEXT'] ?>
                        </div>*/?>
                        <div class="item hidden-lg-up">
                            <a class="other-pages-link other-pages-list-open" href=""
                               data-modal-class="other-pages-list-modal" data-type="right">
                                <span>Список услуг</span>
                                <svg class="three-dot" width="10" height="10">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#plus"></use>
                                </svg>
                            </a>
                            <div class="other-pages-list-modal modal__inner modal_right mfp-hide">
                                <button class="modal__close-btn js-modal-close">
                                    <svg class="icon__close" width="20" height="20">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                    </svg>
                                </button>
                                <div class="modal__content">
                                    <div class="modal__content-row">
                                        <div class="sidebar__inner">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:catalog.section.list",
                                                "service_detail_modal",
                                                $arParams['SECTIONlISTPARAMS'],
                                                $component,
                                                ["HIDE_ICONS" => "Y"]
                                            ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? if (!empty($arResult['PROPERTIES']['BENEFITS']['VALUE'])): ?>
                            <ul class="detail-promo__list">
                                <? foreach ($arResult['PROPERTIES']['BENEFITS']['VALUE'] as $arSectUF): ?>
                                    <li class="detail-promo__list-item">
                                        <?= $arSectUF ?>
                                    </li>
                                <? endforeach ?>
                            </ul>
                        <? endif ?>
                    </div>
                    <div class="detail-promo__bottom-row">
                        <a class="btn btn_primary" href="<?= preg_replace('~\D+~', '', \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE")) ?>">
                            <span>Позвонить</span>
                        </a>
                        <?/*<a class="btn btn_primary modal-open" href="#consultation">
                            <span>Получить консультацию</span>
                        </a>*/?>
                        <?/* if ($arResult['PRICE_LIST_ARRAY']): ?>
                            <a class="link anchor-link" href="#catalog">Прейскурант</a>
                        <? endif; */?>
                    </div>
                </div>
            </div>
            <? if (!empty($arResult['PREVIEW_PICTURE']['SRC'])): ?>
                <div class="col col-6 col-sm-12 col-xs-12">
                    <div class="detail-promo__img-wrapper">
                        <div class="detail-promo__mask"></div>
                        <div class="detail-promo__img" style="background-image: url(<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>);"></div>
                    </div>
                </div>
            <? endif ?>
        </div>
    </div>
</div>
<? $this->EndViewTarget(); ?>

<?// Выведем проблемы и решения ?>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/problems_solutions.php", [], ["SHOW_BORDER" => false]); ?>

<? // Выведем специалистов по услуге
$APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/specialists.php", [
    "TEMPLATE" => "specialists_slider",
    "FILTER" => ['PROPERTY_SERVICE' => $arResult['IBLOCK_SECTION_ID']],
], ["SHOW_BORDER" => false]);
?>


<? //Слайдер клиники ?>
<? if (!empty(\Bitrix\Main\Config\Option::get( "askaron.settings", "UF_CLINIC_SLIDER"))): ?>
    <section class="section s-photos-slider mb-less pt-0 scrl fadeInUp">
        <div class="row section__top-row">
            <div class="col col-12">
                <div class="section__top-row-inner">
                    <div class="section-title">Клиника «Качество Жизни»</div>
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-pagination"></div>
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-12">
                <div class="default-slider">
                    <div class="swiper-container not-in-modal">
                        <div class="swiper-wrapper">
                            <? foreach (\Bitrix\Main\Config\Option::get( "askaron.settings", "UF_CLINIC_SLIDER") as $keyPhoto => $arPhoto): ?>
                                <div class="swiper-slide">
                                    <img class="slider-photo-item swiper-lazy" data-src="<?= CFile::GetPath($arPhoto) ?>" alt="Клиника «Качество Жизни»"/>
                                </div>
                            <? endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-12 hidden-md-up">
                <div class="mobile-swiper-controls">
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-pagination"></div>
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif ?>

<? // подключение акции
if ($arResult['LAST_STOCK']):?>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/last_stock_form.php', ['LAST_STOCK' => $arResult['LAST_STOCK']], ['SHOW_BORDER' => false]) ?>
<? endif; ?>

<? //level 1
$directionID = [$arParams['SECTIONlISTPARAMS']['SECTION_ID'], $arResult['IBLOCK_SECTION_ID']];
//level 2
//$directionID = $arResult['IBLOCK_SECTION_ID']; ?>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/stock_slider.php', ['DIRECTION_ID' => $directionID], ['SHOW_BORDER' => true]) ?>

<?// Процесс работ ?>
<? if ($arResult['PROPERTIES']['WORK_STAGES']['~VALUE'] || $arResult['PROPERTIES']['RECOMMENDATIONS']['~VALUE']): ?>
    <section class="section mb-less">
        <div class="row section__top-row">
            <div class="col col-12">
                <div class="section__top-row-inner scrl fadeInUp">
                    <div class="section-title">Процесс работ</div>
                </div>
                <div class="section__top-row-inner scrl fadeInUp">
                    <div class="category-items">
                        <? if ($arResult['PROPERTIES']['WORK_STAGES']['~VALUE']): ?>
                            <div class="category-items__item js__category-items__item <?= ($arResult['PROPERTIES']['WORK_STAGES']['~VALUE'] ? 'active' : false) ?>"
                                 onclick="showTab('work_stage__tab'); this.classList.add('active');">
                                <span>Этапы работ</span>
                            </div>
                        <? endif; ?>
                        <? if ($arResult['PROPERTIES']['RECOMMENDATIONS']['~VALUE']): ?>
                            <div class="category-items__item js__category-items__item <?= ($arResult['PROPERTIES']['WORK_STAGES']['~VALUE'] ? false : 'active') ?>"
                                 onclick="showTab('recommendations__tab'); this.classList.add('active');">
                                <span>Рекомендации после</span>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row scrl fadeInUp">
            <div class="col col-12">
                <? if ($arResult['PROPERTIES']['WORK_STAGES']['~VALUE']): ?>
                    <div class="stage-items js__show_tab" id="work_stage__tab">
                        <? $countStage = 0; ?>
                        <? foreach ($arResult['PROPERTIES']['WORK_STAGES']['~VALUE'] as $key => $arStage): ?>
                            <? $countStage++; ?>
                            <div class="stage-items__item-wrapper">
                                <div class="stage-item">
                                    <div class="stage-item__row">
                                        <div class="stage-item__index"><?= ($countStage < 10 ? '0' . $countStage : $countStage) ?>
                                            .
                                        </div>
                                        <div class="stage-item__title"><?= $arResult['PROPERTIES']['WORK_STAGES']['DESCRIPTION'][$key] ?></div>
                                    </div>
                                    <div class="stage-item__description"><?= $arStage['TEXT'] ?></div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
                <div class="stage-items js__show_tab"
                     id="recommendations__tab"
                    <?= (empty($arResult['PROPERTIES']['WORK_STAGES']['~VALUE']) ? false : 'style="display: none"') ?> >
                    <? $countStage = 0; ?>
                    <? foreach ($arResult['PROPERTIES']['RECOMMENDATIONS']['~VALUE'] as $key => $arStage): ?>
                        <? $countStage++; ?>
                        <div class="stage-items__item-wrapper">
                            <div class="stage-item">
                                <div class="stage-item__row">
                                    <div class="stage-item__index"><?= ($countStage < 10 ? '0' . $countStage : $countStage) ?>
                                        .
                                    </div>
                                    <div class="stage-item__title"><?= $arResult['PROPERTIES']['RECOMMENDATIONS']['DESCRIPTION'][$key] ?></div>
                                </div>
                                <div class="stage-item__description"><?= $arStage['TEXT'] ?></div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<? endif; ?>

<?// Прайс-лист ?>
<? $mainParent = $arResult['SECTION']['PATH'][0]; ?>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/price_accordeon.php', [
    'TITLE' => 'Прейскурант',
    'PRICE_LIST_ARRAY' => $arResult['PRICE_LIST_ARRAY'],
    "DOP_FILTER" => ($arParams['PRICE_SECTION'] ? $arParams['PRICE_SECTION'] : $arResult['IBLOCK_SECTION_ID']),
    'ELEMENT_ACTIVE_ID' => $arResult['ID'],
    'BLOCK_ID' => 'catalog',
],
    ['SHOW_BORDER' => false]); ?>

<?// Описание с видео ?>
<? if (!empty($arResult['DETAIL_TEXT'])): ?>
    <section class="section mb-lesser">
        <div class="content-container">
            <?= $arResult['DETAIL_TEXT'] ?>
            <? if ($arResult['PROPERTIES']['VIDEO']['VALUE']): ?>
                <div class="detail-block">
                    <div class="detail-block__main-row">
                        <? if ($arResult['PROPERTIES']['VIDEO']['~VALUE']['TEXT']): ?>
                            <div class="detail-block__col"><?= $arResult['PROPERTIES']['VIDEO']['~VALUE']['TEXT'] ?></div>
                        <? endif; ?>
                        <? if ($arResult['PROPERTIES']['VIDEO']['DESCRIPTION']): ?>
                            <div class="detail-block__col">
                                <iframe class="modal__video"
                                        src="https://www.youtube.com/embed/<?= $arResult['PROPERTIES']['VIDEO']['DESCRIPTION'] ?>?enablejsapi=1&amp;amp;amp;version=3&amp;amp;amp;playerapiid=ytplayer"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen=""></iframe>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            <? endif; ?>
            <? if ($arResult['PROPERTIES']['AFTER_VIDEO']['~VALUE']['TEXT']): ?>
                <?= $arResult['PROPERTIES']['AFTER_VIDEO']['~VALUE']['TEXT'] ?>
            <? endif; ?>
        </div>
    </section>
<? endif ?>

<?// До и после ?>
<? $filter = ['ID' => $arResult['ID'], '!PROPERTY_BEFORE_AFTER' => false];
$APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/before_after.php", [
    "TEMPLATE" => "before_after_slider",
    "FILTER" => $filter,
    "DOP_FILTER" => ($arParams['BEFORE_AFTER_SECTION'] ? $arParams['BEFORE_AFTER_SECTION'] : false),
], ["SHOW_BORDER" => false]); ?>