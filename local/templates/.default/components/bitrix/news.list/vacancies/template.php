<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="section s-vacancy">
    <div class="row section__top-row">
        <div class="col col-10 col-md-12 col-sm-12 col-xs-12 offset-1 offset-md-0 offset-sm-0 offset-xs-0">
            <div class="section__top-row-inner scrl fadeInUp">
                <div class="section-title">Вакансии</div>
            </div>
        </div>
    </div>
    <div class="row scrl fadeInUp">
        <div class="col col-7 col-xl-6 col-lg-6 col-md-8 col-sm-12 col-xs-12 offset-1 offset-md-0 offset-sm-0 offset-xs-0">
            <div class="s-vacancy__list">
                <? foreach ($arResult['ITEMS'] as $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                    ?>
                    <div class="vacancy-card item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <div class="vacancy-card__title"><?= $arItem['NAME'] ?></div>
                        <div class="vacancy-card__text"><?= $arItem['PROPERTIES']['EXPERIENCE']['NAME'] ?>
                            :&nbsp;<?= $arItem['PROPERTIES']['EXPERIENCE']['VALUE'] ?></div>
                        <a class="arrow-link vacancy-info-open" href="javascript:void(0);" data-modal-class="vacancy-info-modal"
                           data-type="right"><span class="arrow-link__title">Подробнее</span></a>
                        <div class="vacancy-info-modal modal__inner modal_right modal-form mfp-hide">
                            <button class="modal__close-btn js-modal-close">
                                <svg class="icon__close" width="20" height="20">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                </svg>

                            </button>
                            <div class="modal__content">
                                <div class="modal__content-row">
                                    <div class="modal__title"><?= $arItem['NAME'] ?></div>
                                    <div class="modal__text"><?= $arItem['PROPERTIES']['EXPERIENCE']['NAME'] ?>
                                        :&nbsp;<?= $arItem['PROPERTIES']['EXPERIENCE']['VALUE'] ?></div>
                                </div>
                                <div class="modal__content-row">
                                    <div class="content-container">
                                        <b><?= $arItem['PROPERTIES']['REQUIREMENTS']['NAME'] ?>:</b>
                                        <?= $arItem['PROPERTIES']['REQUIREMENTS']['~VALUE']['TEXT'] ?>
                                        <b><?= $arItem['PROPERTIES']['DUTIES']['NAME'] ?>:</b>
                                        <?= $arItem['PROPERTIES']['DUTIES']['~VALUE']['TEXT'] ?>
                                        <b><?= $arItem['PROPERTIES']['TERMS']['NAME'] ?>:</b>
                                        <?= $arItem['PROPERTIES']['TERMS']['~VALUE']['TEXT'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <div class="col col-3 col-md-4 col-sm-12 col-xs-12 offset-xl-1 offset-lg-1 offset-md-0 offset-sm-0 offset-xs-0">
            <div class="modal-card-wrapper modal-card-wrapper_unique">
                <div class="modal-card">
                    <div class="modal-card__icon-wrapper">
                        <div class="modal-card__icon-bg bg-linen"></div>
                        <svg class="icon__find-work" width="90px" height="90px">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#find-work"></use>
                        </svg>

                    </div>
                    <div class="modal-card__title">Хотите работать в клинике «Качество жизни»?</div>
                    <a class="btn btn_primary modal-open" href="#vacancy-respond"><span>Откликнуться</span></a>
                </div>
            </div>
            <a class="service-link" href="<?= \Bitrix\Main\Config\Option::get( "askaron.settings", "UF_HH_LINK"); ?>">
                <svg class="icon__hh" width="40" height="40">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#hh"></use>
                </svg>
                <span>Посмотреть вакансии на HeadHunter</span>
            </a>
        </div>
    </div>
</section>
