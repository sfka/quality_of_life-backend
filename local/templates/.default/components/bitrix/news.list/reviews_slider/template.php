<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;

?>
<? if (count($arResult["ITEMS"]) > 0): ?>
    <section class="section s-reviews-slider mb-less scrl fadeInUp" id="reviews">
        <div class="row section__top-row">
            <div class="col col-12">
                <div class="section__top-row-inner">
                    <div class="section-title">Отзывы</div>
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-pagination"></div>
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <? if (count($arResult['UNIQUE_SECTION']) > 1): ?>
                    <div class="section__top-row-inner">
                        <div class="category-items">
                            <? foreach ($arResult['UNIQUE_SECTION'] as $key => $section): ?>
                                <div class="category-items__item" data-filter-slider="<?= $key ?>">
                                    <span><?= $section ?></span></div>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif ?>
            </div>
        </div>
        <div class="row">
            <div class="col col-12">
                <div class="default-slider">
                    <div class="swiper-container not-in-modal">
                        <div class="swiper-wrapper">
                            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                                <? if (!empty($arParams['DOP_FILTER']) && !in_array($arParams['DOP_FILTER'], $arItem['PROPERTIES']['SERVICE']['VALUE'])) {
                                    continue;
                                } ?>
                                <div class="swiper-slide">
                                    <div class="review-item item">
                                        <div class="review-item__top-row">
                                            <div class="review-item__heading">
                                                <?
                                                $svg = '';
                                                $classModal = '';
                                                if (!empty($arItem['PROPERTIES']['VIDEO']['VALUE'])) {
                                                    $svg = 'play';
                                                    $classModal = 'video';
                                                } elseif (!empty($arItem['PROPERTIES']['PHOTO']['VALUE'])) {
                                                    $svg = 'photo';
                                                    $classModal = 'photo';
                                                }
                                                $thumb = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], [
                                                    'width' => 64,
                                                    'height' => 64,
                                                ], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []);
                                                ?>
                                                <? if (!empty($svg)): ?>
                                                    <a class="review-item__img review-<?= $classModal ?>-open"
                                                       data-modal-class="review-<?= $classModal ?>-modal"
                                                       data-type="center" href="javascript:void(0);"
                                                       style="background: url(<?= $thumb['src'] ?>) no-repeat center center">
                                                        <div class="review-item__img-mask"></div>
                                                        <svg class="photo" width="26" height="20">
                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#<?= $svg ?>"></use>
                                                        </svg>
                                                    </a>
                                                    <? if (!empty($arItem['PROPERTIES']['VIDEO']['VALUE'])): ?>
                                                        <div class="review-video-modal modal__inner modal_center mfp-hide">
                                                            <button class="modal__close-btn js-modal-close">
                                                                <svg class="icon__close" width="24" height="24">
                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                                </svg>
                                                            </button>
                                                            <div class="modal__content">
                                                                <div class="row modal__top-row">
                                                                    <div class="col col-12">
                                                                        <div class="modal__top-row-inner">
                                                                            <div class="modal__title modal__title_big">
                                                                                Видео
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row modal__row">
                                                                    <div class="col col-8 col-sm-12 col-xs-12">
                                                                        <iframe width="100%" height="360" class="modal__video"
                                                                                src="https://www.youtube.com/embed/<?= $arItem['PROPERTIES']['VIDEO']['VALUE'] ?>?enablejsapi=1&amp;version=3&amp;playerapiid=ytplayer"
                                                                                frameborder="0"
                                                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                                                allowfullscreen></iframe>
                                                                    </div>
                                                                    <div class="col col-4 col-sm-12 col-xs-12">
                                                                        <? if (!empty($arItem['PROPERTIES']['COMMENT']['VALUE']['TEXT'])): ?>
                                                                            <div class="answer-item">
                                                                                <div class="answer-item__title">
                                                                                    <?= $arItem['PROPERTIES']['COMMENT']['NAME'] ?>
                                                                                    :
                                                                                </div>
                                                                                <p class="answer-item__description active">
                                                                                    <?= $arItem['PROPERTIES']['COMMENT']['~VALUE']['TEXT'] ?>
                                                                                </p>
                                                                            </div>
                                                                        <? endif ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <? elseif (!empty($arItem['PROPERTIES']['PHOTO']['VALUE'])): ?>
                                                        <div class="review-photo-modal modal__inner modal_center mfp-hide">
                                                            <button class="modal__close-btn js-modal-close">
                                                                <svg class="icon__close" width="24" height="24">
                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                                </svg>
                                                            </button>
                                                            <div class="modal__content modal-before-after-slider">
                                                                <div class="row modal__top-row">
                                                                    <div class="col col-12">
                                                                        <div class="modal__top-row-inner">
                                                                            <div class="modal__title modal__title_big">
                                                                                Фотографии
                                                                            </div>
                                                                            <div class="swiper-controls">
                                                                                <div class="swiper-pagination"></div>
                                                                                <div class="swiper-buttons">
                                                                                    <div class="swiper-button-prev">
                                                                                        <svg class="icon__arrowRightDefault"
                                                                                             width="32" height="24">
                                                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <div class="swiper-button-next">
                                                                                        <svg class="icon__arrowRightDefault"
                                                                                             width="32" height="24">
                                                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row modal__main-row">
                                                                    <div class="col col-8 col-sm-12 col-xs-12">
                                                                        <div class="default-slider">
                                                                            <div class="swiper-container">
                                                                                <div class="swiper-wrapper">
                                                                                    <? foreach ($arItem['PROPERTIES']['PHOTO']['VALUE'] as $keyPhoto => $arPhoto): ?>
                                                                                        <div class="swiper-slide">
                                                                                            <img class="before-after-item__img swiper-lazy"
                                                                                                 data-src="<?= \CFile::GetPath($arPhoto) ?>"
                                                                                                 alt="<?= $arItem['PROPERTIES']['PHOTO']['DESCRIPTION'][$keyPhoto] ?>">
                                                                                        </div>
                                                                                    <? endforeach ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-4 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col col-12 hidden-md-up">
                                                                                <div class="mobile-swiper-controls">
                                                                                    <div class="swiper-controls">
                                                                                        <div class="swiper-pagination"></div>
                                                                                        <div class="swiper-buttons">
                                                                                            <div class="swiper-button-prev">
                                                                                                <svg class="icon__arrowRightDefault"
                                                                                                     width="32"
                                                                                                     height="24">
                                                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                                                </svg>
                                                                                            </div>
                                                                                            <div class="swiper-button-next">
                                                                                                <svg class="icon__arrowRightDefault"
                                                                                                     width="32"
                                                                                                     height="24">
                                                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                                                </svg>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <? if (!empty($arItem['PROPERTIES']['COMMENT']['VALUE']['TEXT'])): ?>
                                                                            <div class="answer-item">
                                                                                <div class="answer-item__title">
                                                                                    <?= $arItem['PROPERTIES']['COMMENT']['NAME'] ?>
                                                                                    :
                                                                                </div>
                                                                                <p class="answer-item__description active">
                                                                                    <?= $arItem['PROPERTIES']['COMMENT']['~VALUE']['TEXT'] ?>
                                                                                </p>
                                                                            </div>
                                                                        <? endif ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <? endif ?>
                                                <? endif ?>
                                                <div class="review-item__row">
                                                    <div class="review-item__info">
                                                        <div class="review-item__name c-gray-umber">
                                                            <?= $arItem['NAME'] ?>
                                                        </div>
                                                        <? if (!empty($arItem['PROPERTIES']['WALK']['VALUE'])): ?>
                                                            <div class="review-item__position c-white-aluminum">
                                                                <?= $arItem['PROPERTIES']['WALK']['VALUE'] ?>
                                                            </div>
                                                        <? endif ?>
                                                    </div>
                                                    <div class="review-item__col">
                                                        <div class="review-item__category c-white-aluminum">
                                                            <?= Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['SERVICE']['VALUE'][0]) ?>
                                                        </div>
                                                        <? if (count($arItem['PROPERTIES']['SERVICE']['VALUE']) > 1): ?>
                                                            <button class="show-more">
                                                                <div class="show-more__btn">
                                                                    <svg class="three-dot" width="2" height="10">
                                                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#three-dot"></use>
                                                                    </svg>
                                                                    <span>Еще</span>
                                                                </div>
                                                                <div class="show-more__list"
                                                                     data-simplebar="data-simplebar">
                                                                    <? foreach ($arItem['PROPERTIES']['SERVICE']['VALUE'] as $keyService => $arService): ?>
                                                                        <? if ($keyService === 0) continue ?>
                                                                        <div class="show-more__list-item">
                                                                            <span>
                                                                                <?= Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arService) ?>
                                                                            </span>
                                                                        </div>
                                                                    <? endforeach ?>
                                                                </div>
                                                            </button>
                                                        <? endif ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item__text-wrapper">
                                                <p class="review-item__review item__text c-gray-umber">
                                                    <?= $arItem['PREVIEW_TEXT'] ?>
                                                </p>
                                                <span class="review-item__show-all-text item__show-all-text c-carmine-pink">
                                                <span>Показать полностью</span>
                                                <span>Свернуть</span>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="review-item__main-row<?= empty($arItem['DETAIL_TEXT']) ? ' border-0' : '' ?>">
                                            <div class="review-item__list">
                                                <? if (!empty($arItem['PROPERTIES']['SPECIALIST']['VALUE'])): ?>
                                                    <div class="review-item__list-item">
                                                        <div class="review-item__list-col">
                                                            <div class="review-item__list-heading">Специалист:</div>
                                                        </div>
                                                        <div class="review-item__list-col">
                                                            <div class="review-item__list-value">
                                                                <?= preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1 $2.$3.', Catalog::getElementNameByID($arItem['PROPERTIES']['SPECIALIST']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['SPECIALIST']['VALUE'])) ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endif ?>
                                                <? if (!empty($arItem['PROPERTIES']['PROBLEM']['VALUE'])): ?>
                                                    <div class="review-item__list-item">
                                                        <div class="review-item__list-col">
                                                            <div class="review-item__list-heading">
                                                                <?= $arItem['PROPERTIES']['PROBLEM']['NAME'] ?>:
                                                            </div>
                                                        </div>
                                                        <div class="review-item__list-col">
                                                            <div class="review-item__list-value">
                                                                <?= $arItem['PROPERTIES']['PROBLEM']['VALUE'] ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endif ?>
                                                <? if (!empty($arItem['PROPERTIES']['OPERATION']['VALUE'])): ?>
                                                    <div class="review-item__list-item">
                                                        <div class="review-item__list-col">
                                                            <div class="review-item__list-heading">
                                                                <?= $arItem['PROPERTIES']['OPERATION']['NAME'] ?>:
                                                            </div>
                                                        </div>
                                                        <div class="review-item__list-col">
                                                            <div class="review-item__list-value">
                                                                <?= $arItem['PROPERTIES']['OPERATION']['VALUE'][0] ?>
                                                            </div>
                                                            <? if (count($arItem['PROPERTIES']['OPERATION']['VALUE']) > 1): ?>
                                                                <button class="show-more">
                                                                    <div class="show-more__btn">
                                                                        <svg class="three-dot" width="2" height="10">
                                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#three-dot"></use>
                                                                        </svg>
                                                                        <span>Еще</span>
                                                                    </div>
                                                                    <div class="show-more__list"
                                                                         data-simplebar="data-simplebar">
                                                                        <? foreach ($arItem['PROPERTIES']['OPERATION']['VALUE'] as $keyOperations => $arOperations): ?>
                                                                            <? if ($keyOperations === 0) continue ?>
                                                                            <div class="show-more__list-item">
                                                                                <span>
                                                                                    <?= $arOperations ?>
                                                                                </span>
                                                                            </div>
                                                                        <? endforeach ?>
                                                                    </div>
                                                                </button>
                                                            <? endif ?>
                                                        </div>
                                                    </div>
                                                <? endif ?>
                                                <? if (!empty($arItem['PROPERTIES']['SOURCE']['VALUE'])): ?>
                                                    <div class="review-item__list-item">
                                                        <div class="review-item__list-col">
                                                            <div class="review-item__list-heading">
                                                                <?= $arItem['PROPERTIES']['SOURCE']['NAME'] ?>:
                                                            </div>
                                                        </div>
                                                        <div class="review-item__list-col">
                                                            <div class="review-item__list-value">
                                                                <?= $arItem['PROPERTIES']['SOURCE']['VALUE'] ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endif ?>
                                            </div>
                                        </div>
                                        <? if (!empty($arItem['DETAIL_TEXT'])): ?>
                                            <div class="review-item__bottom-row">
                                                <a class="arrow-link answer-modal-open" href="javascript:void(0);"
                                                   data-modal-class="answer-modal" data-type="right">
                                                    <span class="arrow-link__title">Ответ клиники</span>
                                                </a>
                                                <div class="answer-modal modal__inner modal_right mfp-hide">
                                                    <button class="modal__close-btn js-modal-close">
                                                        <svg class="icon__close" width="20" height="20">
                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                        </svg>
                                                    </button>
                                                    <div class="modal__content">
                                                        <div class="modal__content-row">
                                                            <div class="modal__title">Ответ клиники</div>
                                                            <? if (!empty($arItem['PROPERTIES']['SERVICE']['VALUE'])): ?>
                                                                <div class="modal__text">
                                                                    <? foreach ($arItem['PROPERTIES']['SERVICE']['VALUE'] as $keyService => $arService): ?>
                                                                        <span>
                                                                            <? if ($keyService === 0): ?>
                                                                                <?= Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arService) ?><?= (count($arItem['PROPERTIES']['SERVICE']['VALUE']) > 1) ? ', ' : '' ?>
                                                                            <? else: ?>
                                                                                <?= mb_strtolower(Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arService)) ?>
                                                                            <? endif ?>
                                                                        </span>
                                                                    <? endforeach ?>
                                                                </div>
                                                            <? endif ?>
                                                        </div>
                                                        <div class="modal__content-row modal__content-row_unique">
                                                            <p class="modal__paragraph">
                                                                <?= $arItem['DETAIL_TEXT'] ?>
                                                            </p>
                                                        </div>
                                                        <? if (!empty($arItem['PROPERTIES']['RELATED_MATERIALS']['VALUE'])): ?>
                                                            <div class="modal__content-row">
                                                                <div class="modal__subtitle">
                                                                    <?= $arItem['PROPERTIES']['RELATED_MATERIALS']['NAME'] ?>
                                                                    :
                                                                </div>
                                                                <div class="modal__nav">
                                                                    <? foreach ($arItem['PROPERTIES']['RELATED_MATERIALS']['VALUE'] as $keyRelated => $arRelated): ?>
                                                                        <a class="modal__nav-link"
                                                                           href="<?= $arItem['PROPERTIES']['RELATED_MATERIALS']['DESCRIPTION'][$keyRelated] ?>">
                                                                            <?= $arRelated ?>
                                                                        </a>
                                                                    <? endforeach ?>
                                                                </div>
                                                            </div>
                                                        <? endif ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif ?>
                                    </div>
                                </div>
                            <? endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-12 hidden-md-up">
                <div class="mobile-swiper-controls">
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-pagination"></div>
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif ?>