<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arResult['UNIQUE_SECTION'] = [];
foreach ($arParams['SECTION_FILTER']['PROPERTY_SERVICE'] as $sectionID) {
    $res = \CIBlockSection::GetByID($sectionID);
    if ($ar_res = $res->GetNext()) {
        if ($ar_res['DEPTH_LEVEL'] == 2) {
            $arResult['UNIQUE_SECTION'][$ar_res['ID']] = $ar_res['NAME'];
        }
    }
}
if (empty($arParams['SECTION_FILTER']['PROPERTY_SERVICE'])) {
    foreach ($arResult['ITEMS'] as $arItem) {
        foreach ($arItem['PROPERTIES']['SERVICE']['VALUE'] as $service) {
            $res = \CIBlockSection::GetByID($service);
            if ($ar_res = $res->GetNext()) {
                if ($ar_res['DEPTH_LEVEL'] == 2) {
                    $arResult['UNIQUE_SECTION'][$service] = $ar_res['NAME'];
                }
            }
        }
    }
}