<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;
use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest();
?>

<? if (!$ajax): ?>
    <? $this->SetViewTarget('left_tags'); ?>
    <div class="sidebar__tags-wrapper">
        <div class="sidebar__col">
            <div class="sidebar__title">Направления деятельности</div>
            <div class="sidebar__tags">
                <? foreach ($arResult['LEFT_MENU']['SECTIONS'] as $arDirections): ?>
                    <span class="sidebar__tags-item" data-direction="<?= $arDirections['ID'] ?>">
                        <span>#<?= mb_strtolower($arDirections['NAME']) ?></span>
                    </span>
                <? endforeach ?>
            </div>
        </div>
        <? if (!empty($arResult['LEFT_MENU']['TAGS'])): ?>
            <div class="sidebar__col">
                <div class="sidebar__title">Теги</div>
                <div class="sidebar__tags">
                    <? foreach ($arResult['LEFT_MENU']['TAGS'] as $arTags): ?>
                        <span class="sidebar__tags-item" data-section="<?= $arTags['ID'] ?>">
                            <span>#<?= mb_strtolower($arTags['NAME']) ?></span>
                        </span>
                    <? endforeach ?>
                </div>
            </div>
        <? endif ?>
    </div>
    <? $this->EndViewTarget(); ?>
<? endif ?>

<div class="col col-9 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
    <div class="hidden-lg-up">
        <div class="mobile-tags-select-wrapper">
            <div class="nice-select-wrapper">
                <div class="nice-select-wrapper__placeholder">Выбрать по тегу</div>
                <select class="nice-select ajax__filter" required>
                    <? foreach ($arResult['LEFT_MENU']['SECTIONS'] as $arDirections): ?>
                        <option data-direction="<?= $arDirections['ID'] ?>">#<?= mb_strtolower($arDirections['NAME']) ?></option>
                    <? endforeach ?>
                    <? foreach ($arResult['LEFT_MENU']['TAGS'] as $arTags): ?>
                        <option data-section="<?= $arTags['ID'] ?>">#<?= mb_strtolower($arTags['NAME']) ?></option>
                    <? endforeach ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-7 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="dynamic-content-wrapper width-fix">
                <div class="dynamic-content">
                    <? if ($ajax && (!empty($request->get('DIRECTION')) || !empty($request->get('SECTION'))) && empty($request->get('PAGEN_1'))): ?>
                        <div class="dynamic-content__heading">
                            <div class="dynamic-content__heading-row">
                                <div class="dynamic-content__title c-gray-umber">
                                    <? if (!empty($request->get('DIRECTION'))): ?>
                                        #<?= mb_strtolower(Catalog::getSectionNameByID($arResult['ITEMS'][0]['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], $request->get('DIRECTION'))); ?>
                                    <? elseif ($request->get('SECTION')): ?>
                                        #<?= mb_strtolower(Catalog::getSectionNameByID($arParams['IBLOCK_ID'], $request->get('SECTION'))); ?>
                                    <? endif ?>
                                </div>
                            </div>
                            <? if (!empty($arResult['SERVICE'])): ?>
                                <div class="dynamic-content__heading-row">
                                    <div class="category-items">
                                        <? foreach ($arResult['SERVICE'] as $arService): ?>
                                            <div class="category-items__item<?= ($arService['ID'] == $request->get('SERVICE')) ? ' active' : '' ?>"
                                                 data-direction="<?= $arResult['ITEMS'][0]['PROPERTIES']['DIRECTION']['VALUE'] ?>"
                                                 data-service="<?= $arService['ID'] ?>">
                                                <span><?= $arService['NAME'] ?></span>
                                            </div>
                                        <? endforeach ?>
                                    </div>
                                </div>
                            <? endif ?>
                        </div>
                    <? endif ?>
                    <div class="dynamic-content__main ajax_list">
                        <? foreach ($arResult['ITEMS'] as $arItem): ?>
                            <div class="review-item item ajax_item">
                                <div class="review-item__top-row">
                                    <div class="review-item__direction">
                                        <? if (!empty($arItem['IBLOCK_SECTION_ID'])): ?>
                                            #<?= mb_strtolower(Catalog::getSectionNameByID($arParams['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID'])); ?>
                                        <? else: ?>
                                            #<?= mb_strtolower(Catalog::getSectionNameByID($arItem['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['DIRECTION']['VALUE'])); ?>
                                        <? endif ?>
                                    </div>
                                    <div class="review-item__heading">
                                        <?
                                        $svg = '';
                                        $classModal = '';
                                        if (!empty($arItem['PROPERTIES']['VIDEO']['VALUE'])) {
                                            $svg = 'play';
                                            $classModal = 'video';
                                        } elseif (!empty($arItem['PROPERTIES']['PHOTO']['VALUE'])) {
                                            $svg = 'photo';
                                            $classModal = 'photo';
                                        }
                                        $thumb = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], [
                                            'width' => 64,
                                            'height' => 64,
                                        ], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []);
                                        ?>
                                        <? if (!empty($svg)): ?>
                                            <a class="review-item__img review-<?= $classModal ?>-open" data-modal-class="review-<?= $classModal ?>-modal" data-type="center" href="javascript:void(0);" style="background: url(<?= $thumb['src'] ?>) no-repeat center center">
                                                <div class="review-item__img-mask"></div>
                                                <svg class="photo" width="26" height="20">
                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#<?= $svg ?>"></use>
                                                </svg>
                                            </a>
                                            <? if (!empty($arItem['PROPERTIES']['VIDEO']['VALUE'])): ?>
                                                <div class="review-video-modal modal__inner modal_center mfp-hide">
                                                    <button class="modal__close-btn js-modal-close">
                                                        <svg class="icon__close" width="24" height="24">
                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                        </svg>
                                                    </button>
                                                    <div class="modal__content">
                                                        <div class="row modal__top-row">
                                                            <div class="col col-12">
                                                                <div class="modal__top-row-inner">
                                                                    <div class="modal__title modal__title_big">Видео</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row modal__row">
                                                            <div class="col col-8 col-sm-12 col-xs-12">
                                                                <iframe width="100%" height="360" class="modal__video"
                                                                        src="https://www.youtube.com/embed/<?= $arItem['PROPERTIES']['VIDEO']['VALUE'] ?>?enablejsapi=1&amp;version=3&amp;playerapiid=ytplayer"
                                                                        frameborder="0"
                                                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                                        allowfullscreen></iframe>
                                                            </div>
                                                            <div class="col col-4 col-sm-12 col-xs-12">
                                                                <? if (!empty($arItem['PROPERTIES']['COMMENT']['VALUE']['TEXT'])): ?>
                                                                    <div class="answer-item">
                                                                        <div class="answer-item__title">
                                                                            <?= $arItem['PROPERTIES']['COMMENT']['NAME'] ?>:
                                                                        </div>
                                                                        <p class="answer-item__description active">
                                                                            <?= $arItem['PROPERTIES']['COMMENT']['~VALUE']['TEXT'] ?>
                                                                        </p>
                                                                    </div>
                                                                <? endif ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? elseif (!empty($arItem['PROPERTIES']['PHOTO']['VALUE'])): ?>
                                                <div class="review-photo-modal modal__inner modal_center mfp-hide">
                                                    <button class="modal__close-btn js-modal-close">
                                                        <svg class="icon__close" width="24" height="24">
                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                        </svg>
                                                    </button>
                                                    <div class="modal__content modal-before-after-slider">
                                                        <div class="row modal__top-row">
                                                            <div class="col col-12">
                                                                <div class="modal__top-row-inner">
                                                                    <div class="modal__title modal__title_big">Фотографии</div>
                                                                    <div class="swiper-controls">
                                                                        <div class="swiper-pagination"></div>
                                                                        <div class="swiper-buttons">
                                                                            <div class="swiper-button-prev">
                                                                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                                </svg>
                                                                            </div>
                                                                            <div class="swiper-button-next">
                                                                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                                </svg>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row modal__main-row">
                                                            <div class="col col-8 col-sm-12 col-xs-12">
                                                                <div class="default-slider">
                                                                    <div class="swiper-container">
                                                                        <div class="swiper-wrapper">
                                                                            <? foreach ($arItem['PROPERTIES']['PHOTO']['VALUE'] as $keyPhoto => $arPhoto): ?>
                                                                                <div class="swiper-slide">
                                                                                    <img class="before-after-item__img swiper-lazy" data-src="<?= \CFile::GetPath($arPhoto) ?>" alt="<?= $arItem['PROPERTIES']['PHOTO']['DESCRIPTION'][$keyPhoto] ?>">
                                                                                </div>
                                                                            <? endforeach ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col col-4 col-sm-12 col-xs-12">
                                                                <div class="row">
                                                                    <div class="col col-12 hidden-md-up">
                                                                        <div class="mobile-swiper-controls">
                                                                            <div class="swiper-controls">
                                                                                <div class="swiper-pagination"></div>
                                                                                <div class="swiper-buttons">
                                                                                    <div class="swiper-button-prev">
                                                                                        <svg class="icon__arrowRightDefault" width="32" height="24">
                                                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <div class="swiper-button-next">
                                                                                        <svg class="icon__arrowRightDefault" width="32" height="24">
                                                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <? if (!empty($arItem['PROPERTIES']['COMMENT']['VALUE']['TEXT'])): ?>
                                                                    <div class="answer-item">
                                                                        <div class="answer-item__title">
                                                                            <?= $arItem['PROPERTIES']['COMMENT']['NAME'] ?>:
                                                                        </div>
                                                                        <p class="answer-item__description active">
                                                                            <?= $arItem['PROPERTIES']['COMMENT']['~VALUE']['TEXT'] ?>
                                                                        </p>
                                                                    </div>
                                                                <? endif ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? endif ?>
                                        <? endif ?>
                                        <div class="review-item__row">
                                            <div class="review-item__info">
                                                <div class="review-item__name c-gray-umber">
                                                    <?= $arItem['NAME'] ?>
                                                </div>
                                                <? if (!empty($arItem['PROPERTIES']['WALK']['VALUE'])): ?>
                                                    <div class="review-item__position c-white-aluminum">
                                                        <?= $arItem['PROPERTIES']['WALK']['VALUE'] ?>
                                                    </div>
                                                <? endif ?>
                                            </div>
                                            <div class="review-item__col">
                                                <div class="review-item__category c-white-aluminum">
                                                    <?= Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['SERVICE']['VALUE'][0]) ?>
                                                </div>
                                                <? if (count($arItem['PROPERTIES']['SERVICE']['VALUE']) > 1): ?>
                                                    <button class="show-more">
                                                        <div class="show-more__btn">
                                                            <svg class="three-dot" width="2" height="10">
                                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#three-dot"></use>
                                                            </svg>
                                                            <span>Еще</span>
                                                        </div>
                                                        <div class="show-more__list" data-simplebar="data-simplebar">
                                                            <? foreach ($arItem['PROPERTIES']['SERVICE']['VALUE'] as $keyService => $arService): ?>
                                                                <? if ($keyService === 0) continue ?>
                                                                <div class="show-more__list-item">
                                                                    <span>
                                                                        <?= Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arService) ?>
                                                                    </span>
                                                                </div>
                                                            <? endforeach ?>
                                                        </div>
                                                    </button>
                                                <? endif ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item__text-wrapper">
                                        <p class="review-item__review item__text c-gray-umber">
                                            <?= $arItem['PREVIEW_TEXT'] ?>
                                        </p>
                                        <span class="review-item__show-all-text item__show-all-text c-carmine-pink">
                                            <span>Показать полностью</span>
                                            <span>Свернуть</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="review-item__main-row<?= empty($arItem['DETAIL_TEXT']) ? ' border-0' : '' ?>">
                                    <div class="review-item__list">
                                        <? if (!empty($arItem['PROPERTIES']['SPECIALIST']['VALUE'])): ?>
                                            <div class="review-item__list-item">
                                                <div class="review-item__list-col">
                                                    <div class="review-item__list-heading">Специалист:</div>
                                                </div>
                                                <div class="review-item__list-col">
                                                    <div class="review-item__list-value">
                                                        <?= preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1 $2.$3.', Catalog::getElementNameByID($arItem['PROPERTIES']['SPECIALIST']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['SPECIALIST']['VALUE'])) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif ?>
                                        <? if (!empty($arItem['PROPERTIES']['PROBLEM']['VALUE'])): ?>
                                            <div class="review-item__list-item">
                                                <div class="review-item__list-col">
                                                    <div class="review-item__list-heading">
                                                        <?= $arItem['PROPERTIES']['PROBLEM']['NAME'] ?>:
                                                    </div>
                                                </div>
                                                <div class="review-item__list-col">
                                                    <div class="review-item__list-value">
                                                        <?= $arItem['PROPERTIES']['PROBLEM']['VALUE'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif ?>
                                        <? if (!empty($arItem['PROPERTIES']['OPERATION']['VALUE'])): ?>
                                            <div class="review-item__list-item">
                                                <div class="review-item__list-col">
                                                    <div class="review-item__list-heading">
                                                        <?= $arItem['PROPERTIES']['OPERATION']['NAME'] ?>:
                                                    </div>
                                                </div>
                                                <div class="review-item__list-col">
                                                    <div class="review-item__list-value">
                                                        <?= $arItem['PROPERTIES']['OPERATION']['VALUE'][0] ?>
                                                    </div>
                                                    <? if (count($arItem['PROPERTIES']['OPERATION']['VALUE']) > 1): ?>
                                                        <button class="show-more">
                                                            <div class="show-more__btn">
                                                                <svg class="three-dot" width="2" height="10">
                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#three-dot"></use>
                                                                </svg>
                                                                <span>Еще</span>
                                                            </div>
                                                            <div class="show-more__list" data-simplebar="data-simplebar">
                                                                <? foreach ($arItem['PROPERTIES']['OPERATION']['VALUE'] as $keyOperations => $arOperations): ?>
                                                                    <? if ($keyOperations === 0) continue ?>
                                                                    <div class="show-more__list-item">
                                                                        <span>
                                                                            <?= $arOperations ?>
                                                                        </span>
                                                                    </div>
                                                                <? endforeach ?>
                                                            </div>
                                                        </button>
                                                    <? endif ?>
                                                </div>
                                            </div>
                                        <? endif ?>
                                        <? if (!empty($arItem['PROPERTIES']['SOURCE']['VALUE'])): ?>
                                            <div class="review-item__list-item">
                                                <div class="review-item__list-col">
                                                    <div class="review-item__list-heading">
                                                        <?= $arItem['PROPERTIES']['SOURCE']['NAME'] ?>:
                                                    </div>
                                                </div>
                                                <div class="review-item__list-col">
                                                    <div class="review-item__list-value">
                                                        <?= $arItem['PROPERTIES']['SOURCE']['VALUE'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif ?>
                                    </div>
                                </div>
                                <? if (!empty($arItem['DETAIL_TEXT'])): ?>
                                    <div class="review-item__bottom-row">
                                        <a class="arrow-link answer-modal-open" href="javascript:void(0);" data-modal-class="answer-modal" data-type="right">
                                            <span class="arrow-link__title">Ответ клиники</span>
                                        </a>
                                        <div class="answer-modal modal__inner modal_right mfp-hide">
                                            <button class="modal__close-btn js-modal-close">
                                                <svg class="icon__close" width="20" height="20">
                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                </svg>
                                            </button>
                                            <div class="modal__content">
                                                <div class="modal__content-row">
                                                    <div class="modal__title">Ответ клиники</div>
                                                    <? if (!empty($arItem['PROPERTIES']['SERVICE']['VALUE'])): ?>
                                                        <div class="modal__text">
                                                            <? if (!empty($arItem['IBLOCK_SECTION_ID'])): ?>
                                                                #<?= mb_strtolower(Catalog::getSectionNameByID($arParams['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID'])); ?>
                                                            <? else: ?>
                                                                #<?= mb_strtolower(Catalog::getSectionNameByID($arItem['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['DIRECTION']['VALUE'])); ?>
                                                            <? endif ?>
                                                        </div>
                                                        <? if (empty($arItem['IBLOCK_SECTION_ID'])): ?>
                                                            <div class="modal__text">
                                                                <? foreach ($arItem['PROPERTIES']['SERVICE']['VALUE'] as $keyService => $arService): ?>
                                                                    <span>
                                                                        <? if ($keyService === 0): ?>
                                                                            <?= Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arService) ?><?= (count($arItem['PROPERTIES']['SERVICE']['VALUE']) > 1) ? ', ' : '' ?>
                                                                        <? else: ?>
                                                                            <?= mb_strtolower(Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arService)) ?>
                                                                        <? endif ?>
                                                                    </span>
                                                                <? endforeach ?>
                                                            </div>
                                                        <? endif ?>
                                                    <? endif ?>
                                                </div>
                                                <div class="modal__content-row modal__content-row_unique">
                                                    <p class="modal__paragraph">
                                                        <?= $arItem['DETAIL_TEXT'] ?>
                                                    </p>
                                                </div>
                                                <? if (!empty($arItem['PROPERTIES']['RELATED_MATERIALS']['VALUE'])): ?>
                                                    <div class="modal__content-row">
                                                        <div class="modal__subtitle">
                                                            <?= $arItem['PROPERTIES']['RELATED_MATERIALS']['NAME'] ?>:
                                                        </div>
                                                        <div class="modal__nav">
                                                            <? foreach ($arItem['PROPERTIES']['RELATED_MATERIALS']['VALUE'] as $keyRelated => $arRelated): ?>
                                                                <a class="modal__nav-link" href="<?= $arItem['PROPERTIES']['RELATED_MATERIALS']['DESCRIPTION'][$keyRelated] ?>">
                                                                    <?= $arRelated ?>
                                                                </a>
                                                            <? endforeach ?>
                                                        </div>
                                                    </div>
                                                <? endif ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif ?>
                            </div>
                        <? endforeach ?>
                    </div>
                </div>
            </div>
            <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
                <? if ($arResult["NAV_RESULT"]->nEndPage > 1 && $arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->nEndPage): ?>
                    <div class="loader-wrapper show_more" style="opacity: 0" data-show-more="<?= $arResult["NAV_RESULT"]->NavNum ?>" data-next-page="<?= ($arResult["NAV_RESULT"]->NavPageNomer + 1) ?>" data-max-page="<?= $arResult["NAV_RESULT"]->nEndPage ?>">
                        <svg class="loader" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 199.998 96.615" enable-background="new 0 0 199.998 96.615">
                            <g>
                                <path fill="none" stroke="#E84849" stroke-width="2.5" stroke-linecap="round" d="M198.746,59.841h-14.537c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371c-0.33,1.036-1.843,0.876-1.948-0.206l-3.747-53.409c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993c-0.031,0.074-0.104,0.122-0.184,0.122h-16.937l16.904-0.02h-32.537c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371c-0.33,1.036-1.843,0.876-1.948-0.206l-3.747-53.409c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993c-0.031,0.074-0.104,0.122-0.184,0.122H68.452l16.182-0.02H52.096c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371c-0.33,1.036-1.843,0.876-1.948-0.206L37.114,2.938c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993c-0.031,0.074-0.104,0.122-0.184,0.122H2.034"></path>
                            </g>
                        </svg>
                    </div>
                <? endif ?>
            <? endif; ?>
        </div>
        <div class="col col-4 col-xl-4 col-lg-12 col-md-12 hidden-sm-down offset-1 offset-xl-0 offset-lg-0 offset-md-0 offset-sm-0 offset-xs-0">
            <div class="modal-card-wrapper">
                <div class="modal-card">
                    <div class="modal-card__icon-wrapper">
                        <div class="modal-card__icon-bg bg-linen"></div>
                        <svg class="patient-reviews" width="84px" height="84px">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#patient-reviews"></use>
                        </svg>
                    </div>
                    <div class="modal-card__title">Помогите нам стать лучше — оцените качество услуг!</div>
                    <a class="btn btn_primary modal-open" href="#add-review">
                        <span>Оставить отзыв</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>