<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;

?>
<? if (count($arResult["ITEMS"]) > 0): ?>
    <section class="section s-before-after-slider mb-less scrl fadeInUp">
        <div class="row section__top-row">
            <div class="col col-12">
                <div class="section__top-row-inner">
                    <div class="section-title">До и после</div>
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-pagination"></div>
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <? if (count($arResult['ITEMS']) > 1): ?>
                    <div class="section__top-row-inner">
                        <div class="category-items">
                            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                                <div class="category-items__item<?= $arParams['FILTER_TABS'] == $arItem['IBLOCK_SECTION_ID'] ? ' active' : '' ?>"
                                     data-filter-slider="<?= $arItem['IBLOCK_SECTION_ID'] ?>">
                                    <span><?= Catalog::getSectionNameByID(SITE_SERVICE_IBLOCK_ID, $arItem['IBLOCK_SECTION_ID']) ?></span>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif ?>
            </div>
        </div>
        <div class="row">
            <div class="col col-12">
                <div class="default-slider">
                    <div class="swiper-container not-in-modal">
                        <div class="swiper-wrapper">
                            <? $count = 0; ?>
                            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                                <? if (!empty($arParams['DOP_FILTER']) && $arParams['DOP_FILTER'] != $arItem['IBLOCK_SECTION_ID']) {
                                    continue;
                                } ?>
                                <? foreach ($arItem['PROPERTIES']['BEFORE_AFTER']['VALUE'] as $keyPhoto => $arPhoto): ?>
                                    <? $count++; ?>
                                    <? if ($count >= 6) break; ?>
                                    <? $img = CFile::ResizeImageGet($arPhoto, ['width' => 800, 'height' => 800], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []) ?>
                                    <div class="swiper-slide">
                                        <div class="before-after-item item">
                                            <a class="before-after-item__link before-after-photo-open"
                                               href="javascript:void(0);" data-modal-class="before-after-photo-modal"
                                               data-type="center">
                                                <div class="before-after-item__img-wrapper">
                                                    <img class="before-after-item__img lazy"
                                                         data-src="<?= $img['src'] ?>"
                                                         alt=""/>
                                                </div>
                                            </a>
                                            <div class="before-after-photo-modal modal__inner modal_center mfp-hide">
                                                <button class="modal__close-btn js-modal-close">
                                                    <svg class="icon__close" width="24" height="24">
                                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                    </svg>
                                                </button>
                                                <div class="modal__content modal-before-after-slider">
                                                    <div class="row modal__top-row">
                                                        <div class="col col-12">
                                                            <div class="modal__top-row-inner">
                                                                <div class="modal__title modal__title_big"></div>
                                                                <div class="swiper-controls">
                                                                    <div class="swiper-pagination"></div>
                                                                    <div class="swiper-buttons">
                                                                        <div class="swiper-button-prev">
                                                                            <svg class="icon__arrowRightDefault"
                                                                                 width="32"
                                                                                 height="24">
                                                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                            </svg>
                                                                        </div>
                                                                        <div class="swiper-button-next">
                                                                            <svg class="icon__arrowRightDefault"
                                                                                 width="32"
                                                                                 height="24">
                                                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row modal__main-row">
                                                        <div class="col col-8 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="default-slider">
                                                                <div class="swiper-container">
                                                                    <div class="swiper-wrapper">
                                                                        <div class="swiper-slide">
                                                                            <img class="before-after-item__img swiper-lazy"
                                                                                 data-src="<?= $img['src'] ?>"
                                                                                 alt=""/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col col-12 hidden-md-up">
                                                                    <div class="mobile-swiper-controls">
                                                                        <div class="swiper-controls">
                                                                            <div class="swiper-pagination"></div>
                                                                            <div class="swiper-buttons">
                                                                                <div class="swiper-button-prev">
                                                                                    <svg class="icon__arrowRightDefault"
                                                                                         width="32"
                                                                                         height="24">
                                                                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                                    </svg>
                                                                                </div>
                                                                                <div class="swiper-button-next">
                                                                                    <svg class="icon__arrowRightDefault"
                                                                                         width="32"
                                                                                         height="24">
                                                                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <? if (!empty($arItem['PROPERTIES']['BEFORE_AFTER']['DESCRIPTION'][$keyPhoto])): ?>
                                                            <div class="col col-4 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="answer-item">
                                                                    <div class="answer-item__title">
                                                                        Комментарий специалиста:
                                                                    </div>
                                                                    <p class="answer-item__description active">
                                                                        <?= $arItem['PROPERTIES']['BEFORE_AFTER']['DESCRIPTION'][$keyPhoto] ?>
                                                                    </p>
                                                                </div>
                                                                <a class="arrow-link"
                                                                   href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                                                    <span class="arrow-link__title">Подробнее об услуге</span>
                                                                </a>
                                                            </div>
                                                        <? endif ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach ?>
                            <? endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-12 hidden-md-up">
                <div class="mobile-swiper-controls">
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-pagination"></div>
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif ?>