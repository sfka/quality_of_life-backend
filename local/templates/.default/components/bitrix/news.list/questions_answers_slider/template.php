<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;

?>
<? if (count($arResult["ITEMS"]) > 0): ?>
    <section class="section s-question-answer-slider mb-less scrl fadeInUp">
        <div class="row section__top-row">
            <div class="col col-12">
                <div class="section__top-row-inner">
                    <div class="section-title">Вопросы и ответы</div>
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-12">
                <div class="default-slider">
                    <div class="swiper-container not-in-modal">
                        <div class="swiper-wrapper">
                            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                                <div class="swiper-slide">
                                    <div class="question-item item">
                                        <div class="question-item__main-row">
                                            <div class="question-item__name c-white-aluminum">Спрашивает <?= $arItem['NAME'] ?></div>
                                            <div class="item__text-wrapper">
                                                <p class="question-item__question item__text c-gray-umber">
                                                    <?= $arItem['PREVIEW_TEXT'] ?>
                                                </p>
                                                <span class="question-item__show-all-text item__show-all-text c-carmine-pink">
                                                    <span>Показать полностью</span>
                                                    <span>Свернуть</span>
                                                </span>
                                            </div>
                                        </div>
                                        <? if (!empty($arItem['DETAIL_TEXT'])): ?>
                                            <div class="question-item__bottom-row">
                                                <a class="arrow-link answer-modal-open" href="javascript:void(0);" data-modal-class="answer-modal" data-type="right">
                                                    <span class="arrow-link__title">Ответ клиники</span>
                                                </a>
                                                <div class="answer-modal modal__inner modal_right mfp-hide">
                                                    <button class="modal__close-btn js-modal-close">
                                                        <svg class="icon__close" width="20" height="20">
                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                        </svg>
                                                    </button>
                                                    <div class="modal__content">
                                                        <div class="modal__content-row">
                                                            <div class="modal__title">Ответ клиники</div>
                                                            <? if (!empty($arItem['PROPERTIES']['DIRECTION']['VALUE'])): ?>
                                                                <div class="modal__text">
                                                                    #<?= mb_strtolower(Catalog::getSectionNameByID($arItem['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['DIRECTION']['VALUE'])) ?>
                                                                </div>
                                                            <? endif ?>
                                                        </div>
                                                        <div class="modal__content-row modal__content-row_unique">
                                                            <p class="modal__paragraph">
                                                                <?= $arItem['DETAIL_TEXT'] ?>
                                                            </p>
                                                        </div>
                                                        <? if (!empty($arItem['PROPERTIES']['RELATED_MATERIALS']['VALUE'])): ?>
                                                            <div class="modal__content-row">
                                                                <div class="modal__subtitle">
                                                                    <?= $arItem['PROPERTIES']['RELATED_MATERIALS']['NAME'] ?>:
                                                                </div>
                                                                <div class="modal__nav">
                                                                    <? foreach ($arItem['PROPERTIES']['RELATED_MATERIALS']['VALUE'] as $keyRelated => $arRelated): ?>
                                                                        <a class="modal__nav-link" href="<?= $arItem['PROPERTIES']['RELATED_MATERIALS']['DESCRIPTION'][$keyRelated] ?>">
                                                                            <?= $arRelated ?>
                                                                        </a>
                                                                    <? endforeach ?>
                                                                </div>
                                                            </div>
                                                        <? endif ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif ?>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-12 hidden-md-up">
                <div class="mobile-swiper-controls">
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>

                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif ?>