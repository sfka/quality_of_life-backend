<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult['SECTIONS'] = [];
foreach ($arResult['ITEMS'] as $arItem) {
    foreach ($arItem['PROPERTIES']['SERVICE']['VALUE'] as $subSection) {
        $arSubSection[] = $subSection;
    }
}
foreach ($arSubSection as $subSection) {
    $resSubsection = \CIBlockSection::GetByID($subSection);
    if($arSubSection = $resSubsection->GetNext()) {
        $resSection  = \CIBlockSection::GetByID($arSubSection['IBLOCK_SECTION_ID']);
        if($arSection = $resSection->GetNext()) {
            $arResult['SECTIONS'][$arSubSection['IBLOCK_SECTION_ID']]['ID'] = $arSection['ID'];
            $arResult['SECTIONS'][$arSubSection['IBLOCK_SECTION_ID']]['NAME'] = $arSection['NAME'];
        }
        $arResult['SECTIONS'][$arSubSection['IBLOCK_SECTION_ID']]['SUBSECTIONS'][$arSubSection['ID']] = $arSubSection;
    }
}
