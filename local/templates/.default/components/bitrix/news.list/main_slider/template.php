<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="index-slider">
    <div class="stock-block">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <? foreach ($arResult['ITEMS'] as $arItem): ?>
                    <div class="swiper-slide" <?= ($arItem['PROPERTIES']['BACKGROUND']['VALUE'] ? 'style="background-color: #' . $arItem['PROPERTIES']['BACKGROUND']['VALUE'] . '"' : false); ?>>
                        <div class="stock-block__item">
                            <div class="stock-block__col left">
                                <div class="stock-block__content">
                                    <div class="stock-block__top-row">
                                        <? if ($arItem['PROPERTIES']['DISCOUNT']['VALUE']): ?>
                                            <div class="discount"><?= $arItem['PROPERTIES']['DISCOUNT']['VALUE'] ?></div>
                                        <? endif; ?>
                                        <div class="price">
                                            <div class="price__item">
                                                <? if ($arItem['PROPERTIES']['OLD_PRICE']['VALUE'] && $arItem['PROPERTIES']['PRICE']['VALUE']): ?>
                                                    <span class="price__text">Цена:</span><span
                                                            class="price__value old"><?= $arItem['PROPERTIES']['OLD_PRICE']['VALUE'] ?></span>
                                                    <span class="price__value new"><?= $arItem['PROPERTIES']['PRICE']['VALUE'] ?></span>
                                                <? elseif ($arItem['PROPERTIES']['PRICE']['VALUE']): ?>
                                                    <span class="price__text">Цена:</span><span
                                                            class="price__value"><?= $arItem['PROPERTIES']['PRICE']['VALUE'] ?></span>
                                                <? endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="stock-block__main-row">
                                        <div class="stock-block__title"><?= $arItem['NAME'] ?></div>
                                        <p class="stock-block__description"><?= $arItem['PREVIEW_TEXT'] ?></p>
                                    </div>
                                    <div class="stock-block__bottom-row">
                                        <a class="arrow-link" href="<?=$arItem['DETAIL_PAGE_URL'] ?>">
                                            <span class="arrow-link__title">Подробнее</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="stock-block__date">
                                    <?= (!empty($arItem['DATE_ACTIVE_FROM'])) ? '* c ' . mb_strtolower(FormatDateFromDB($arItem['DATE_ACTIVE_FROM'], "j F")) : ''; ?>
                                    <?= (!empty($arItem['DATE_ACTIVE_TO'])) ? ' по ' . mb_strtolower(FormatDateFromDB($arItem['DATE_ACTIVE_TO'], "j F")) : ''; ?>
                                </div>
                            </div>
                            <? if ($arItem['PREVIEW_PICTURE']['SRC']): ?>
                                <div class="stock-block__col right">
                                    <div class="stock-block__img-wrapper">
                                        <?
                                            $color = \IL\Utilities::hex2rgb('#' . ($arItem['PROPERTIES']['BACKGROUND']['VALUE'] ? $arItem['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5'), '0.5');
                                            $stockBlockImgMaskStyle = '
                                                background-image: -webkit-gradient(linear,right top,left top,from(' . $color . '),to(#' . ($arItem['PROPERTIES']['BACKGROUND']['VALUE'] ? $arItem['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') . '));
                                                background-image: -webkit-linear-gradient(right,' . $color . ',#' . ($arItem['PROPERTIES']['BACKGROUND']['VALUE'] ? $arItem['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') . ');
                                                background-image: linear-gradient(to left,' . $color . ',#' . ($arItem['PROPERTIES']['BACKGROUND']['VALUE'] ? $arItem['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') . ');
                                            ';
                                        ?>
                                        <div class="stock-block__img-mask" style="<?=$stockBlockImgMaskStyle ?>"></div>
                                        <? if ($arItem['DETAIL_PICTURE']['SRC']): ?>
                                            <div class="stock-block__img swiper-lazy"
                                                 data-background="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>"></div>
                                        <? elseif($arItem['PREVIEW_PICTURE']['SRC']) : ?>
                                            <div class="stock-block__img swiper-lazy"
                                                 data-background="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"></div>
                                        <? endif; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
            <div class="index-slider__controls hide">
                <div class="common-container">
                    <div class="index-slider__controls-inner">
                        <div class="index-slider__counter">
                            <div class="index-slider__counter-current">1</div>
                            <div class="index-slider__counter-limiter">/</div>
                            <div class="index-slider__counter-all"><?= count($arResult['ITEMS']) ?></div>
                        </div>
                        <div class="index-slider__pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
