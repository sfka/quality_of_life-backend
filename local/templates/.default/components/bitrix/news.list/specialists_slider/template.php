<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;

?>
<? if (count($arResult["ITEMS"]) > 0): ?>
    <section class="section s-specialists-slider mb-less scrl fadeInUp">
        <div class="row section__top-row">
            <div class="col col-12">
                <div class="section__top-row-inner">
                    <div class="section-title"><?= $arParams['BLOCK_TITLE'] ?></div>
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-12">
                <div class="default-slider">
                    <div class="swiper-container not-in-modal">
                        <div class="swiper-wrapper">
                            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                                <div class="swiper-slide">
                                    <div class="specialist-item item">
                                        <a class="specialist-item__img-wrapper"
                                           href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                            <? if ($arItem['PREVIEW_PICTURE']['SRC']) {
                                                $thumb = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], [
                                                    'width' => 200,
                                                    'height' => 200,
                                                ], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []);
                                            } else {
                                                $thumb['src'] = SITE_STYLE_PATH . '/img/content/specialists/no-photo-expert.svg';
                                            } ?>
                                            <img class="specialist-item__img lazy" data-src="<?= $thumb['src'] ?>"
                                                 alt="<?= $arItem['NAME'] ?>"/>
                                        </a>
                                        <div class="specialist-item__content">
                                            <div class="specialist-item__row top">
                                                <div class="specialist-item__row-inner">
                                                    <div class="specialist-item__col unique">
                                                        <? $arServices = Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['SERVICE']['VALUE'][0]); ?>
                                                        <div class="specialist-item__category">
                                                            <?= $arServices ?>
                                                        </div>
                                                        <? if (count($arItem['PROPERTIES']['SERVICE']['VALUE']) > 1): ?>
                                                            <button class="show-more">
                                                                <div class="show-more__btn">
                                                                    <svg class="three-dot" width="2" height="10">
                                                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#three-dot"></use>
                                                                    </svg>
                                                                    <span>Еще</span>
                                                                </div>
                                                                <div class="show-more__list"
                                                                     data-simplebar="data-simplebar">
                                                                    <? foreach ($arItem['PROPERTIES']['SERVICE']['VALUE'] as $keyService => $arService): ?>
                                                                        <? if ($keyService == 0) continue ?>
                                                                        <? $arServiceName = Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arService); ?>
                                                                        <div class="show-more__list-item">
                                                                            <span><?= $arServiceName ?></span>
                                                                        </div>
                                                                    <? endforeach ?>
                                                                </div>
                                                            </button>
                                                        <? endif ?>
                                                    </div>
                                                    <? if ($arItem['PROPERTIES']['REVIEWS'] > 0): ?>
                                                        <div class="specialist-item__col">
                                                            <a class="specialist-item__link"
                                                               href="<?= $arItem['DETAIL_PAGE_URL'] ?>#reviews">
                                                                Отзывы: <?= $arItem['PROPERTIES']['REVIEWS'] ?>
                                                            </a>
                                                        </div>
                                                    <? endif ?>
                                                </div>
                                            </div>
                                            <div class="specialist-item__row main">
                                                <div class="specialist-item__row-inner">
                                                    <div class="specialist-item__col">
                                                        <a class="specialist-item__name"
                                                           href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                                            <?= $arItem['NAME'] ?>
                                                        </a>
                                                        <div class="specialist-item__position">
                                                            <?= !empty($arItem['PROPERTIES']['PROFESSION']['VALUE']) ? $arItem['PROPERTIES']['PROFESSION']['VALUE'] : '&nbsp;' ?>
                                                        </div>
                                                    </div>
                                                    <div class="specialist-item__col">
                                                        <? if (!empty($arItem['PROPERTIES']['YOUTUBE']['VALUE'])): ?>
                                                            <a class="specialist-item__play specialist-video-open"
                                                               href="javascript:void(0)"
                                                               data-modal-class="specialist-video-modal"
                                                               data-type="center">
                                                                <div class="specialist-item__play-icon-wrapper">
                                                                    <svg class="play" width="10" height="12">
                                                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#play"></use>
                                                                    </svg>
                                                                </div>
                                                                <span class="specialist-item__play-label">Видео о специалисте</span>
                                                            </a>
                                                            <div class="specialist-video-modal modal__inner modal_center mfp-hide">
                                                                <button class="modal__close-btn js-modal-close">
                                                                    <svg class="icon__close" width="20" height="20">
                                                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                                    </svg>
                                                                </button>
                                                                <div class="modal__content">
                                                                    <iframe class="modal__video"
                                                                            src="https://www.youtube.com/embed/<?= $arItem['PROPERTIES']['YOUTUBE']['VALUE'] ?>"
                                                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                                            allowfullscreen="allowfullscreen"></iframe>
                                                                </div>
                                                            </div>
                                                        <? endif ?>
                                                    </div>
                                                </div>
                                                <? if (!empty($arItem['PROPERTIES']['DEGREE']['VALUE']) || !empty($arItem['PROPERTIES']['STANDING']['VALUE'])): ?>
                                                    <ul class="specialist-item__list">
                                                        <? if (!empty($arItem['PROPERTIES']['DEGREE']['VALUE'])): ?>
                                                            <li class="specialist-item__list-item">
                                                                <span class="specialist-item__list-property">
                                                                    <?= $arItem['PROPERTIES']['DEGREE']['NAME'] ?>:
                                                                </span>
                                                                <span class="specialist-item__list-value">
                                                                    <?= $arItem['PROPERTIES']['DEGREE']['VALUE'] ?>
                                                                </span>
                                                            </li>
                                                        <? endif ?>
                                                        <? if (!empty($arItem['PROPERTIES']['STANDING']['VALUE'])): ?>
                                                            <li class="specialist-item__list-item">
                                                                <span class="specialist-item__list-property">
                                                                    <?= $arItem['PROPERTIES']['STANDING']['NAME'] ?>:
                                                                </span>
                                                                <span class="specialist-item__list-value">
                                                                    <?= $arItem['PROPERTIES']['STANDING']['VALUE'] ?>
                                                                </span>
                                                            </li>
                                                        <? endif ?>
                                                    </ul>
                                                <? endif ?>
                                            </div>
                                            <div class="specialist-item__row bottom">
                                                <a class="arrow-link"
                                                   href="/appointments/?specialist=<?= $arItem['EXTERNAL_ID'] ?>">
                                                    <span class="arrow-link__title">Запись на прием</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-12 hidden-md-up">
                <div class="mobile-swiper-controls">
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif ?>