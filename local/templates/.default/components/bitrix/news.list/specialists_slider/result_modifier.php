<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */

/** @var array $APPLICATION */

use \IL\Catalog;
use \IL\Settings;

// Получим количество отзывов специалиста
foreach ($arResult['ITEMS'] as $key => $SPECIALIST) {
    $arReviews = count(Catalog::getElementList(Settings::REVIEWS_IBLOCK_ID, ['PROPERTY_SPECIALIST' => $SPECIALIST['ID']]));
    $arResult['ITEMS'][$key]['PROPERTIES']['REVIEWS'] = $arReviews;
}