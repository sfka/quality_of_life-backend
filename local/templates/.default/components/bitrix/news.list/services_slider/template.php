<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<section class="section s-service-slider mb-less scrl fadeInUp">
    <div class="row section__top-row">
        <div class="col col-12">
            <div class="section__top-row-inner">
                <div class="section-title">Комплекс услуг участвующих в программе</div>
                <div class="swiper-controls not-in-modal">
                    <div class="swiper-buttons">
                        <div class="swiper-button-prev">
                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                            </svg>

                        </div>
                        <div class="swiper-button-next">
                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                            </svg>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-12">
            <div class="default-slider">
                <div class="swiper-container not-in-modal">
                    <div class="swiper-wrapper">
                        <? foreach ($arResult['ITEMS'] as $arItem): ?>
                        <div class="swiper-slide">
                            <a class="service-card" href="<?=$arItem['DETAIL_PAGE_URL'] ?>">
                                <div class="service-card__text"><?=\IL\Catalog::getSectionNameByID($arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID']) ?></div>
                                <div class="service-card__title dotdotdot-text"><?=$arItem['NAME'] ?></div>
                                <svg class="arrowRightDefault" width="24" height="18">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </a>
                        </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <p class="important-text"><span>* Внимание!</span> Не суммируется с другими специальными
                предложениями</p>
        </div>
    </div>
    <div class="row">
        <div class="col col-12 hidden-md-up">
            <div class="mobile-swiper-controls">
                <div class="swiper-controls not-in-modal">
                    <div class="swiper-buttons">
                        <div class="swiper-button-prev">
                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                            </svg>

                        </div>
                        <div class="swiper-button-next">
                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                            </svg>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
