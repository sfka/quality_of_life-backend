<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;
use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest();
?>

<? if (!$ajax): ?>
    <? $this->SetViewTarget('left_tags'); ?>
    <div class="sidebar__tags-wrapper">
        <div class="sidebar__col">
            <div class="sidebar__title">Направления деятельности</div>
            <div class="sidebar__tags">
                <? foreach ($arResult['LEFT_MENU']['SECTIONS'] as $arDirections): ?>
                    <span class="sidebar__tags-item" data-direction="<?= $arDirections['ID'] ?>">
                        <span>#<?= mb_strtolower($arDirections['NAME']) ?></span>
                    </span>
                <? endforeach ?>
            </div>
        </div>
        <? if (!empty($arResult['LEFT_MENU']['TAGS'])): ?>
            <div class="sidebar__col">
                <div class="sidebar__title">Теги</div>
                <div class="sidebar__tags">
                    <? foreach ($arResult['LEFT_MENU']['TAGS'] as $arTags): ?>
                        <span class="sidebar__tags-item" data-section="<?= $arTags['ID'] ?>">
                            <span>#<?= mb_strtolower($arTags['NAME']) ?></span>
                        </span>
                    <? endforeach ?>
                </div>
            </div>
        <? endif ?>
    </div>
    <? $this->EndViewTarget(); ?>
<? endif ?>

<div class="col col-9 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
    <div class="hidden-lg-up">
        <div class="mobile-tags-select-wrapper">
            <div class="nice-select-wrapper">
                <div class="nice-select-wrapper__placeholder">Выбрать по тегу</div>
                <select class="nice-select ajax__filter" required>
                    <? foreach ($arResult['LEFT_MENU']['SECTIONS'] as $arDirections): ?>
                        <option data-direction="<?= $arDirections['ID'] ?>">#<?= mb_strtolower($arDirections['NAME']) ?></option>
                    <? endforeach ?>
                    <? foreach ($arResult['LEFT_MENU']['TAGS'] as $arTags): ?>
                        <option data-section="<?= $arTags['ID'] ?>">#<?= mb_strtolower($arTags['NAME']) ?></option>
                    <? endforeach ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-7 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="dynamic-content-wrapper width-fix">
                <div class="dynamic-content">
                    <? if ($ajax && (!empty($request->get('DIRECTION')) || !empty($request->get('SECTION'))) && empty($request->get('PAGEN_1'))): ?>
                        <div class="dynamic-content__heading">
                            <div class="dynamic-content__heading-row">
                                <div class="dynamic-content__title c-gray-umber">
                                    <? if (!empty($request->get('DIRECTION'))): ?>
                                        #<?= mb_strtolower(Catalog::getSectionNameByID($arResult['ITEMS'][0]['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], $request->get('DIRECTION'))); ?>
                                    <? elseif ($request->get('SECTION')): ?>
                                        #<?= mb_strtolower(Catalog::getSectionNameByID($arParams['IBLOCK_ID'], $request->get('SECTION'))); ?>
                                    <? endif ?>
                                </div>
                            </div>
                            <? if (!empty($arResult['SERVICE'])): ?>
                                <div class="dynamic-content__heading-row">
                                    <div class="category-items">
                                        <? foreach ($arResult['SERVICE'] as $arService): ?>
                                            <div class="category-items__item<?= ($arService['ID'] == $request->get('SERVICE')) ? ' active' : '' ?>"
                                                 data-direction="<?= $arResult['ITEMS'][0]['PROPERTIES']['DIRECTION']['VALUE'] ?>"
                                                 data-service="<?= $arService['ID'] ?>">
                                                <span><?= $arService['NAME'] ?></span>
                                            </div>
                                        <? endforeach ?>
                                    </div>
                                </div>
                            <? endif ?>
                        </div>
                    <? endif ?>
                    <div class="dynamic-content__main ajax_list">
                        <? foreach ($arResult['ITEMS'] as $arItem): ?>
                            <div class="question-item item ajax_item">
                                <div class="question-item__main-row<?= empty($arItem['DETAIL_TEXT']) ? ' border-0' : '' ?>">
                                    <div class="question-item__heading">
                                        <div class="question-item__name c-white-aluminum">
                                            Спрашивает <?= $arItem['NAME'] ?>
                                        </div>
                                        <span class="question-item__category">
                                            <? if (!empty($arItem['IBLOCK_SECTION_ID'])): ?>
                                                #<?= mb_strtolower(Catalog::getSectionNameByID($arParams['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID'])); ?>
                                            <? else: ?>
                                                #<?= mb_strtolower(Catalog::getSectionNameByID($arItem['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['DIRECTION']['VALUE'])); ?>
                                            <? endif ?>
                                        </span>
                                    </div>
                                    <div class="item__text-wrapper">
                                        <p class="question-item__question item__text c-gray-umber">
                                            <?= $arItem['PREVIEW_TEXT'] ?>
                                        </p>
                                        <span class="question-item__show-all-text item__show-all-text c-carmine-pink">
                                            <span>Показать полностью</span>
                                            <span>Свернуть</span>
                                        </span>
                                    </div>
                                </div>
                                <? if (!empty($arItem['DETAIL_TEXT'])): ?>
                                    <div class="question-item__bottom-row">
                                        <a class="arrow-link answer-modal-open" href="javascript:void(0);" data-modal-class="answer-modal" data-type="right">
                                            <span class="arrow-link__title">Ответ клиники</span>
                                        </a>
                                        <div class="answer-modal modal__inner modal_right mfp-hide">
                                            <button class="modal__close-btn js-modal-close">
                                                <svg class="icon__close" width="20" height="20">
                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                </svg>
                                            </button>
                                            <div class="modal__content">
                                                <div class="modal__content-row">
                                                    <div class="modal__title">Ответ клиники</div>
                                                    <div class="modal__text">
                                                        <? if (!empty($arItem['IBLOCK_SECTION_ID'])): ?>
                                                            #<?= mb_strtolower(Catalog::getSectionNameByID($arParams['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID'])); ?>
                                                        <? else: ?>
                                                            #<?= mb_strtolower(Catalog::getSectionNameByID($arItem['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['DIRECTION']['VALUE'])); ?>
                                                        <? endif ?>
                                                    </div>
                                                </div>
                                                <div class="modal__content-row modal__content-row_unique">
                                                    <p class="modal__paragraph">
                                                        <?= $arItem['DETAIL_TEXT'] ?>
                                                    </p>
                                                </div>
                                                <? if (!empty($arItem['PROPERTIES']['RELATED_MATERIALS']['VALUE'])): ?>
                                                    <div class="modal__content-row">
                                                        <div class="modal__subtitle">
                                                            <?= $arItem['PROPERTIES']['RELATED_MATERIALS']['NAME'] ?>:
                                                        </div>
                                                        <div class="modal__nav">
                                                            <? foreach ($arItem['PROPERTIES']['RELATED_MATERIALS']['VALUE'] as $keyRelated => $arRelated): ?>
                                                                <a class="modal__nav-link" href="<?= $arItem['PROPERTIES']['RELATED_MATERIALS']['DESCRIPTION'][$keyRelated] ?>">
                                                                    <?= $arRelated ?>
                                                                </a>
                                                            <? endforeach ?>
                                                        </div>
                                                    </div>
                                                <? endif ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif ?>
                            </div>
                        <? endforeach ?>
                    </div>
                </div>
            </div>
            <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
                <? if ($arResult["NAV_RESULT"]->nEndPage > 1 && $arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->nEndPage): ?>
                    <div class="loader-wrapper show_more" style="opacity: 0" data-show-more="<?= $arResult["NAV_RESULT"]->NavNum ?>" data-next-page="<?= ($arResult["NAV_RESULT"]->NavPageNomer + 1) ?>" data-max-page="<?= $arResult["NAV_RESULT"]->nEndPage ?>">
                        <svg class="loader" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 199.998 96.615" enable-background="new 0 0 199.998 96.615">
                            <g>
                                <path fill="none" stroke="#E84849" stroke-width="2.5" stroke-linecap="round" d="M198.746,59.841h-14.537c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371c-0.33,1.036-1.843,0.876-1.948-0.206l-3.747-53.409c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993c-0.031,0.074-0.104,0.122-0.184,0.122h-16.937l16.904-0.02h-32.537c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371c-0.33,1.036-1.843,0.876-1.948-0.206l-3.747-53.409c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993c-0.031,0.074-0.104,0.122-0.184,0.122H68.452l16.182-0.02H52.096c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371c-0.33,1.036-1.843,0.876-1.948-0.206L37.114,2.938c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993c-0.031,0.074-0.104,0.122-0.184,0.122H2.034"></path>
                            </g>
                        </svg>
                    </div>
                <? endif ?>
            <? endif; ?>
        </div>
        <div class="col col-4 col-xl-4 col-lg-12 col-md-12 hidden-sm-down offset-1 offset-xl-0 offset-lg-0 offset-md-0 offset-sm-0 offset-xs-0">
            <div class="modal-card-wrapper">
                <div class="modal-card">
                    <div class="modal-card__icon-wrapper">
                        <div class="modal-card__icon-bg bg-linen"></div>
                        <svg class="icon__question-answer" width="90px" height="90px">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#question-answer"></use>
                        </svg>
                    </div>
                    <div class="modal-card__title">Помогите нам стать лучше — оцените качество услуг!</div>
                    <a class="btn btn_primary modal-open" href="#ask-question">
                        <span>Задать вопрос</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>