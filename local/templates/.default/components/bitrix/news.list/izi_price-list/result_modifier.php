<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */

/** @var array $APPLICATION */

use \IL\Catalog;
use \Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest();

$arResult['LEFT_MENU'] = Catalog::getLeftMenu($arParams['IBLOCK_ID']);
if ($ajax && !empty($request->get('DIRECTION'))) {
    $arResult['SERVICE'] = Catalog::getTabsMenu($arParams['IBLOCK_ID'], $request->get('DIRECTION'));
}

$arResult['PRICE_LIST_ARRAY'] = \IL\Price::getFullPrice(SITE_SERVICE_IBLOCK_ID);
if ($ajax && (!empty($request->get('DIRECTION')))) {
    foreach ($arResult['PRICE_LIST_ARRAY'] as $key => $arItem){
        if ($request->get('DIRECTION') != $arItem['ID']){
            unset($arResult['PRICE_LIST_ARRAY'][$key]);
        }
        if(!empty($request->get('SECTION'))) {
            foreach ($arItem['SUBSECTIONS'] as $keySub => $arSubsection) {
                foreach ($arSubsection['ELEMENTS'] as $keyEl => $arElement) {
                    if ($request->get('SECTION') != $arElement['IBLOCK_SECTION_ID']) {
                        unset($arResult['PRICE_LIST_ARRAY'][$key]['SUBSECTIONS'][$keySub]['ELEMENTS'][$keyEl]);
                    }
                }
            }
        }
    }
}