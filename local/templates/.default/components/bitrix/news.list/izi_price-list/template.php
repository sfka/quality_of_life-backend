<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;
use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest();
?>

<? if (!$ajax): ?>
    <? $this->SetViewTarget('left_tags'); ?>
    <div class="sidebar__tags-wrapper">
        <div class="sidebar__col">
            <div class="sidebar__title">Направления деятельности</div>
            <div class="sidebar__tags">
                <? foreach ($arResult['PRICE_LIST_ARRAY'] as $arDirections): ?>
                    <? if ($arDirections['SUBSECTIONS']): ?>
                        <span class="sidebar__tags-item" data-direction="<?= $arDirections['ID'] ?>">
                        <span>#<?= mb_strtolower($arDirections['NAME']) ?></span>
                    </span>
                    <? endif; ?>
                <? endforeach ?>
            </div>
        </div>
        <? if (!empty($arResult['PRICE_LIST_ARRAY'])): ?>
            <div class="sidebar__col">
                <div class="sidebar__title">Теги</div>
                <div class="sidebar__tags">
                    <? foreach ($arResult['PRICE_LIST_ARRAY'] as $arDirections): ?>
                        <? if ($arDirections['SUBSECTIONS']): ?>
                            <span class="sidebar__tags-item" data-section="<?= $arDirections['ID'] ?>">
                            <span>#<?= mb_strtolower($arDirections['NAME']) ?></span>
                        </span>
                        <? endif; ?>
                    <? endforeach ?>
                </div>
            </div>
        <? endif ?>
    </div>
    <? $this->EndViewTarget(); ?>
<? endif ?>

<div class="col col-9 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
    <div class="hidden-lg-up">
        <div class="mobile-tags-select-wrapper">
            <div class="nice-select-wrapper">
                <div class="nice-select-wrapper__placeholder">Выбрать по тегу</div>
                <select class="nice-select ajax__filter" required>
                    <? foreach ($arResult['PRICE_LIST_ARRAY'] as $arDirections): ?>
                        <? if ($arDirections['SUBSECTIONS']): ?>
                            <option data-direction="<?= $arDirections['ID'] ?>">
                                #<?= mb_strtolower($arDirections['NAME']) ?></option>
                        <? endif; ?>
                    <? endforeach ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-7 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="dynamic-content-wrapper width-fix" id="change_content">
                <? foreach ($arResult['PRICE_LIST_ARRAY'] as $arItem): ?>
                    <? if ($arItem['SUBSECTIONS']): ?>
                        <div class="dynamic-content">
                            <div class="dynamic-content__heading">
                                <div class="dynamic-content__heading-row">
                                    <div class="dynamic-content__title c-gray-umber">
                                        #<?= strtolower($arItem['NAME']); ?></div>
                                    <a class="arrow-link" href="<?= $arItem['SECTION_PAGE_URL'] ?>"><span
                                                class="arrow-link__title">Подробнее об услуге</span></a>
                                </div>
                                <div class="dynamic-content__heading-row">
                                    <div class="category-items">
                                        <? foreach ($arItem['SUBSECTIONS'] as $arSubsection): ?>
                                            <div class="category-items__item" data-direction="<?= $arItem['ID'] ?>"
                                                 data-section="<?= $arSubsection['ID'] ?>">
                                                <span><?= $arSubsection['NAME'] ?></span>
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="dynamic-content__main">
                                <div class="catalog-item">
                                    <? foreach ($arItem['SUBSECTIONS'] as $arSubsection): ?>
                                        <? foreach ($arSubsection['ELEMENTS'] as $arItem): ?>
                                            <? if (count($arItem['PRICE_LIST']) > 0): ?>
                                                <div class="catalog-item__row">
                                                    <div class="catalog-item__row-inner">
                                                        <div class="catalog-item__title"><?= $arItem['NAME'] ?></div>
                                                        <svg class="arrowRightDefault" width="20" height="15">
                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                        </svg>
                                                    </div>
                                                    <div class="catalog-item__hidden">
                                                        <div class="catalog-item__text-item hidden-xs-down">
                                                            <p class="catalog-item__text">Узнать подробнее про
                                                                услугу:</p>
                                                            <a class="arrow-link"
                                                               href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><span
                                                                        class="arrow-link__title"><?= $arItem['NAME'] ?></span></a>
                                                        </div>
                                                        <div class="catalog-item__text-item hidden-sm-up">
                                                            <p class="catalog-item__text">Узнать про услугу:</p><a
                                                                    class="arrow-link"
                                                                    href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><span
                                                                        class="arrow-link__title">Подробнее</span></a>
                                                        </div>
                                                        <div class="catalog-item__list">
                                                            <? foreach ($arItem['PRICE_LIST'] as $arPrice): ?>
                                                                <div class="catalog-item__list-item">
                                                                    <div class="catalog-item__list-title"><?= $arPrice['NAME'] ?></div>
                                                                    <? if ($arPrice['PROPERTY_PRICE_VALUE']): ?>
                                                                        <div class="catalog-item__price-wrapper">
                                                                            <? if (empty($arPrice['PROPERTY_OLD_PRICE_VALUE'])): ?>
                                                                                <div class="catalog-item__price"><?= number_format($arPrice['PROPERTY_PRICE_VALUE'], 0, '.', ' ') . '₽';  ?></div>
                                                                            <? else: ?>
                                                                                <div class="catalog-item__price catalog-item__price_old"><?= number_format($arPrice['PROPERTY_OLD_PRICE_VALUE'], 0, '.', ' ') . '₽';  ?></div>
                                                                                <div class="catalog-item__price catalog-item__price_new"><?= number_format($arPrice['PROPERTY_PRICE_VALUE'], 0, '.', ' ') . '₽';  ?></div>
                                                                            <? endif; ?>
                                                                        </div>
                                                                    <? endif; ?>
                                                                </div>
                                                            <? endforeach; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? endif ?>
                                        <? endforeach; ?>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
        </div>
        <div class="col col-4 col-xl-4 col-lg-12 col-md-12 hidden-sm-down offset-1 offset-xl-0 offset-lg-0 offset-md-0 offset-sm-0 offset-xs-0">
            <div class="modal-card-wrapper">
                <div class="modal-card">
                    <div class="modal-card__icon-wrapper">
                        <div class="modal-card__icon-bg bg-linen"></div>
                        <svg class="patient-reviews" width="84px" height="84px">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#patient-reviews"></use>
                        </svg>
                    </div>
                    <div class="modal-card__title">Помогите нам стать лучше — оцените качество услуг!</div>
                    <a class="btn btn_primary modal-open" href="#add-review">
                        <span>Оставить отзыв</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>