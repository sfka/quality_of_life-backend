<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arResult */
/** @var array $arParams */

use \IL\Catalog;

$arResult['ITEMS'] = Catalog::getSectionList($arParams['IBLOCK_ID']);