<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<? foreach ($arResult["ITEMS"] as $arItems): ?>
    <? if ($arItems['ID'] === 16 || $arItems['NAME'] === 'Лицензии'): ?>
        <section class="section s-cards s-licenses">
            <div class="row section__top-row">
                <div class="col col-12">
                    <div class="section__top-row-inner scrl fadeInUp">
                        <div class="section-title">
                            <?= $arItems['NAME'] ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col col-12">
                    <div class="grid grid_four-col scrl fadeInUp">
                        <? foreach ($arItems["ITEMS"] as $arItem): ?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);

                            $arFile = CFile::GetPath($arItem['PROPERTIES']['FILE']['VALUE']);
                            ?>
                            <a class="default-card license" href="<?= $arFile ?>" id="<?= $this->GetEditAreaId($arItem['ID']); ?>" target="_blank">
                                <div class="default-card__inner">
                                    <div class="default-card__icon-wrapper">
                                        <div class="default-card__icon-bg bg-linen"></div>
                                        <svg class="icon__document" width="90px" height="90px">
                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#document"></use>
                                        </svg>
                                    </div>
                                    <div class="default-card__content">
                                        <div class="default-card__title dotdotdot-text">
                                            <?= $arItem['NAME'] ?>
                                        </div>
                                        <div class="default-card__moving-arrow">
                                            <svg class="arrowRightLong" width="46" height="24">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightLong"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </section>
    <? else: ?>
        <section class="section s-cards s-certificates">
            <div class="row section__top-row">
                <div class="col col-12">
                    <div class="section__top-row-inner scrl fadeInUp">
                        <div class="section-title">
                            <?= $arItems['NAME'] ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col col-12">
                    <div class="grid grid_four-col scrl fadeInUp">
                        <? foreach ($arItems["ITEMS"] as $arItem): ?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);

                            $arFile = CFile::GetPath($arItem['PROPERTIES']['FILE']['VALUE']);
                            $thumb = CFile::ResizeImageGet($arItem['PROPERTIES']['FILE']['VALUE'], [
                                'width' => 328,
                                'height' => 464
                            ], BX_RESIZE_IMAGE_EXACT, true, []);
                            ?>
                            <a class="certificate-card gallery-img" href="<?= $thumb['src'] ?>" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                                <img class="certificate-card__img" src="<?= $thumb['src'] ?>" alt="<?= $arItem['NAME'] ?>"/>
                            </a>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </section>
    <? endif ?>
<? endforeach; ?>