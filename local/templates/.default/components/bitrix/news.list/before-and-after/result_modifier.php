<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arBA = [];
$arSection = [];
foreach($arResult['ITEMS'] as $arItem) {
    $nav = CIBlockSection::GetNavChain($arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID']);
    while($arSectionPath = $nav->GetNext()) {
        if($arSectionPath['DEPTH_LEVEL'] == 1) {
            $arSection[$arSectionPath['ID']]['NAME'] = $arSectionPath['NAME'];
            $arSection[$arSectionPath['ID']]['ELEMENTS'][$arItem['ID']] = $arItem;
            foreach ($arItem['PROPERTIES']['BEFORE_AFTER']['VALUE'] as $keyBeforeafter => $beforeafter) {
                $arSection[$arSectionPath['ID']]['ELEMENTS'][$arItem['ID']]['PICTURE'][$keyBeforeafter]['VALUE'] = $beforeafter;
                $arSection[$arSectionPath['ID']]['ELEMENTS'][$arItem['ID']]['PICTURE'][$keyBeforeafter]['DESCRIPTION'] = $arItem['PROPERTIES']['BEFORE_AFTER']['DESCRIPTION'][$keyBeforeafter];
            }
        }
    }
}
$arResult['ITEMS'] = $arSection;
unset($arSection);