<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest();
?>
<div class="col col-8 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
    <div class="hidden-lg-up">
        <div class="mobile-tags-select-wrapper">
            <div class="nice-select-wrapper">
                <div class="nice-select-wrapper__placeholder">Выбрать по тегу</div>
                <select class="nice-select" required>
                    <?foreach($arResult['ITEMS'] as $arItem): ?>
                        <option>#<?=mb_strtolower($arItem['NAME']); ?></option>
                    <?endforeach; ?>
                </select>
            </div>
        </div>
    </div>
    <div class="dynamic-content-wrapper">
        <?foreach($arResult['ITEMS'] as $arItem): ?>
        <div class="dynamic-content">
            <div class="dynamic-content__heading">
                <div class="dynamic-content__heading-row">
                    <div class="dynamic-content__title c-gray-umber">#<?=mb_strtolower($arItem['NAME']); ?></div>
                </div>
            </div>
            <div class="dynamic-content__main">
                <div class="grid grid_two-col">
                    <?foreach($arItem['ELEMENTS'] as $arElement): ?>
                    <div class="before-after-card item" data-id="<?=$arElement['ID'] ?>">
                        <a class="before-after-card__link before-after-photo-open" href="javascript:void(0);" data-modal-class="before-after-photo-modal" data-type="center">
                            <!-- resize img -->
                            <?$img = CFile::ResizeImageGet($arElement['PICTURE'][0]['VALUE'], ['width' => 400, 'height' => 400], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []); ?>
                            <img class="before-after-card__img lazy" data-src="<?=$img['src'] ?>" alt="" />
                            <div class="before-after-card__row">
                                <div class="before-after-card__title"><?=$arElement['NAME'] ?></div>
                                <svg class="arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </a>
                        <div class="before-after-photo-modal modal__inner modal_center mfp-hide">
                            <button class="modal__close-btn js-modal-close" onclick="changeCommentBA()">
                                <svg class="icon__close" width="24" height="24">
                                    <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                </svg>
                            </button>
                            <div class="modal__content modal-before-after-slider">
                                <div class="row modal__top-row">
                                    <div class="col col-12">
                                        <div class="modal__top-row-inner">
                                            <div class="modal__title modal__title_big"><?=$arElement['NAME'] ?></div>
                                            <div class="swiper-controls">
                                                <div class="swiper-pagination"></div>
                                                <div class="swiper-buttons">
                                                    <div class="swiper-button-prev" onclick="changeCommentBA('<?=$arElement['ID'] ?>')">
                                                        <svg class="icon__arrowRightDefault" width="32" height="24">
                                                            <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                        </svg>

                                                    </div>
                                                    <div class="swiper-button-next" onclick="changeCommentBA('<?=$arElement['ID'] ?>')">
                                                        <svg class="icon__arrowRightDefault" width="32" height="24">
                                                            <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                        </svg>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row modal__main-row">
                                    <div class="col col-8 col-md-12 col-sm-12 col-xs-12">
                                        <div class="default-slider">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <? foreach ($arElement['PICTURE'] as $key => $picture): ?>
                                                        <div class="swiper-slide" data-slide="<?=$key + 1 ?>">
                                                            <?$img = CFile::ResizeImageGet($picture['VALUE'], ['width' => 800, 'height' => 800], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []); ?>
                                                            <img class="before-after-item__img swiper-lazy" <?=(count($arElement['PICTURE']) > 1 ? 'data-' : false)?>src="<?=$img['src'] ?>" alt="" />
                                                            <div class="before-after-item__row">
                                                                <div class="row">
                                                                    <div class="col col-6">
                                                                        <div class="before-after-item__text">До</div>
                                                                    </div>
                                                                    <div class="col col-6">
                                                                        <div class="before-after-item__text">После</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col col-12 hidden-md-up">
                                                <div class="mobile-swiper-controls">
                                                    <div class="swiper-controls">
                                                        <div class="swiper-pagination"></div>
                                                        <div class="swiper-buttons">
                                                            <div class="swiper-button-prev" onclick="changeCommentBA('<?=$arElement['ID'] ?>')">
                                                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                                                    <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                </svg>

                                                            </div>
                                                            <div class="swiper-button-next" onclick="changeCommentBA('<?=$arElement['ID'] ?>')">
                                                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                                                    <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                                </svg>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-4 col-md-12 col-sm-12 col-xs-12">
                                        <div class="answer-item">
                                            <div class="answer-item__title">Комментарий специалиста:</div>
                                            <? foreach ($arElement['PICTURE'] as $keyPicture => $picture): ?>
                                                <p class="answer-item__description <?=($keyPicture == 0 ? 'active' : false) ?>" data-parent="<?=$arElement['ID'] ?>" data-id="<?=$keyPicture+1 ?>"><?=$arElement['PICTURE'][$keyPicture]['DESCRIPTION'] ?></p>
                                            <?endforeach; ?>
                                            <a class="arrow-link" href="<?=$arElement['DETAIL_PAGE_URL'] ?>"><span class="arrow-link__title">Подробнее об услуге</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?endforeach; ?>
                </div>
            </div>
        </div>
        <?endforeach; ?>
    </div>
</div>