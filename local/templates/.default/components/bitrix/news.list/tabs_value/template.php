<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="section s-company-values">
    <div class="row">
        <div class="col col-10 col-md-12 col-sm-12 col-xs-12 offset-1 offset-md-0 offset-sm-0 offset-xs-0">
            <div class="row section__top-row">
                <div class="col col-12">
                    <div class="section-title scrl fadeInUp">Ценности клиники</div>
                    <div class="tabs scrl fadeInUp">
                        <? $count = 0; ?>
                        <? foreach ($arResult['ITEMS'] as $arItem): ?>
                            <div class="tabs__item <?= ($count == 0 ? 'active' : false) ?>"><?= $arItem['NAME'] ?></div>
                            <? $count++; ?>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="tags-content-wrapper scrl fadeInUp">
                <? $count = 0; ?>
                <? foreach ($arResult['ITEMS'] as $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                    ?>
                    <div class="tabs-content <?= ($count == 0 ? 'active' : false) ?>"
                         id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <div class="row">
                            <div class="col col-6 col-xs-12">
                                <div class="s-about__img-wrapper">
                                    <img class="s-about__img s-about__background lazy"
                                         data-src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"
                                         alt="<?= $arItem['NAME'] ?>">
                                    <img class="s-about__img s-about__foreground lazy"
                                         data-src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"
                                         alt="<?= $arItem['NAME'] ?>">
                                </div>
                            </div>
                            <div class="col col-6 col-xs-12">
                                <div class="section__col">
                                    <p class="important-paragraph"><?= $arItem['PREVIEW_TEXT'] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? $count++; ?>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</section>