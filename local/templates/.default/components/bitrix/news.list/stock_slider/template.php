<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($arResult["ITEMS"]): ?>
    <section class="section s-stock-slider mb-less scrl fadeInUp">
        <div class="row section__top-row">
            <div class="col col-12">
                <div class="section__top-row-inner">
                    <div class="section-title">Акции и скидки</div>
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-12">
                <div class="default-slider">
                    <div class="swiper-container not-in-modal">
                        <div class="swiper-wrapper">
                            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                                <?
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                                ?>
                                <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                                    <a class="stock-card stock-card_short" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                        <div class="stock-card__row bg-honey-dew"
                                             style="background-color:<?= '#' . ($arItem['PROPERTIES']['BACKGROUND']['VALUE'] ? $arItem['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') ?>">
                                            <? if ($arItem['PROPERTIES']['DISCOUNT']['VALUE'] || $arItem['PROPERTIES']['PRICE']['VALUE']): ?>
                                                <div class="stock-card__col">
                                                    <div class="stock-card__group">
                                                        <? if ($arItem['PROPERTIES']['DISCOUNT']['VALUE']): ?>
                                                            <div class="discount"><?= $arItem['PROPERTIES']['DISCOUNT']['VALUE'] ?></div>
                                                        <? endif; ?>
                                                        <? if ($arItem['PROPERTIES']['PRICE']['VALUE']): ?>
                                                            <div class="price">
                                                                <div class="price__item"><span
                                                                            class="price__text">Цена:</span><span
                                                                            class="price__value"><?= $arItem['PROPERTIES']['PRICE']['VALUE'] ?></span>
                                                                </div>
                                                            </div>
                                                        <? endif; ?>
                                                    </div>
                                                </div>
                                            <? endif; ?>

                                            <div class="stock-card__img-wrapper">
                                                <div class="stock-card__img-mask"></div>
                                                <img class="stock-card__img lazy"
                                                     data-src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"
                                                     alt="<?= $arItem['NAME'] ?>"/>
                                            </div>
                                        </div>
                                        <div class="stock-card__row">
                                            <span class="stock-card__date">
                                                <?= (!empty($arItem['DATE_ACTIVE_FROM'])) ? 'c ' . mb_strtolower(FormatDateFromDB($arItem['DATE_ACTIVE_FROM'], "j F")) : ''; ?>
                                                <?= (!empty($arItem['DATE_ACTIVE_TO'])) ? ' по ' . mb_strtolower(FormatDateFromDB($arItem['DATE_ACTIVE_TO'], "j F")) : ''; ?>
                                            </span>
                                            <div class="stock-card__col">
                                                <div class="stock-card__title dotdotdot-text"><?= $arItem['NAME'] ?></div>
                                                <div class="stock-card__moving-arrow">
                                                    <svg class="arrowRightLong" width="36" height="18">
                                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightLong"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-12 hidden-md-up">
                <div class="mobile-swiper-controls">
                    <div class="swiper-controls not-in-modal">
                        <div class="swiper-buttons">
                            <div class="swiper-button-prev">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg class="icon__arrowRightDefault" width="32" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif; ?>