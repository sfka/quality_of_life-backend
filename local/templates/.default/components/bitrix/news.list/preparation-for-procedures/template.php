<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arItem = current($arResult['ITEMS']);
?>

<div class="col col-8 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
    <div class="row">
        <div class="col col-11 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="content-container">
                <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
                    <p>
                        <?= $arItem['PREVIEW_TEXT'] ?>
                    </p>
                <? endif ?>
                <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/warning.php", ["SHOW_BORDER" => false]); ?>
                <? if (!empty($arItem['DETAIL_TEXT'])): ?>
                    <p>
                        <?= $arItem['DETAIL_TEXT'] ?>
                    </p>
                <? endif ?>
            </div>
            <? if (!empty($arItem['DETAIL_TEXT'])): ?>
                <div class="promo-photo-block scrl fadeInUp">
                    <div class="promo-photo-block__main-row">
                        <img class="promo-photo-block__img promo-photo-block__img_background lazy" data-src="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>" alt="<?= $arItem['DETAIL_PICTURE']['ALT'] ?>">
                        <img class="promo-photo-block__img promo-photo-block__img_foreground lazy" data-src="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>" alt="<?= $arItem['DETAIL_PICTURE']['ALT'] ?>">
                    </div>
                    <div class="promo-photo-block__bottom-row">
                        <p class="promo-photo-block__description c-white-aluminum">
                            <?= $arItem['DETAIL_PICTURE']['DESCRIPTION'] ?>
                        </p>
                    </div>
                </div>
            <? endif ?>
        </div>
    </div>
</div>