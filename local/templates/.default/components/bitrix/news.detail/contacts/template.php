<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="section s-contacts-info scrl fadeInUp">
    <div class="row">
        <div class="col col-12">
            <div class="contacts-list">
                <? if (!empty($arResult['PROPERTIES']['PHONES']['VALUE'])): ?>
                    <div class="contacts-list__item">
                        <div class="contacts-list__title"><?= $arResult['PROPERTIES']['PHONES']['NAME'] ?>:</div>
                        <div class="contacts-list__content">
                            <? foreach ($arResult['PROPERTIES']['PHONES']['VALUE'] as $phone): ?>
                                <a class="contacts-list__link" href="tel:<?= preg_replace('~\D+~', '', $phone) ?>">
                                    <?= $phone ?>
                                </a>
                            <? endforeach ?>
                        </div>
                    </div>
                <? endif ?>
                <? if (!empty($arResult['PROPERTIES']['EMAIL']['VALUE'])): ?>
                    <div class="contacts-list__item">
                        <div class="contacts-list__title"><?= $arResult['PROPERTIES']['EMAIL']['NAME'] ?>:</div>
                        <div class="contacts-list__content">
                            <a class="contacts-list__link" href="mailto:<?= $arResult['PROPERTIES']['EMAIL']['VALUE'] ?>">
                                <?= $arResult['PROPERTIES']['EMAIL']['VALUE'] ?>
                            </a>
                        </div>
                    </div>
                <? endif ?>
                <? if (!empty($arResult['PROPERTIES']['SCHEDULE']['VALUE'])): ?>
                    <div class="contacts-list__item">
                        <div class="contacts-list__title"><?= $arResult['PROPERTIES']['SCHEDULE']['NAME'] ?>:</div>
                        <div class="contacts-list__content">
                            <p class="contacts-list__text">
                                <? foreach ($arResult['PROPERTIES']['SCHEDULE']['VALUE'] as $schedule): ?>
                                    <?= $schedule ?><br>
                                <? endforeach ?>
                            </p>
                        </div>
                    </div>
                <? endif ?>
                <? if (!empty($arResult['PROPERTIES']['ADDRESS']['VALUE'])): ?>
                    <div class="contacts-list__item">
                        <div class="contacts-list__title"><?= $arResult['PROPERTIES']['ADDRESS']['NAME'] ?>:</div>
                        <div class="contacts-list__content">
                            <p class="contacts-list__text">
                                <?= $arResult['PROPERTIES']['ADDRESS']['VALUE'] ?>
                            </p>
                        </div>
                    </div>
                <? endif ?>
                <? if (!empty($arResult['PROPERTIES']['SOCILAS']['VALUE'])): ?>
                    <div class="contacts-list__item">
                        <div class="contacts-list__title"><?= $arResult['PROPERTIES']['SOCILAS']['NAME'] ?>:</div>
                        <div class="contacts-list__content">
                            <div class="social">
                                <? foreach ($arResult['PROPERTIES']['SOCILAS']['VALUE'] as $social): ?>
                                    <? $arSocialInfo = current(\IL\Catalog::getElementList($arResult['PROPERTIES']['SOCILAS']['LINK_IBLOCK_ID'], ['ID' => $social])) ?>
                                    <a class="social__link" href="<?= !empty($arSocialInfo['PROPERTIES']['LINK']['VALUE']) ? $arSocialInfo['PROPERTIES']['LINK']['VALUE'] : 'javascript:void(0);' ?>"<?= !empty($arSocialInfo['PROPERTIES']['LINK']['VALUE']) ? ' target="_blank"' : '' ?>>
                                        <? if (!empty($arSocialInfo['PROPERTIES']['ICO']['VALUE'])): ?>
                                            <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($arSocialInfo['PROPERTIES']['ICO']['VALUE'])) ?>
                                        <? endif ?>
                                    </a>
                                <? endforeach ?>
                            </div>
                        </div>
                    </div>
                <? endif ?>
            </div>
        </div>
    </div>
</div>
<? if (!empty($arResult['PROPERTIES']['MAPS']['VALUE'])): ?>
    <div class="section s-map scrl fadeInUp">
        <div class="row">
            <div class="col col-12">
                <div class="map" id="maps"></div>
            </div>
        </div>
    </div>
    <script>
        if ($('#maps').length) {
            LazyLoad.js('https://api-maps.yandex.ru/2.1/?apikey=<?= \Bitrix\Main\Config\Option::get("fileman", "yandex_map_api_key") ?>&load=package.standard&lang=ru-RU', () => {
                yandexMapInit([<?= $arResult['PROPERTIES']['MAPS']['VALUE'] ?>]);
            });
        }
        yandexMapInit = function (coords) {
            $("#maps").length && ymaps.ready(function () {
                var myMap = new ymaps.Map("maps", {
                        center: coords,
                        zoom: 18,
                        controls: ["zoomControl"]
                    }, {searchControlProvider: "yandex#search"}),
                    circleLayout = ymaps.templateLayoutFactory.createClass(
                        '<svg class="pulse" x="0px" y="0px" width="60px" height="60px">' +
                        '<circle class="pulse-disk" cx="30" cy="30" r="12px" />' +
                        '<circle class="pulse-circle" cx="30" cy="30" stroke-width="1" r="10px" />' +
                        '<circle class="pulse-circle-2" cx="30" cy="30" stroke-width="1" r="20px" />' +
                        '</svg>'
                    ),
                    circlePlacemark = new ymaps.Placemark(coords, {}, {
                        iconLayout: circleLayout,
                        iconOffset: [-30, -30],
                        iconShape: {type: "Circle", coordinates: [0, 0], radius: 25}
                    });
                myMap.geoObjects.add(circlePlacemark)
            })
        }
    </script>
<? endif ?>
<? if (!empty($arResult['PROPERTIES']['HOW_THERE']['VALUE'])): ?>
    <section class="section s-about s-direction scrl fadeInUp">
        <div class="row">
            <? if (!empty($arResult['PROPERTIES']['PARKING']['VALUE'])): ?>
                <div class="col col-6 col-sm-12 col-xs-12">
                    <div class="s-about__img-wrapper s-about__img-wrapper_unique">
                        <img class="s-about__img s-about__background lazy" data-src="<?= CFile::GetPath($arResult['PROPERTIES']['PARKING']['VALUE']) ?>" alt="parking">
                        <img class="s-about__img s-about__foreground lazy" data-src="<?= CFile::GetPath($arResult['PROPERTIES']['PARKING']['VALUE']) ?>" alt="parking">
                    </div>
                    <span class="s-about__text">Бесплатная охраняемая парковка</span>
                </div>
            <? endif ?>
            <div class="col col-5 col-md-6 col-sm-12 col-xs-12">
                <div class="section__col">
                    <div class="info-list">
                        <h3 class="info-list__title">
                            <?= $arResult['PROPERTIES']['HOW_THERE']['NAME'] ?>
                        </h3>
                        <div class="info-list__list">
                            <? foreach ($arResult['PROPERTIES']['HOW_THERE']['VALUE'] as $keyThere => $arHowThere): ?>
                                <div class="info-list__item">
                                    <div class="info-list__item-title"><?= $arResult['PROPERTIES']['HOW_THERE']['DESCRIPTION'][$keyThere] ?></div>
                                    <p class="info-list__item-description">
                                        <?= $arHowThere['TEXT'] ?>
                                    </p>
                                </div>
                            <? endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif ?>
<? if (!empty($arResult['PROPERTIES']['REQUISITES']['VALUE']) || !empty($arResult['PROPERTIES']['PAYMENT_DETAILS']['VALUE'])): ?>
    <div class="section s-details">
        <? if (!empty($arResult['PROPERTIES']['REQUISITES']['VALUE'])): ?>
            <div class="row section__top-row">
                <div class="col col-12">
                    <div class="section__top-row-inner scrl fadeInUp">
                        <div class="section-title">
                            <?= $arResult['PROPERTIES']['REQUISITES']['NAME'] ?>
                        </div>
                    </div>
                </div>
            </div>
        <? endif ?>
        <div class="row scrl fadeInUp">
            <? if (!empty($arResult['PROPERTIES']['REQUISITES']['VALUE'])): ?>
                <div class="col col-5 col-xs-12">
                    <div class="details">
                        <p class="details__title">Общество с ограниченной ответственностью «Качество жизни»</p>
                        <ul class="details__list">
                            <? foreach ($arResult['PROPERTIES']['REQUISITES']['VALUE'] as $arRequisites): ?>
                                <li><?= $arRequisites ?></li>
                            <? endforeach ?>
                        </ul>
                    </div>
                </div>
            <? endif ?>
            <? if (!empty($arResult['PROPERTIES']['PAYMENT_DETAILS']['VALUE'])): ?>
                <div class="col col-5 col-xs-12 offset-1 offset-xs-0">
                    <div class="details">
                        <p class="details__title"><?= $arResult['PROPERTIES']['PAYMENT_DETAILS']['NAME'] ?>:</p>
                        <ul class="details__list">
                            <? foreach ($arResult['PROPERTIES']['PAYMENT_DETAILS']['VALUE'] as $arPaymentDetails): ?>
                                <li><?= $arPaymentDetails ?></li>
                            <? endforeach ?>
                        </ul>
                    </div>
                </div>
            <? endif ?>
        </div>
    </div>
<? endif ?>
<div class="section s-feedback scrl fadeInUp">
    <div class="row">
        <div class="col col-4 col-xl-5 col-lg-5 col-md-6 col-sm-12 col-xs-12 offset-1 offset-xl-0 offset-lg-0 offset-md-0 offset-sm-0 offset-xs-0">
            <div class="feedback-form-wrapper">
                <div class="feedback-form-wrapper__title">Важно каждое мнение</div>
                <p class="feedback-form-wrapper__description">Ваши пожелания и замечания помогают нам совершенствовать качество услуг и сервиса. Напишите нам, и мы станем еще лучше для вас!</p>
                <form class="default-form" action="<?= SITE_AJAX_PATH ?>/form_wishes.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="utm_source" value="<?= $_GET['utm_source'] ?>">
                    <input type="hidden" name="utm_medium" value="<?= $_GET['utm_medium'] ?>">
                    <input type="hidden" name="utm_campaign" value="<?= $_GET['utm_campaign'] ?>">
                    <input type="hidden" name="utm_content" value="<?= $_GET['utm_content'] ?>">
                    <input type="hidden" name="utm_term" value="<?= $_GET['utm_term'] ?>">
                    <div class="default-form__one-column w-100">
                        <div class="default-form__input-wrapper">
                            <input class="default-form__input" type="text" name="name" required>
                            <span class="default-form__input-placeholder">Ваше имя *</span>
                            <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                        </div>
                        <div class="default-form__input-wrapper">
                            <input class="default-form__input" type="email" name="email" required>
                            <span class="default-form__input-placeholder">Ваш e–mail *</span>
                            <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                        </div>
                        <div class="default-form__input-wrapper">
                            <textarea class="default-form__input" name="message" required></textarea>
                            <span class="default-form__input-placeholder">Задать вопрос*</span>
                            <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                        </div>
                    </div>
                    <div class="default-form__btn-wrapper">
                        <button class="btn btn_primary">
                            <span>Отправить вопрос</span>
                        </button>
                    </div>
                    <p class="default-form__text default-form__text_one-column">Я даю согласие на обработку,
                        <a href='/terms-of-use/'>персональных данных</a>
                    </p>
                </form>
            </div>
        </div>
        <div class="col col-4 col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12 offset-1 offset-sm-0 offset-xs-0">
            <div class="s-feedback__col">
                <? if (!empty($arResult['PREVIEW_TEXT'])): ?>
                    <p class="important-paragraph">
                        <?= $arResult['PREVIEW_TEXT'] ?>
                    </p>
                <? endif ?>
                <? if (!empty($arResult['PREVIEW_PICTURE']['SRC'])): ?>
                    <div class="contact-person">
                        <div class="contact-person__img-wrapper">
                            <div class="contact-person__background"></div>
                            <img class="contact-person__img lazy" data-src="<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arResult['PREVIEW_PICTURE']['DESCRIPTION'] ?>">
                        </div>
                        <div class="contact-person__content">
                            <div class="contact-person__name"><?= $arResult['PREVIEW_PICTURE']['DESCRIPTION'] ?></div>
                            <div class="contact-person__position"><?= $arResult['DETAIL_TEXT'] ?></div>
                        </div>
                    </div>
                <? endif ?>
            </div>
        </div>
    </div>
</div>
<style>
    .contacts-list .social__link svg {
        width: 17px;
        height: 17px;
    }
</style>