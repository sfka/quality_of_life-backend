<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$res = \CIBlockElement::GetList([], ['IBLOCK_ID' => SITE_SERVICE_IBLOCK_ID, 'PROPERTY_PROBLEMS_SOLUTIONS' => $arResult['ID']], false, false, ['ID', 'NAME', 'IBLOCK_SECTION_ID']);
while($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arResult['LINK_SUBSECTIONS'][] = $arFields['IBLOCK_SECTION_ID'];
    $arResult['LINK_ELEMENTS'][$arFields['ID']] = $arFields['ID'];
    $elements[$arFields['ID']] = $arFields['ID'];
}

foreach ($elements as $key => $elementID) {
    $res = \CIBlockElement::GetList([], ['IBLOCK_ID' => SITE_PRICE_IBLOCK_ID, 'PROPERTY_SERVICE' => $elementID], false, false, ['ID', 'NAME', 'PROPERTY_SERVICE', 'PROPERTY_DIRECTION', 'PROPERTY_PRICE', 'PROPERTY_OLD_PRICE']);
    while($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $resSection = \CIBlockSection::GetByID($arFields['PROPERTY_DIRECTION_VALUE'])->Fetch();
        $arResult['LINK_STOCK_SECTIONS'][$resSection['ID']] = $resSection['ID'];
        $resElement = \CIBlockElement::GetList([], ['IBLOCK_ID' => SITE_SERVICE_IBLOCK_ID, 'ID' => $arFields['PROPERTY_SERVICE_VALUE']], false, false, ['ID', 'NAME', 'DETAIL_PAGE_URL']);
        while($obElement = $resElement->GetNextElement()) {
            $arFieldsElement = $obElement->GetFields();
        }
        if ($resSection['DEPTH_LEVEL'] == 2) {
            $price[$arFields['PROPERTY_DIRECTION_VALUE']]['ID'] = $arFields['PROPERTY_DIRECTION_VALUE'];
            $price[$arFields['PROPERTY_DIRECTION_VALUE']]['SERVICE'][$arFields['PROPERTY_SERVICE_VALUE']]['ID'] = $arFields['PROPERTY_SERVICE_VALUE'];
            $price[$arFields['PROPERTY_DIRECTION_VALUE']]['SERVICE'][$arFields['PROPERTY_SERVICE_VALUE']]['NAME'] = $arFieldsElement['NAME'];
            $price[$arFields['PROPERTY_DIRECTION_VALUE']]['SERVICE'][$arFields['PROPERTY_SERVICE_VALUE']]['DETAIL_PAGE_URL'] = $arFieldsElement['DETAIL_PAGE_URL'];
            $price[$arFields['PROPERTY_DIRECTION_VALUE']]['SERVICE'][$arFields['PROPERTY_SERVICE_VALUE']]['PRICELIST'][$arFields['ID']] = $arFields;
        }
    }
}
$arResult['PRICE_LIST'] = $price;

// формируем акцию
$res = \CIBlockElement::GetList(['DATE_ACTIVE_FROM' => 'DESC'], ['IBLOCK_ID' => SITE_STOCK_IBLOCK_ID, 'PROPERTY_SERVICE' => $arResult['LINK_ELEMENTS']], false, ['nPageSize' => 1], ['ID', 'NAME', 'DATE_ACTIVE_FROM', 'PREVIEW_TEXT'])->Fetch();
$arResult['LAST_STOCK'] = $res;
// формируем ID раздела для прайса
$resElement = \CIBlockElement::GetList([], ['IBLOCK_ID' => SITE_SERVICE_IBLOCK_ID, 'PROPERTY_PROBLEMS_SOLUTIONS' => $arResult['ID']], false, false, ['ID', 'NAME', 'IBLOCK_SECTION_ID']);
while($obElement = $resElement->GetNextElement()) {
    $arFieldsElement = $obElement->GetFields();
    $nav = \CIBlockSection::GetNavChain(false, $arFieldsElement['IBLOCK_SECTION_ID']);
    while ($arSectionPath = $nav->GetNext()) {
        if ($arSectionPath['DEPTH_LEVEL'] == 1) {
            $sectionIDs[]['VALUE'] = $arSectionPath['ID'];
        }
    }
}

$arResult['PRICE_LIST_ARRAY'] = \IL\Price::getPriceBySectionID($sectionIDs);