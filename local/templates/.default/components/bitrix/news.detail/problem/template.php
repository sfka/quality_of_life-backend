<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? $this->SetViewTarget('detail_promo_class'); ?> pb-less<? $this->EndViewTarget(); ?>

<? $this->SetViewTarget('detail_promo'); ?>
<div class="detail-promo">
    <div class="common-container">
        <div class="row">
            <div class="col col-6 col-sm-12 col-xs-12">
                <div class="detail-promo__col">
                    <div class="detail-promo__top-row">
                        <div class="type">Проблемы и их решения</div>
                        <div class="category"><?= $arResult['SECTION']['PATH'][0]['NAME'] ?></div>
                    </div>
                    <div class="detail-promo__main-row sm-down-mb-0">
                        <div class="detail-promo__title"><?= $arResult['NAME'] ?></div>
                    </div>
                    <div class="detail-promo__bottom-row hidden-sm-down">
                        <a class="btn btn_primary modal-open" href="#consultation">
                            <span>Получить консультацию</span>
                        </a>
                    </div>
                </div>
            </div>
            <? if ($arResult['DETAIL_PICTURE']['SRC']): ?>
                <div class="col col-6 col-sm-12 col-xs-12">
                    <div class="detail-promo__img-wrapper">
                        <img class="detail-promo__img lazy" data-src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>">
                    </div>
                    <div class="detail-promo__bg-img"
                         style="background-image: url(<?= $arResult['DETAIL_PICTURE']['SRC'] ?>);"></div>
                </div>
            <? endif; ?>
        </div>
    </div>
</div>
<? $this->EndViewTarget(); ?>

<div class="row">
    <div class="col col-3 col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 sidebar-col">
        <div class="sidebar-wrapper">
            <aside class="sidebar" data-simplebar>
                <div class="sidebar__inner">
                    <? if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y") { ?>
                        <div class="share">
                            <div class="share__title">Поделиться</div>
                            <noindex>
                                <?
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.share",
                                    "share",
                                    [
                                        "HANDLERS" => '',
                                        "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                                        "PAGE_TITLE" => $arResult["~NAME"],
                                        "SHORTEN_URL_LOGIN" => '',
                                        "SHORTEN_URL_KEY" => '',
                                        "HIDE" => 'N',
                                    ],
                                    $component,
                                    ["HIDE_ICONS" => "Y"]
                                );
                                ?>
                            </noindex>
                        </div>
                    <? } ?>
                </div>
            </aside>
        </div>
    </div>
    <div class="col col-8 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div class="content-container">

            <section class="section mb-less">
                <div class="content-container">
                    <?= $arResult['~DETAIL_TEXT'] ?>
                </div>
            </section>
            <?
            //Подключение до / после
            $filter = ['!PROPERTY_BEFORE_AFTER' => false, 'PROPERTY_PROBLEMS_SOLUTIONS' => $arResult['ID']];
            $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/before_after.php", [
                "TEMPLATE" => "before_after_slider",
                "FILTER" => $filter,
                "DOP_FILTER" => ($arParams['BEFORE_AFTER_SECTION'] ? $arParams['BEFORE_AFTER_SECTION'] : false),
            ], ["SHOW_BORDER" => false]);

            //Подключение отзывов
            $filter = ['PROPERTY_SERVICE' => $arResult['LINK_SUBSECTIONS']];
            $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/reviews.php", [
                "TEMPLATE" => "reviews_slider",
                "FILTER" => $filter,
                "DOP_FILTER" => ($arParams['REVIEWS_SECTION'] ? $arParams['REVIEWS_SECTION'] : false),
            ], ["SHOW_BORDER" => false]);
            unset($elementInfo);

            //Подключение прайса
            ?>
            <?$APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/price_accordeon.php', [
                    'TITLE' => 'Прейскурант услуг участвующих в акции',
                    'PRICE_LIST_ARRAY' => $arResult['PRICE_LIST_ARRAY'],
                    "DOP_FILTER" => ($arParams['PRICE_SECTION'] ? $arParams['PRICE_SECTION'] : false),
            ], ['SHOW_BORDER' => false]); ?>

            <?// подключение акции
            if ($arResult['LAST_STOCK']):?>
                <?$APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/last_stock_form.php', ['LAST_STOCK' => $arResult['LAST_STOCK']], ['SHOW_BORDER' => false]) ?>
            <?endif; ?>
        </div>
    </div>
</div>
<div class="modal__inner modal_right modal-form mfp-hide" id="consultation">
    <button class="modal__close-btn js-modal-close">
        <svg class="icon__close" width="20" height="20">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </button>
    <div class="modal__content">
        <div class="modal__content-row">
            <div class="modal__title">Получить консультацию</div>
            <div class="modal__text">Администратор клиники свяжется с вами в ближайшее время</div>
        </div>
        <div class="modal__content-row">
            <form class="default-form" action="<?= SITE_AJAX_PATH ?>/form_problem.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="problem" value="<?= $arResult['ID'] ?>">
                <input type="hidden" name="utm_source" value="<?= $_GET['utm_source'] ?>">
                <input type="hidden" name="utm_medium" value="<?= $_GET['utm_medium'] ?>">
                <input type="hidden" name="utm_campaign" value="<?= $_GET['utm_campaign'] ?>">
                <input type="hidden" name="utm_content" value="<?= $_GET['utm_content'] ?>">
                <input type="hidden" name="utm_term" value="<?= $_GET['utm_term'] ?>">
                <div class="default-form__one-column">
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="text" name="name" required>
                        <span class="default-form__input-placeholder">Ваше имя *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="tel" name="phone" required>
                        <span class="default-form__input-placeholder">Ваш телефон *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                </div>
                <div class="default-form__btn-wrapper">
                    <button class="btn btn_primary">
                        <span>Отправить заявку</span>
                    </button>
                </div>
                <p class="default-form__text default-form__text_one-column">Я даю согласие на обработку,
                    <a href='/terms-of-use/'>персональных данных</a>
                </p>
            </form>
        </div>
    </div>
</div>