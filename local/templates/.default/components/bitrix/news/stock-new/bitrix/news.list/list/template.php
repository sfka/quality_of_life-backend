<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (count($arResult["ITEMS"]) > 0): ?>
    <div class="row section__top-row">
        <div class="col col-12">
            <div class="section__top-row-inner scrl fadeInUp">
                <div class="section-title">Другие акции и скидки</div>
            </div>
        </div>
    </div>
    <div class="row scrl fadeInUp">
        <div class="col col-12">
            <div class="grid grid_two-col">
                <? foreach ($arResult["ITEMS"] as $arItem): ?>
                    <a class="stock-card stock-card_short" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <div class="stock-card__row bg-magnolia">
                            <? if (!empty($arItem['PROPERTIES']['DISCOUNT']['VALUE']) || !empty($arItem['PROPERTIES']['PRICE']['VALUE'])): ?>
                                <div class="stock-card__col">
                                    <div class="stock-card__group">
                                        <? if (!empty($arItem['PROPERTIES']['DISCOUNT']['VALUE'])): ?>
                                            <div class="discount">Скидка <?= $arItem['PROPERTIES']['DISCOUNT']['VALUE'] ?></div>
                                        <? endif ?>
                                        <? if (!empty($arItem['PROPERTIES']['PRICE']['VALUE'])): ?>
                                            <div class="price">
                                                <div class="price__item">
                                                    <span class="price__text">Цена:</span>
                                                    <? if (!empty($arItem['PROPERTIES']['OLD_PRICE']['VALUE'])): ?>
                                                        <span class="price__value old">
                                                            <?= $arItem['PROPERTIES']['OLD_PRICE']['VALUE'] ?>
                                                        </span>
                                                        <span class="price__value new"><?= $arItem['PROPERTIES']['PRICE']['VALUE'] ?></span>
                                                    <? else: ?>
                                                        <span class="price__value">
                                                            <?= $arItem['PROPERTIES']['PRICE']['VALUE'] ?>
                                                        </span>
                                                    <? endif ?>
                                                </div>
                                            </div>
                                        <? endif ?>
                                    </div>
                                </div>
                            <? endif ?>
                            <? if (!empty($arItem['PREVIEW_PICTURE'])): ?>
                                <? $thumb = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], [
                                    'width' => $key === 960,
                                    'height' => 970,
                                ], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []); ?>
                                <div class="stock-card__img-wrapper">
                                    <div class="stock-card__img-mask"></div>
                                    <img class="stock-card__img lazy" data-src="<?= $thumb['src'] ?>" alt="<?= $arItem['NAME'] ?>"/>
                                </div>
                            <? endif ?>
                        </div>
                        <div class="stock-card__row">
                            <span class="stock-card__date">
                                <?= (!empty($arItem['DATE_ACTIVE_FROM'])) ? 'c ' . mb_strtolower(FormatDateFromDB($arItem['DATE_ACTIVE_FROM'], "j F")) : ''; ?>
                                <?= (!empty($arItem['DATE_ACTIVE_TO'])) ? ' по ' . mb_strtolower(FormatDateFromDB($arItem['DATE_ACTIVE_TO'], "j F")) : ''; ?>
                            </span>
                            <div class="stock-card__col">
                                <div class="stock-card__title dotdotdot-text">
                                    <?= $arItem["NAME"] ?>
                                </div>
                                <div class="stock-card__moving-arrow">
                                    <svg class="arrowRightLong" width="36" height="18">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightLong"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>
                <? endforeach ?>
            </div>
        </div>
    </div>
<? endif ?>