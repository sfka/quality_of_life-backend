<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */

/** @var array $APPLICATION */

use \IL\Catalog;
use \Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest();

$arResult['LEFT_MENU'] = Catalog::getLeftMenu($arParams['IBLOCK_ID']);
if ($ajax && !empty($request->get('DIRECTION'))) {
    $arResult['SERVICE'] = Catalog::getTabsMenu($arParams['IBLOCK_ID'], $request->get('DIRECTION'));
}