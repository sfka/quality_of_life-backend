<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;
use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest();
?>

<? if (!$ajax): ?>
    <? $this->SetViewTarget('left_tags'); ?>
    <div class="sidebar__tags-wrapper">
        <div class="sidebar__col">
            <div class="sidebar__title">Направления деятельности</div>
            <div class="sidebar__tags">
                <? foreach ($arResult['LEFT_MENU']['SECTIONS'] as $arDirections): ?>
                    <span class="sidebar__tags-item" data-direction="<?= $arDirections['ID'] ?>">
                        <span>#<?= mb_strtolower($arDirections['NAME']) ?></span>
                    </span>
                <? endforeach ?>
            </div>
        </div>
        <? if (!empty($arResult['LEFT_MENU']['TAGS'])): ?>
            <div class="sidebar__col">
                <div class="sidebar__title">Общие</div>
                <div class="sidebar__tags">
                    <? foreach ($arResult['LEFT_MENU']['TAGS'] as $arTags): ?>
                        <span class="sidebar__tags-item" data-section="<?= $arTags['ID'] ?>">
                            <span>#<?= mb_strtolower($arTags['NAME']) ?></span>
                        </span>
                    <? endforeach ?>
                </div>
            </div>
        <? endif ?>
    </div>
    <? $this->EndViewTarget(); ?>
<? endif ?>

<div class="col col-8 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
    <div class="hidden-lg-up">
        <div class="mobile-tags-select-wrapper">
            <div class="nice-select-wrapper">
                <div class="nice-select-wrapper__placeholder">Выбрать по тегу</div>
                <select class="nice-select ajax__filter" required>
                    <? foreach ($arResult['LEFT_MENU']['SECTIONS'] as $arDirections): ?>
                        <option data-direction="<?= $arDirections['ID'] ?>">
                            #<?= mb_strtolower($arDirections['NAME']) ?></option>
                    <? endforeach ?>
                    <? foreach ($arResult['LEFT_MENU']['TAGS'] as $arTags): ?>
                        <option data-section="<?= $arTags['ID'] ?>">#<?= mb_strtolower($arTags['NAME']) ?></option>
                    <? endforeach ?>
                </select>
            </div>
        </div>
    </div>
    <div class="dynamic-content-wrapper">
        <div class="dynamic-content">
            <? if ($ajax && (!empty($request->get('DIRECTION')) || !empty($request->get('SECTION'))) && empty($request->get('PAGEN_1'))): ?>
                <div class="dynamic-content__heading">
                    <div class="dynamic-content__heading-row">
                        <div class="dynamic-content__title c-gray-umber">
                            <? if (!empty($request->get('DIRECTION'))): ?>
                                #<?= mb_strtolower(Catalog::getSectionNameByID($arResult['ITEMS'][0]['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], $request->get('DIRECTION'))); ?>
                            <? elseif ($request->get('SECTION')): ?>
                                #<?= mb_strtolower(Catalog::getSectionNameByID($arParams['IBLOCK_ID'], $request->get('SECTION'))); ?>
                            <? endif ?>
                        </div>
                    </div>
                    <? if (!empty($arResult['SERVICE'])): ?>
                        <div class="dynamic-content__heading-row">
                            <div class="category-items">
                                <? foreach ($arResult['SERVICE'] as $arService): ?>
                                    <div class="category-items__item<?= ($arService['ID'] == $request->get('SERVICE')) ? ' active' : '' ?>"
                                         data-direction="<?= $arResult['ITEMS'][0]['PROPERTIES']['DIRECTION']['VALUE'] ?>"
                                         data-service="<?= $arService['ID'] ?>">
                                        <span><?= $arService['NAME'] ?></span>
                                    </div>
                                <? endforeach ?>
                            </div>
                        </div>
                    <? endif ?>
                </div>
            <? endif ?>
            <div class="dynamic-content__main">
                <div class="unique-grid ajax_list">
                    <? $i = 0; ?>
                    <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                           class="ajax_item stock-card<?= $i%3 === 0 ? ' stock-card_long' : ' stock-card_short' ?>">
                            <div class="stock-card__row"
                                 style="background-color:<?= '#' . ($arItem['PROPERTIES']['BACKGROUND']['VALUE'] ? $arItem['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') ?>">
                                <div class="stock-card__col" <?=($i%3 === 0 ? 'style="z-index:2"' : false) ?>>
                                    <? if (!empty($arItem['PROPERTIES']['DISCOUNT']['VALUE']) || !empty($arItem['PROPERTIES']['PRICE']['VALUE'])): ?>
                                        <div class="stock-card__group">
                                            <? if (!empty($arItem['PROPERTIES']['DISCOUNT']['VALUE'])): ?>
                                                <div class="discount"><?= $arItem['PROPERTIES']['DISCOUNT']['VALUE'] ?></div>
                                            <? endif ?>
                                            <? if (!empty($arItem['PROPERTIES']['PRICE']['VALUE'])): ?>
                                                <div class="price">
                                                    <div class="price__item">
                                                        <span class="price__text">Цена:</span>
                                                        <? if (!empty($arItem['PROPERTIES']['OLD_PRICE']['VALUE'])): ?>
                                                            <span class="price__value old">
                                                            <?= $arItem['PROPERTIES']['OLD_PRICE']['VALUE'] ?>
                                                        </span>
                                                            <span class="price__value new"><?= $arItem['PROPERTIES']['PRICE']['VALUE'] ?></span>
                                                        <? else: ?>
                                                            <span class="price__value">
                                                            <?= $arItem['PROPERTIES']['PRICE']['VALUE'] ?>
                                                        </span>
                                                        <? endif ?>
                                                    </div>
                                                </div>
                                            <? endif ?>
                                        </div>
                                    <? endif ?>
                                    <? if ($i%3 === 0): ?>
                                        <div class="stock-card__title dotdotdot-text">
                                            <?= $arItem['NAME'] ?>
                                        </div>
                                        <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
                                            <p class="stock-card__description">
                                                <?= $arItem['PREVIEW_TEXT'] ?>
                                            </p>
                                        <? endif ?>
                                        <object>
                                            <a class="arrow-link" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                                <span class="arrow-link__title">Подробнее</span>
                                            </a>
                                        </object>
                                        <span class="stock-card__date">
                                            <?= (!empty($arItem['DATE_ACTIVE_FROM'])) ? 'c ' . mb_strtolower(FormatDateFromDB($arItem['DATE_ACTIVE_FROM'], "j F")) : ''; ?>
                                            <?= (!empty($arItem['DATE_ACTIVE_TO'])) ? ' по ' . mb_strtolower(FormatDateFromDB($arItem['DATE_ACTIVE_TO'], "j F")) : ''; ?>
                                        </span>
                                    <? endif ?>
                                </div>
                                <? if (!empty($arItem['PREVIEW_PICTURE']['SRC'])): ?>
                                    <div class="stock-card__img-wrapper">
                                        <? if ($i%3 === 0): ?>
                                            <?
                                                $color = \IL\Utilities::hex2rgb('#' . ($arItem['PROPERTIES']['BACKGROUND']['VALUE'] ? $arItem['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5'), '0.5');
                                                $stockBlockImgMaskStyle = '
                                                background-image: -webkit-gradient(linear,right top,left top,from(' . $color . '),to(#' . ($arItem['PROPERTIES']['BACKGROUND']['VALUE'] ? $arItem['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') . '));
                                                background-image: -webkit-linear-gradient(right,' . $color . ',#' . ($arItem['PROPERTIES']['BACKGROUND']['VALUE'] ? $arItem['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') . ');
                                                background-image: linear-gradient(to left,' . $color . ',#' . ($arItem['PROPERTIES']['BACKGROUND']['VALUE'] ? $arItem['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') . ');
                                            ';
                                            ?>
                                            <div class="stock-block__img-mask"
                                                 style="<?= $stockBlockImgMaskStyle ?>"></div>
                                            <? if ($arItem['DETAIL_PICTURE']['SRC']): ?>
                                                <div class="stock-block__img lazy"
                                                     style="background-image: url(<?= $arItem['DETAIL_PICTURE']['SRC'] ?>)"></div>
                                            <? elseif ($arItem['PREVIEW_PICTURE']['SRC']) : ?>
                                                <div class="stock-block__img lazy"
                                                     style="background-image: url(<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>)"></div>
                                            <? endif; ?>
                                        <? else: ?>
                                            <div class="stock-card__img-mask"></div>
                                            <img class="stock-card__img lazy"
                                                 data-src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"
                                                 alt="<?= $arItem['NAME'] ?>"/>
                                        <? endif; ?>
                                    </div>
                                <? endif ?>
                            </div>
                            <? if ($i%3 !== 0): ?>
                                <div class="stock-card__row">
                                    <span class="stock-card__date">
                                        <?= (!empty($arItem['DATE_ACTIVE_FROM'])) ? 'c ' . mb_strtolower(FormatDateFromDB($arItem['DATE_ACTIVE_FROM'], "j F")) : ''; ?>
                                        <?= (!empty($arItem['DATE_ACTIVE_TO'])) ? ' по ' . mb_strtolower(FormatDateFromDB($arItem['DATE_ACTIVE_TO'], "j F")) : ''; ?>
                                    </span>
                                    <div class="stock-card__col">
                                        <div class="stock-card__title dotdotdot-text">
                                            <?= $arItem['NAME'] ?>
                                        </div>
                                        <div class="stock-card__moving-arrow">
                                            <svg class="arrowRightLong" width="36" height="18">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightLong"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            <? endif ?>
                        </a>
                        <? $i++; ?>
                    <? endforeach ?>
                    <? unset($i); ?>
                </div>
            </div>
        </div>
    </div>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <? if ($arResult["NAV_RESULT"]->nEndPage > 1 && $arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->nEndPage): ?>
            <div class="loader-wrapper show_more" style="opacity: 0"
                 data-show-more="<?= $arResult["NAV_RESULT"]->NavNum ?>"
                 data-next-page="<?= ($arResult["NAV_RESULT"]->NavPageNomer + 1) ?>"
                 data-max-page="<?= $arResult["NAV_RESULT"]->nEndPage ?>">
                <svg class="loader" version="1.1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 199.998 96.615"
                     enable-background="new 0 0 199.998 96.615">
                    <g>
                        <path fill="none" stroke="#E84849" stroke-width="2.5" stroke-linecap="round"
                              d="M198.746,59.841h-14.537c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371c-0.33,1.036-1.843,0.876-1.948-0.206l-3.747-53.409c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993c-0.031,0.074-0.104,0.122-0.184,0.122h-16.937l16.904-0.02h-32.537c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371c-0.33,1.036-1.843,0.876-1.948-0.206l-3.747-53.409c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993c-0.031,0.074-0.104,0.122-0.184,0.122H68.452l16.182-0.02H52.096c-0.086,0-0.163-0.056-0.19-0.138l-5.734-17.522c-0.06-0.185-0.322-0.183-0.381,0.002l-2.982,14.371c-0.33,1.036-1.843,0.876-1.948-0.206L37.114,2.938c-0.118-1.214-1.894-1.199-1.992,0.017l-5.761,91.31c-0.097,1.202-1.846,1.236-1.99,0.039l-4.031-43.504c-0.024-0.204-0.303-0.243-0.383-0.054l-3.803,8.993c-0.031,0.074-0.104,0.122-0.184,0.122H2.034"></path>
                    </g>
                </svg>
            </div>
        <? endif ?>
    <? endif; ?>
</div>