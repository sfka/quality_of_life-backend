<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \IL\Catalog;
use \IL\Settings;

function getPricelistItems($arItemsID)
{ // 19 - pricelist iblock id

    $arSelect = ["*", "PROPERTY_SERVICE", "PROPERTY_PRICE", "PROPERTY_OLD_PRICE"];
    $arFilter = ["IBLOCK_ID" => 19, "ID" => $arItemsID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"];
    $res = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
    while ($ob = $res->GetNext()) {
        $pricelistItems[$ob['ID']] = $ob;
        $id_services[$ob['ID']][] = $ob['PROPERTY_SERVICE_VALUE'];
    }

    foreach ($pricelistItems as $key => $value) {
        $pricelistItems[$key]['SERVICES'] = $id_services[$key];
    }

    return $pricelistItems;
}

function getServiceItems($arServiceItemsID)
{ // 3 - services iblock id

    $arSelect = ["*"];
    $arFilter = ["IBLOCK_ID" => 3, "ID" => $arServiceItemsID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"];
    $res = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
    while ($ob = $res->GetNext()) {
        $servicesItems[$ob['ID']] = $ob;
    }

    return $servicesItems;
}

function getBAItems($arServiceItemsID)
{ // 20 - before-after iblock id

    $arSelect = ["*", "PROPERTY_PHOTO_BEFORE", "PROPERTY_PHOTO_AFTER", "PROPERTY_SPEC_COM"];
    $arFilter = ["IBLOCK_ID" => 20, "PROPERTY_BA_SERVICE" => $arServiceItemsID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"];
    $res = CIBlockElement::GetList([], $arFilter, false, ['nTopCount' => '5'], $arSelect);
    while ($ob = $res->GetNext()) {

        if ($ob['PROPERTY_PHOTO_BEFORE_VALUE']) {
            $ob['PHOTO_BEFORE'] = CFile::GetFileArray($ob['PROPERTY_PHOTO_BEFORE_VALUE']);
            unset($ob['PROPERTY_PHOTO_BEFORE_VALUE']);
        }
        if ($ob['PROPERTY_PHOTO_AFTER_VALUE']) {
            $ob['PHOTO_AFTER'] = CFile::GetFileArray($ob['PROPERTY_PHOTO_AFTER_VALUE']);
            unset($ob['PROPERTY_PHOTO_AFTER_VALUE']);
        }
        $baItems[$ob['ID']] = $ob;
    }

    return $baItems;
}

function getReviewItems($arServiceItemsID)
{ // 2 - reviews iblock id

    $arSelect = ["*", "PROPERTY_R_SERVICE", "PROPERTY_WALK", "PROPERTY_PHOTO", "PROPERTY_VIDEO", "PROPERTY_COMMENT", "PROPERTY_PROBLEM", "PROPERTY_OPERATION", "PROPERTY_SOURCE", "PROPERTY_RELATED_MATERIALS", "PROPERTY_PHONE", "PROPERTY_DATE"];
    $arFilter = ["IBLOCK_ID" => 2, "PROPERTY_R_SERVICE" => $arServiceItemsID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"];
    $res = CIBlockElement::GetList([], $arFilter, false, ['nTopCount' => '5'], $arSelect);
    while ($ob = $res->GetNext()) {

        if ($ob['PROPERTY_PHOTO_VALUE']) {
            $ob['PHOTO'] = CFile::GetFileArray($ob['PROPERTY_PHOTO_VALUE']);
            unset($ob['PROPERTY_PHOTO_VALUE']);
        }
        if ($ob['PROPERTY_VIDEO_VALUE']) {
            $ob['VIDEO'] = CFile::GetFileArray($ob['PROPERTY_VIDEO_VALUE']);
            unset($ob['PROPERTY_VIDEO_VALUE']);
        }
        $reviewItems[$ob['ID']] = $ob;
    }

    return $reviewItems;
}

if ($arResult['PROPERTIES']['PRICELIST']['VALUE']) {

    $pricelistItems = getPricelistItems($arResult['PROPERTIES']['PRICELIST']['VALUE']);

    foreach ($pricelistItems as $id => $item) {
        if (!empty($item['SERVICES'])) {

            foreach ($item['SERVICES'] as $key => $value) {
                $id_services[] = $value;
            }

            $serviceItems = getServiceItems($id_services);
        }
    }

    //------
    // create pricelist block
    if (!empty($serviceItems) && !empty($pricelistItems)) {

        $all_services = $serviceItems;
        foreach ($pricelistItems as $id => $item) {

            foreach ($item['SERVICES'] as $key => $value) {

                if (array_key_exists($value, $serviceItems)) {

                    $serviceItems[$value]['PRICELIST'][$item['ID']] = $item;
                }
            }
        }

        $serviceItems['ALL_SERVICE_LIST'] = $all_services;
        unset($all_services);

        $arResult['PRICELIST_BLOCK'] = $serviceItems;
        unset($serviceItems);
        unset($pricelistItems);
    }
    //------
    // create before-after block
    if (empty($id_services)) {

        if ($arResult['PRICELIST_BLOCK']['ALL_SERVICE_LIST']) {

            foreach ($serviceItems['PRICELIST_BLOCK']['ALL_SERVICE_LIST'] as $key => $value) {
                $id_services[] = $value;
            }

            $baItems = getBAItems($id_services);
        }
    } else {

        $baItems = getBAItems($id_services);
    }

    if ($arResult['PRICELIST_BLOCK']['ALL_SERVICE_LIST']) {
        $baItems['ALL_SERVICE_LIST'] = $arResult['PRICELIST_BLOCK']['ALL_SERVICE_LIST'];
    } else {
        $serviceItems = getServiceItems($id_services);
        $baItems['ALL_SERVICE_LIST'] = $serviceItems;
    }

    if ($baItems) {
        $arResult['BEFORE-AFTER_BLOCK'] = $baItems;
        unset($baItems);
    }


    //------
    // create review block
    if (!empty($id_services)) {

        $reviewItems = getReviewItems($id_services);
        if ($arResult['PRICELIST_BLOCK']['ALL_SERVICE_LIST']) {
            $reviewItems['ALL_SERVICE_LIST'] = $arResult['PRICELIST_BLOCK']['ALL_SERVICE_LIST'];
        } else {
            $serviceItems = getServiceItems($id_services);
            $reviewItems['ALL_SERVICE_LIST'] = $serviceItems;
        }

        $arResult['REVIEWS_BLOCK'] = $reviewItems;
        unset($reviewItems);
        unset($serviceItems);
        unset($id_services);
    }
}

foreach ($arResult['PRICELIST_BLOCK'] as $arItem) {
    $arDirection = \CIBlockSection::GetByID($arItem['IBLOCK_SECTION_ID'])->Fetch();
    if ($arDirection['DEPTH_LEVEL'] == 2) $arResult['SECTIONS'][$arDirection['ID']] = $arDirection;
}

$arResult['BEFORE_AFTER'] = [];
foreach ($arResult['PROPERTIES']['SERVICE']['VALUE'] as $service) {
    $resBA = CIBlockElement::GetList([], ['IBLOCK_ID' => $arResult['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], 'ID' => $service], false, [], ['ID', 'NAME', 'DETAIL_PAGE_URL', 'IBLOCK_SECTION_ID', 'PROPERTY_BEFORE_AFTER']);
    while ($obBA = $resBA->GetNextElement()) {
        $arFieldsBA = $obBA->GetFields();
        if ($arFieldsBA['PROPERTY_BEFORE_AFTER_VALUE']) {
            $arResult['BEFORE_AFTER'][$arFieldsBA['IBLOCK_SECTION_ID']]['NAME'] = $arFieldsBA['NAME'];
            $arResult['BEFORE_AFTER'][$arFieldsBA['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID'] = $arFieldsBA['IBLOCK_SECTION_ID'];
            $arFile = CFile::GetFileArray($arFieldsBA["PROPERTY_BEFORE_AFTER_VALUE"]);
            $arResult['BEFORE_AFTER'][$arFieldsBA['IBLOCK_SECTION_ID']]['BEFORE_AFTER_ITEMS'][$arFieldsBA['PROPERTY_BEFORE_AFTER_VALUE']] = $arFile;
        }
    }
}
// формируем ID раздела для прайса
foreach ($arResult['PROPERTIES']['DIRECTION']['VALUE'] as $directionID) {
    $resSection = \CIBlockSection::GetByID($directionID);
    if($arSection = $resSection->GetNext()) {
        if ($arSection['DEPTH_LEVEL'] == 1) $sectionIDs[] = $arSection['ID'];
    }
}
$arResult['PRICE_LIST_ARRAY'] = \IL\Price::getPriceBySectionID($sectionIDs);

