<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;
?>
<? if ($_SERVER['REAL_FILE_PATH'] == '/program/index.php') {
    $titleModal = 'Записаться по программе';
    $program = 'Y';
}
if ($_SERVER['REAL_FILE_PATH'] == '/stock/index.php') {
    $titleModal = 'Записаться по акции';
    $stock = 'Y';
} ?>

<? $this->SetViewTarget('detail_promo_class'); ?> pb-less<? $this->EndViewTarget(); ?>

<? $this->SetViewTarget('detail_promo'); ?>
    <div class="detail-promo p-0">
        <div class="stock-block" style="background-color:<?= '#' . ($arResult['PROPERTIES']['BACKGROUND']['VALUE'] ? $arResult['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') ?>">
            <div class="stock-block__item">
                <div class="stock-block__col left">
                    <div class="stock-block__content">
                        <? if (!empty($arResult['PROPERTIES']['DISCOUNT']['VALUE']) || !empty($arResult['PROPERTIES']['PRICE']['VALUE'])): ?>
                            <div class="stock-block__top-row">
                                <? if (!empty($arResult['PROPERTIES']['DISCOUNT']['VALUE'])): ?>
                                    <div class="discount"><?= $arResult['PROPERTIES']['DISCOUNT']['VALUE'] ?></div>
                                <? endif ?>
                                <? if (!empty($arResult['PROPERTIES']['PRICE']['VALUE'])): ?>
                                    <div class="price">
                                        <div class="price__item">
                                            <span class="price__text">Цена:</span>
                                            <span class="price__value">
                                            <?= $arResult['PROPERTIES']['PRICE']['VALUE'] ?>
                                        </span>
                                        </div>
                                    </div>
                                <? endif ?>
                            </div>
                        <? endif ?>
                        <div class="stock-block__main-row">
                            <div class="stock-block__title">
                                <?= $arResult['NAME'] ?>
                            </div>
                            <? if (!empty($arResult['PREVIEW_TEXT'])): ?>
                                <p class="stock-block__description">
                                    <?= $arResult['PREVIEW_TEXT'] ?>
                                </p>
                            <? endif ?>
                        </div>
                        <div class="stock-block__bottom-row">
                            <a class="btn btn_primary modal-open hidden-xs-down" href="#stock">
                                <span><?= $titleModal ?></span>
                            </a>
                            <div class="hidden-sm-up">
                                <a class="arrow-link modal-open" href="#stock">
                                    <span class="arrow-link__title"><?= $titleModal ?></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="stock-block__date">
                        *<?= (!empty($arResult['DATE_ACTIVE_FROM'])) ? 'c ' . mb_strtolower(FormatDateFromDB($arResult['DATE_ACTIVE_FROM'], "j F")) : ''; ?>
                        <?= (!empty($arResult['DATE_ACTIVE_TO'])) ? ' по ' . mb_strtolower(FormatDateFromDB($arResult['DATE_ACTIVE_TO'], "j F")) : ''; ?>
                    </div>
                </div>
                <div class="stock-block__col right">
                    <div class="stock-block__img-wrapper">
                        <?
                            $color = \IL\Utilities::hex2rgb('#' . ($arResult['PROPERTIES']['BACKGROUND']['VALUE'] ? $arResult['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5'), '0.5');
                            $stockBlockImgMaskStyle = '
                                                background-image: -webkit-gradient(linear,right top,left top,from(' . $color . '),to(#' . ($arResult['PROPERTIES']['BACKGROUND']['VALUE'] ? $arResult['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') . '));
                                                background-image: -webkit-linear-gradient(right,' . $color . ',#' . ($arResult['PROPERTIES']['BACKGROUND']['VALUE'] ? $arResult['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') . ');
                                                background-image: linear-gradient(to left,' . $color . ',#' . ($arResult['PROPERTIES']['BACKGROUND']['VALUE'] ? $arResult['PROPERTIES']['BACKGROUND']['VALUE'] : 'fffdf5') . ');
                                            ';
                        ?>
                        <div class="stock-block__img-mask" style="<?=$stockBlockImgMaskStyle ?>"></div>
                        <? if ($arResult['DETAIL_PICTURE']['SRC']): ?>
                            <div class="stock-block__img"
                                 style="background-image: url(<?= $arResult['DETAIL_PICTURE']['SRC'] ?>);"></div>
                        <? elseif($arResult['PREVIEW_PICTURE']['SRC']) : ?>
                            <div class="stock-block__img"
                                 style="background-image: url(<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>);"></div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? $this->EndViewTarget(); ?>

    <div class="row">
        <div class="col col-3 col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 sidebar-col">
            <div class="sidebar-wrapper">
                <aside class="sidebar" data-simplebar>
                    <div class="sidebar__inner">
                        <? if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y") { ?>
                            <div class="share">
                                <div class="share__title">Поделиться</div>
                                <noindex>
                                    <?
                                    $APPLICATION->IncludeComponent(
                                        "bitrix:main.share",
                                        "share",
                                        [
                                            "HANDLERS" => $arParams["SHARE_HANDLERS"],
                                            "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                                            "PAGE_TITLE" => $arResult["~NAME"],
                                            "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                                            "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                                            "HIDE" => $arParams["SHARE_HIDE"],
                                        ],
                                        $component,
                                        ["HIDE_ICONS" => "Y"]
                                    );
                                    ?>
                                </noindex>
                            </div>
                        <? } ?>
                    </div>
                </aside>
            </div>
        </div>
        <div class="col col-8 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
            <div class="content-container">

                <section class="section mb-less">
                    <div class="content-container">

                        <?= $arResult['~DETAIL_TEXT'] ?>

                        <? if (!empty($arResult['PROPERTIES']['YOUTUBE']['DESCRIPTION'])): ?>
                            <div class="detail-block" style="margin-top: 0;">
                                <div class="detail-block__main-row">
                                    <? if (!empty($arResult['PROPERTIES']['YOUTUBE']['VALUE']['TEXT'])): ?>
                                        <div class="detail-block__col">
                                            <p>
                                                <?= $arResult['PROPERTIES']['YOUTUBE']['~VALUE']['TEXT'] ?>
                                            </p>
                                        </div>
                                    <? endif ?>
                                    <div class="detail-block__col">
                                        <iframe class="modal__video"
                                                src="https://www.youtube.com/embed/<?= $arResult['PROPERTIES']['YOUTUBE']['DESCRIPTION'] ?>"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        <? endif ?>

                        <? if (!empty($arResult['PROPERTIES']['TERMS']['VALUE']['TEXT']) || !empty($arResult['PROPERTIES']['RESULT']['VALUE']['TEXT'])): ?>
                            <div class="list-group-wrapper">
                                <? if (!empty($arResult['PROPERTIES']['TERMS']['VALUE']['TEXT'])): ?>
                                    <div class="list-group">
                                        <h3><?= $arResult['PROPERTIES']['TERMS']['NAME'] ?>:</h3>
                                        <?= $arResult['PROPERTIES']['TERMS']['~VALUE']['TEXT'] ?>
                                    </div>
                                <? endif ?>
                                <? if (!empty($arResult['PROPERTIES']['RESULT']['VALUE']['TEXT'])): ?>
                                    <div class="list-group">
                                        <h3><?= $arResult['PROPERTIES']['RESULT']['NAME'] ?>:</h3>
                                        <?= $arResult['PROPERTIES']['RESULT']['~VALUE']['TEXT'] ?>
                                    </div>
                                <? endif ?>
                            </div>
                            <hr>
                        <? endif ?>

                    </div>
                </section>
                <? //Подключение слайдера комплекса услуг
                if ($program == 'Y') {
                    foreach ($arResult['PROPERTIES']['SERVICE']['VALUE'] as $arService) {
                        $arFilterService[] = $arService;
                    }
                    $filter = ['ID' => $arFilterService];
                    $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/services_slider.php', ['TEMPLATE' => 'services_slider', 'FILTER' => $filter], ['SHOW_BORDER' => false]);
                    unset($arFilterService);
                } ?>

                <? if (!empty($arResult['PROPERTIES']['DIRECTION']['VALUE']) || !empty($arResult['PROPERTIES']['SERVICE']['VALUE'])): ?>
                    <?
                    //Подключение до / после
                    foreach ($arResult['PROPERTIES']['SERVICE']['VALUE'] as $arService) {
                        $arFilterService[] = $arService;
                    }
                    $filter = ['ID' => $arFilterService, '!PROPERTY_BEFORE_AFTER' => false];
                    $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/before_after.php", [
                        "TEMPLATE" => "before_after_slider",
                        "FILTER" => $filter,
                        "DOP_FILTER" => ($arParams['BEFORE_AFTER_SECTION'] ? $arParams['BEFORE_AFTER_SECTION'] : false),
                    ], ["SHOW_BORDER" => false]);

                    //Подключение отзывов
                    foreach ($arResult['PROPERTIES']['DIRECTION']['VALUE'] as $arDirection) {
                        $arFilterService[] = $arDirection;
                    }
                    $filter = ['PROPERTY_SERVICE' => $arFilterService];
                    $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/reviews.php", [
                        "TEMPLATE" => "reviews_slider",
                        "FILTER" => $filter,
                        "DOP_FILTER" => ($arParams['REVIEWS_SECTION'] ? $arParams['REVIEWS_SECTION'] : false),
                    ], ["SHOW_BORDER" => false]);
                    unset($elementInfo);

                    //Подключение прайса
                    ?>
                    <? if ($stock == 'Y'): ?>
                        <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/price_accordeon.php', [
                            'TITLE' => 'Прейскурант услуг участвующих в акции',
                            'PRICE_LIST_ARRAY' => $arResult['PRICE_LIST_ARRAY'],
                            "DOP_FILTER" => ($arParams['PRICE_SECTION'] ? $arParams['PRICE_SECTION'] : false),
                        ],
                            ['SHOW_BORDER' => false]); ?>
                    <? endif ?>
                <? endif ?>
                <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/sign_up.php',
                    ['BLOCK_TITLE' => 'Записаться по акции', 'BTN_TITLE' => 'Записаться по акции'], ['SHOW_BORDER' => false]) ?>


                <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/contraindications_all.php", [], ["SHOW_BORDER" => false]); ?>

            </div>
        </div>
    </div>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/form_modal.php', ['TITLE' => $titleModal, 'ELEMENT' => $arResult['ID'], 'ACTION' => SITE_AJAX_PATH . '/form_stock.php', 'FORM_ID' => 'stock'], ['SHOW_BORDER' => false]) ?>