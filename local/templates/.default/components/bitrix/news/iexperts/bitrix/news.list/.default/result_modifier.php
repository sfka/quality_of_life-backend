<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (!CModule::IncludeModule("iblock"))
    return false;
$arResult['SECTIONS'] = [];
$res = CIBlockElement::GetList([], ['IBLOCK_ID' => $arParams['IBLOCK_ID']], false, [], ['ID', 'NAME', 'DETAIL_PAGE_URL']);
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $arResult['ALL_ITEMS'][$arFields['ID']] = $arFields;
    $arResult['ALL_ITEMS'][$arFields['ID']]['PROPERTIES'] = $arProps;
}
foreach ($arResult['ALL_ITEMS'] as $arItem) {
    foreach ($arItem['PROPERTIES']['SERVICE']['VALUE'] as $subSection) {
        $arSubSection[] = $subSection;
    }
}
foreach ($arSubSection as $subSection) {
    $resSubsection = \CIBlockSection::GetByID($subSection);
    if ($arSubSection = $resSubsection->GetNext()) {
        $resSection = \CIBlockSection::GetByID($arSubSection['IBLOCK_SECTION_ID']);
        if ($arSection = $resSection->GetNext()) {
            $arResult['SECTIONS'][$arSubSection['IBLOCK_SECTION_ID']]['ID'] = $arSection['ID'];
            $arResult['SECTIONS'][$arSubSection['IBLOCK_SECTION_ID']]['NAME'] = $arSection['NAME'];
        }
        $arResult['SECTIONS'][$arSubSection['IBLOCK_SECTION_ID']]['SUBSECTIONS'][$arSubSection['ID']] = $arSubSection;
    }
}
foreach ($arResult['ITEMS'] as $key => $arItem) {
    $res = CIBlockElement::GetList([], ['IBLOCK_ID' => SITE_REVIEWS_IBLOCK_ID, 'PROPERTY_SPECIALIST' => $arItem['ID']], false, [], ['ID', 'NAME', 'PROPERTY_SPECIALIST_VALUE']);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arResult['ITEMS'][$key]['REVIEWS'][] = $arFields;
    }
}
