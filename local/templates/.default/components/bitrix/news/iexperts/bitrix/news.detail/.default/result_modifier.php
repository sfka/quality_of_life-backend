<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$res = CIBlockElement::GetList([], ['IBLOCK_ID' => SITE_REVIEWS_IBLOCK_ID, 'PROPERTY_SPECIALIST' => $arResult['ID']], false, [], ['ID', 'NAME']);
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arResult['REVIEWS'][] = $arFields;
}
