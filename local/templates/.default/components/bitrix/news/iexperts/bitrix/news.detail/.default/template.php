<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;

?>

<? $this->SetViewTarget('detail_promo_class'); ?> pb-less<? $this->EndViewTarget(); ?>

<? $this->SetViewTarget('detail_promo'); ?>
<div class="detail-promo item">
    <div class="common-container">
        <div class="row">
            <div class="col col-6 col-sm-12 col-xs-12">
                <div class="detail-promo__col">
                    <? if (!empty($arResult['PROPERTIES']['DIRECTION']['VALUE'])): ?>
                        <div class="detail-promo__direction">
                            <?$count = 0; ?>
                            <? foreach ($arResult['PROPERTIES']['DIRECTION']['VALUE'] as $direction): ?>
                                <?$count++; ?>
                                <span>#<?= mb_strtolower(Catalog::getSectionNameByID(SITE_SERVICE_IBLOCK_ID, $direction)) ?><?if($count != count($arResult['PROPERTIES']['DIRECTION']['VALUE'])) echo(', ') ?></span>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>
                    <div class="detail-promo__top-row">
                        <? $arServices = Catalog::getSectionNameByID($arResult['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arResult['PROPERTIES']['SERVICE']['VALUE'][0]); ?>
                        <div class="type"><?= $arServices ?></div>
                        <? if (count($arResult['PROPERTIES']['SERVICE']['VALUE']) > 1): ?>
                            <button class="show-more">
                                <div class="show-more__btn">
                                    <svg class="three-dot" width="2" height="10">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#three-dot"></use>
                                    </svg>
                                    <span>Еще</span>
                                </div>
                                <div class="show-more__list" data-simplebar="data-simplebar">
                                    <? foreach ($arResult['PROPERTIES']['SERVICE']['VALUE'] as $keyService => $arService): ?>
                                        <? if ($keyService == 0) continue ?>
                                        <? $arServiceName = Catalog::getSectionNameByID($arResult['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arService); ?>
                                        <div class="show-more__list-item">
                                            <span><?= $arServiceName ?></span>
                                        </div>
                                    <? endforeach ?>
                                </div>
                            </button>
                        <? endif ?>
                    </div>
                    <div class="detail-promo__main-row">
                        <div class="detail-promo__title"><?= $arResult['NAME'] ?></div>
                        <? if ($arResult['PROPERTIES']['PROFESSION']['VALUE']): ?>
                            <div class="detail-promo__text"><?= $arResult['PROPERTIES']['PROFESSION']['VALUE'] ?></div>
                        <? endif ?>
                        <ul class="table-list">
                            <? if ($arResult['PROPERTIES']['DEGREE']['VALUE']): ?>
                                <li class="table-list__item"><span
                                            class="table-list__property">Ученая степень:</span><span
                                            class="table-list__value"><?= $arResult['PROPERTIES']['DEGREE']['VALUE'] ?></span>
                                </li>
                            <? endif; ?>
                            <? if ($arResult['PROPERTIES']['STANDING']['VALUE']): ?>
                                <li class="table-list__item"><span class="table-list__property">Стаж:</span><span
                                            class="table-list__value"><?= $arResult['PROPERTIES']['STANDING']['VALUE'] ?></span>
                                </li>
                            <? endif ?>
                            <? if (count($arResult['REVIEWS']) > 0): ?>
                                <li class="table-list__item"><span class="table-list__property">Отзывы:</span><span
                                            class="table-list__value"><?= count($arResult['REVIEWS']) ?></span>
                                </li>
                            <? endif; ?>
                        </ul>
                    </div>
                    <div class="detail-promo__bottom-row"><a class="btn btn_primary"
                                                             href="/appointments/?specialist=<?= $arResult['EXTERNAL_ID'] ?>"><span>Запись на прием</span></a>
                        <?if($arResult['PROPERTIES']['YOUTUBE']['VALUE']): ?>
                        <a class="arrow-link specialist-video-open" href="javascript:void(0)"
                           data-modal-class="specialist-video-modal" data-type="center">
                            <span class="arrow-link__title">Видео о специалисте</span>
                        </a>
                        <?endif; ?>
                        <div class="specialist-video-modal modal__inner modal_center mfp-hide">
                            <button class="modal__close-btn js-modal-close">
                                <svg class="icon__close" width="20" height="20">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                </svg>
                            </button>
                            <div class="modal__content">
                                <iframe class="modal__video"
                                        src="https://www.youtube.com/embed/<?= $arResult['PROPERTIES']['YOUTUBE']['VALUE'] ?>?enablejsapi=1&amp;version=3&amp;playerapiid=ytplayer"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen="allowfullscreen"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-6 col-sm-12 col-xs-12">
                <div class="detail-promo__img-wrapper detail-promo__img-wrapper_unique">
                    <img class="detail-promo__img lazy" src="<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>">
                </div>
                <div class="detail-promo__bg-img"
                     style="background-image: url(<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>);"></div>
            </div>
        </div>
    </div>
</div>
<? $this->EndViewTarget(); ?>

<div class="row">
    <div class="col col-3 col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 sidebar-col">
        <div class="sidebar-wrapper">
            <aside class="sidebar" data-simplebar>
                <div class="sidebar__inner">
                    <? if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y") { ?>
                        <div class="share">
                            <div class="share__title">Поделиться</div>
                            <noindex>
                                <?
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.share",
                                    "share",
                                    [
                                        "HANDLERS" => $arParams["SHARE_HANDLERS"],
                                        "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                                        "PAGE_TITLE" => $arResult["~NAME"],
                                        "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                                        "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                                        "HIDE" => $arParams["SHARE_HIDE"],
                                    ],
                                    $component,
                                    ["HIDE_ICONS" => "Y"]
                                );
                                ?>
                            </noindex>
                        </div>
                    <? } ?>
                </div>
            </aside>
        </div>
    </div>
    <div class="col col-8 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div class="content-container">

            <section class="section mb-less">
                <div class="content-container">
                    <? if (!empty($arResult['PROPERTIES']['SPECIALIZATION']['~VALUE'])): ?>
                        <h3><?= $arResult['PROPERTIES']['SPECIALIZATION']['NAME'] ?></h3>
                        <?= $arResult['PROPERTIES']['SPECIALIZATION']['~VALUE']['TEXT'] ?>
                    <? endif; ?>
                    <? if (!empty($arResult['PROPERTIES']['PROGRESS']['~VALUE'])): ?>
                        <h3><?= $arResult['PROPERTIES']['PROGRESS']['NAME'] ?></h3>
                        <?= $arResult['PROPERTIES']['PROGRESS']['~VALUE']['TEXT'] ?>
                    <? endif; ?>
                    <? if (!empty($arResult['PROPERTIES']['EXPERIENCE']['~VALUE'])): ?>
                        <h3><?= $arResult['PROPERTIES']['EXPERIENCE']['NAME'] ?></h3>
                        <? foreach ($arResult['PROPERTIES']['EXPERIENCE']['~VALUE'] as $exp): ?>
                            <?= $exp['TEXT'] ?>
                        <? endforeach; ?>
                    <? endif; ?>
                    <? if (!empty($arResult['PROPERTIES']['EDUCATION']['~VALUE'])): ?>
                        <h3><?= $arResult['PROPERTIES']['EDUCATION']['NAME'] ?></h3>
                        <?= $arResult['PROPERTIES']['EDUCATION']['~VALUE']['TEXT'] ?>
                    <? endif; ?>
                    <? if (!empty($arResult['PROPERTIES']['COURSES']['~VALUE'])): ?>
                        <h3><?= $arResult['PROPERTIES']['COURSES']['NAME'] ?></h3>
                        <?= $arResult['PROPERTIES']['COURSES']['~VALUE']['TEXT'] ?>
                    <? endif; ?>
                </div>
            </section>
            <div class="important-info">
                <div class="important-info__inner">
                    <div class="important-info__title">Прейскурант</div>
                    <p class="important-info__description"><? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/specialist_detail_price.php') ?></p>
                </div>
            </div>
            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/reviews.php", [
                "TEMPLATE" => "reviews_slider",
                "FILTER" => ['PROPERTY_SPECIALIST' => $arResult['ID']],
                "DOP_FILTER" => ($arParams['REVIEWS_SECTION'] ? $arParams['REVIEWS_SECTION'] : false),
            ], ["SHOW_BORDER" => false]); ?>
            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/review_send.php') ?>
        </div>
    </div>
</div>