<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;
use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest();

?>
<? if (!$ajax): ?>
    <? $this->SetViewTarget('left_tags'); ?>
    <div class="sidebar__tags-wrapper">
        <div class="sidebar__col">
            <div class="sidebar__title">Направления деятельности</div>
            <div class="sidebar__tags">
                <? foreach ($arResult['SECTIONS'] as $arSection): ?>
                    <? if ($arSection['NAME']): ?>
                        <span class="sidebar__tags-item" data-direction="<?= $arSection['ID'] ?>">
                                        <span>#<?= mb_strtolower($arSection['NAME']) ?></span>
                                    </span>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
        </div>
    </div>
    <? $this->EndViewTarget(); ?>
<? endif ?>
<div class="col col-8 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
    <div class="hidden-lg-up">
        <div class="mobile-tags-select-wrapper">
            <div class="nice-select-wrapper">
                <div class="nice-select-wrapper__placeholder">Выбрать по рубрике</div>
                <select class="nice-select ajax__filter" required>
                    <? foreach ($arResult['SECTIONS'] as $arSection): ?>
                        <? if ($arSection['NAME']): ?>
                            <option data-direction="<?= $arSection['ID'] ?>"><?= $arSection['NAME'] ?></option>
                        <? endif; ?>
                    <? endforeach; ?>
                </select>
            </div>
        </div>
    </div>
    <div class="dynamic-content-wrapper">
        <div class="dynamic-content">
            <? if ($ajax): ?>
                <div class="dynamic-content__heading">
                    <?
                    $resSection = \CIBlockSection::GetByID($request->get('DIRECTION'));
                    if ($arSection = $resSection->GetNext()) {
                        $arResult['SECTION'] = $arSection;
                    }
                    ?>
                    <? if ($arResult['SECTION']): ?>
                        <div class="dynamic-content__heading-row">
                            <div class="dynamic-content__title c-gray-umber">
                                #<?= strtolower($arResult['SECTION']['NAME']); ?></div>
                        </div>
                    <? endif; ?>
                    <div class="dynamic-content__heading-row">
                        <div class="category-items">
                            <? foreach ($arResult['SECTIONS'] as $arSection) {
                                if ($arSection['ID'] == $request->get('DIRECTION') && $ajax) {
                                    foreach ($arSection['SUBSECTIONS'] as $arSubSection):?>
                                        <div class="category-items__item" data-direction="<?= $arSection['ID'] ?>"
                                             data-service="<?= $arSubSection['ID'] ?>">
                                            <span><?= $arSubSection['NAME'] ?></span>
                                        </div>
                                    <?endforeach;
                                }
                            } ?>
                        </div>
                    </div>
                </div>
            <? endif; ?>
            <div class="dynamic-content__main">
                <div class="grid grid_two-col">
                    <? foreach ($arResult['ITEMS'] as $arItem): ?>
                        <div class="specialist-item item">
                            <a class="specialist-item__img-wrapper" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                <?  if ($arItem['PREVIEW_PICTURE']['SRC']) {
                                    /*$thumb = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], [
                                        'width' => 800,
                                        'height' => 00,
                                    ], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []);*/
                                    $thumb['src'] = $arItem['PREVIEW_PICTURE']['SRC'];
                                } else {
                                    $thumb['src'] = SITE_STYLE_PATH . '/img/content/specialists/no-photo-expert.svg';
                                }
                                 ?>
                                <img class="specialist-item__img lazy"
                                     data-src="<?= $thumb['src'] ?>"
                                     alt="<?= $arItem['NAME'] ?>"/>
                            </a>
                            <div class="specialist-item__content">
                                <div class="specialist-item__row top">
                                    <div class="specialist-item__row-inner">
                                        <div class="specialist-item__col unique">
                                            <? $arServices = Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arItem['PROPERTIES']['SERVICE']['VALUE'][0]); ?>
                                            <div class="specialist-item__category">
                                                <?= $arServices ?>
                                            </div>
                                            <? if (count($arItem['PROPERTIES']['SERVICE']['VALUE']) > 1): ?>
                                                <button class="show-more">
                                                    <div class="show-more__btn">
                                                        <svg class="three-dot" width="2" height="10">
                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#three-dot"></use>
                                                        </svg>
                                                        <span>Еще</span>
                                                    </div>
                                                    <div class="show-more__list" data-simplebar="data-simplebar">
                                                        <? foreach ($arItem['PROPERTIES']['SERVICE']['VALUE'] as $keyService => $arService): ?>
                                                            <? if ($keyService == 0) continue ?>
                                                            <? $arServiceName = Catalog::getSectionNameByID($arItem['PROPERTIES']['SERVICE']['LINK_IBLOCK_ID'], $arService); ?>
                                                            <div class="show-more__list-item">
                                                                <span><?= $arServiceName ?></span>
                                                            </div>
                                                        <? endforeach ?>
                                                    </div>
                                                </button>
                                            <? endif ?>
                                        </div>
                                        <? if (count($arItem['REVIEWS']) > 0): ?>
                                            <div class="specialist-item__col">
                                                <a class="specialist-item__link"
                                                   href="<?= $arItem['DETAIL_PAGE_URL'] ?>#reviews">
                                                    Отзывы: <?= count($arItem['REVIEWS']) ?>
                                                </a>
                                            </div>
                                        <? endif ?>
                                    </div>
                                </div>
                                <div class="specialist-item__row main">
                                    <div class="specialist-item__row-inner">
                                        <div class="specialist-item__col">
                                            <a class="specialist-item__name" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                                <?= $arItem['NAME'] ?>
                                            </a>
                                            <div class="specialist-item__position">
                                                <?= !empty($arItem['PROPERTIES']['PROFESSION']['VALUE']) ? $arItem['PROPERTIES']['PROFESSION']['VALUE'] : '&nbsp;' ?>
                                            </div>
                                        </div>
                                        <div class="specialist-item__col">
                                            <? if (!empty($arItem['PROPERTIES']['YOUTUBE']['VALUE'])): ?>
                                                <a class="specialist-item__play specialist-video-open"
                                                   href="javascript:void(0)" data-modal-class="specialist-video-modal"
                                                   data-type="center">
                                                    <div class="specialist-item__play-icon-wrapper">
                                                        <svg class="play" width="10" height="12">
                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#play"></use>
                                                        </svg>
                                                    </div>
                                                    <span class="specialist-item__play-label">Видео о специалисте</span>
                                                </a>
                                                <div class="specialist-video-modal modal__inner modal_center mfp-hide">
                                                    <button class="modal__close-btn js-modal-close">
                                                        <svg class="icon__close" width="20" height="20">
                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                                                        </svg>
                                                    </button>
                                                    <div class="modal__content">
                                                        <iframe class="modal__video"
                                                                src="https://www.youtube.com/embed/<?= $arItem['PROPERTIES']['YOUTUBE']['VALUE'] ?>"
                                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                                allowfullscreen="allowfullscreen"></iframe>
                                                    </div>
                                                </div>
                                            <? endif ?>
                                        </div>
                                    </div>
                                    <? if (!empty($arItem['PROPERTIES']['DEGREE']['VALUE']) || !empty($arItem['PROPERTIES']['STANDING']['VALUE'])): ?>
                                        <ul class="specialist-item__list">
                                            <? if (!empty($arItem['PROPERTIES']['DEGREE']['VALUE'])): ?>
                                                <li class="specialist-item__list-item">
                                                        <span class="specialist-item__list-property">
                                                            <?= $arItem['PROPERTIES']['DEGREE']['NAME'] ?>:
                                                        </span>
                                                    <span class="specialist-item__list-value">
                                                            <?= $arItem['PROPERTIES']['DEGREE']['VALUE'] ?>
                                                        </span>
                                                </li>
                                            <? endif ?>
                                            <? if (!empty($arItem['PROPERTIES']['STANDING']['VALUE'])): ?>
                                                <li class="specialist-item__list-item">
                                                        <span class="specialist-item__list-property">
                                                            <?= $arItem['PROPERTIES']['STANDING']['NAME'] ?>:
                                                        </span>
                                                    <span class="specialist-item__list-value">
                                                            <?= $arItem['PROPERTIES']['STANDING']['VALUE'] ?>
                                                        </span>
                                                </li>
                                            <? endif ?>
                                        </ul>
                                    <? endif ?>
                                </div>
                                <div class="specialist-item__row bottom">
                                    <a class="arrow-link"
                                       href="/appointments/?specialist=<?= $arItem['EXTERNAL_ID'] ?>">
                                        <span class="arrow-link__title">Запись на прием</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>