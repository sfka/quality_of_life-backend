<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? $this->SetViewTarget('detail_promo_class'); ?> pb-less<? $this->EndViewTarget(); ?>

<? $this->SetViewTarget('detail_promo'); ?>
<div class="detail-promo">
    <div class="common-container">
        <div class="row">
            <div class="col col-6 col-sm-12 col-xs-12">
                <div class="detail-promo__col">
                    <? if (!empty($arResult["DISPLAY_ACTIVE_FROM"])): ?>
                        <div class="detail-promo__top-row">
                            <div class="detail-promo__date">
                                <?= $arResult["DISPLAY_ACTIVE_FROM"] ?>
                            </div>
                        </div>
                    <? endif ?>
                    <? if (!empty($arResult["NAME"])): ?>
                        <div class="detail-promo__main-row sm-down-mb-0">
                            <div class="detail-promo__title">
                                <?= $arResult["NAME"] ?>
                            </div>
                        </div>
                    <? endif ?>
                </div>
            </div>
            <div class="col col-6 col-sm-12 col-xs-12">
                <div class="detail-promo__img-wrapper">
                    <img class="detail-promo__img lazy" data-src="<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>">
                </div>
                <div class="detail-promo__bg-img" style="background-image: url(<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>);"></div>
            </div>
        </div>
    </div>
</div>
<? $this->EndViewTarget(); ?>

<div class="row">
    <div class="col col-3 col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 sidebar-col">
        <div class="sidebar-wrapper">
            <aside class="sidebar" data-simplebar>
                <div class="sidebar__inner">
                    <? if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y") { ?>
                        <div class="share">
                            <div class="share__title">Поделиться</div>
                            <noindex>
                                <?
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.share",
                                    "share",
                                    array(
                                        "HANDLERS" => $arParams["SHARE_HANDLERS"],
                                        "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                                        "PAGE_TITLE" => $arResult["~NAME"],
                                        "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                                        "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                                        "HIDE" => $arParams["SHARE_HIDE"],
                                    ),
                                    $component,
                                    array("HIDE_ICONS" => "Y")
                                );
                                ?>
                            </noindex>
                        </div>
                    <? } ?>
                </div>
            </aside>
        </div>
    </div>
    <div class="col col-8 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div class="content-container">
            <section class="section mb-less">
                <div class="content-container">
                    <? if (!empty($arResult["DETAIL_TEXT"])): ?>
                        <?= $arResult["DETAIL_TEXT"] ?>
                    <? endif ?>
                    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/warning.php", ["SHOW_BORDER" => false]); ?>
                </div>
            </section>
            <hr>
            <? if (!empty($arResult["PROPERTIES"]["PHOTO"]["VALUE"])): ?>
                <section class="section s-photos-slider mb-less pt-0 scrl fadeInUp">
                    <div class="row section__top-row">
                        <div class="col col-12">
                            <div class="section__top-row-inner">
                                <div class="section-title">Фотографии</div>
                                <div class="swiper-controls not-in-modal">
                                    <div class="swiper-pagination"></div>
                                    <div class="swiper-buttons">
                                        <div class="swiper-button-prev">
                                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                            </svg>
                                        </div>
                                        <div class="swiper-button-next">
                                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-12">
                            <div class="default-slider">
                                <div class="swiper-container not-in-modal">
                                    <div class="swiper-wrapper">
                                        <? foreach ($arResult["PROPERTIES"]["PHOTO"]["VALUE"] as $keyPhoto => $arPhoto): ?>
                                            <div class="swiper-slide">
                                                <img class="slider-photo-item swiper-lazy" data-src="<?= CFile::GetPath($arPhoto) ?>"
                                                     alt="<?= !empty($arResult["PROPERTIES"]["PHOTO"]["DESCRIPTION"][$keyPhoto]) ? $arResult["PROPERTIES"]["PHOTO"]["DESCRIPTION"][$keyPhoto] : $arResult["NAME"] ?>"/>
                                            </div>
                                        <? endforeach ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-12 hidden-md-up">
                            <div class="mobile-swiper-controls">
                                <div class="swiper-controls not-in-modal">
                                    <div class="swiper-pagination"></div>
                                    <div class="swiper-buttons">
                                        <div class="swiper-button-prev">
                                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                            </svg>
                                        </div>
                                        <div class="swiper-button-next">
                                            <svg class="icon__arrowRightDefault" width="32" height="24">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <? endif ?>
        </div>
    </div>
</div>