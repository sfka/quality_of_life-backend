<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var array $APPLICATION */

use \IL\Catalog;

$arResult['LEFT_MENU'] = Catalog::getLeftMenu($arParams['IBLOCK_ID']);
$arResult['ITEMS'] = Catalog::getGroupItems($arResult['ITEMS']);