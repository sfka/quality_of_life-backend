<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? $this->SetViewTarget('left_tags'); ?>
<div class="sidebar__tags-wrapper">
    <? if (!empty($arResult['LEFT_MENU']['TAGS'])): ?>
        <div class="sidebar__col">
            <div class="sidebar__title">Рубрики</div>
            <div class="sidebar__tags">
                <? foreach ($arResult['LEFT_MENU']['TAGS'] as $arItems): ?>
                    <span class="sidebar__tags-item" data-section="<?= $arItems['ID'] ?>">
                        <span><?= $arItems['NAME'] ?></span>
                    </span>
                <? endforeach ?>
            </div>
        </div>
    <? endif ?>
</div>
<? $this->EndViewTarget(); ?>

<div class="col col-9 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
    <div class="hidden-lg-up">
        <div class="mobile-tags-select-wrapper">
            <div class="nice-select-wrapper">
                <div class="nice-select-wrapper__placeholder">Выбрать по рубрике</div>
                <select class="nice-select ajax__filter" required>
                    <? foreach ($arResult['LEFT_MENU']['TAGS'] as $arItems): ?>
                        <option data-section="<?= $arItems['ID'] ?>"><?= $arItems['NAME'] ?></option>
                    <? endforeach ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-7 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="dynamic-content-wrapper width-fix">
                <? foreach ($arResult['ITEMS'] as $keyItems => $arItems): ?>
                    <div class="dynamic-content" data-section="<?= $arItems['ITEMS'][0]['IBLOCK_SECTION_ID'] ?>">
                        <div class="dynamic-content__heading">
                            <div class="dynamic-content__heading-row">
                                <div class="dynamic-content__title c-gray-umber">
                                    #<?= mb_strtolower($keyItems) ?>
                                </div>
                            </div>
                        </div>
                        <div class="dynamic-content__main">
                            <div class="unique-grid ajax_list">
                                <? foreach ($arItems['ITEMS'] as $key => $arItem): ?>
                                    <a class="ajax_item news-card news-card_<?= $key === 0 ? 'long' : 'short' ?>" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                        <? if (!empty($arItem['PREVIEW_PICTURE'])): ?>
                                            <? $thumb = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], [
                                                'width' => $key === 0 ? 519 : 325,
                                                'height' => 190,
                                            ], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []); ?>
                                            <div class="news-card__img-wrapper">
                                                <img class="news-card__img" src="<?= $thumb['src'] ?>" alt="<?= $arItem['NAME'] ?>"/>
                                            </div>
                                        <? endif ?>
                                        <div class="news-card__content">
                                            <div class="news-card__col">
                                                <span class="news-card__date">
                                                    <?= FormatDateFromDB($arItem['DATE_ACTIVE_FROM'], $arParams['ACTIVE_DATE_FORMAT']) ?>
                                                </span>
                                            </div>
                                            <div class="news-card__col">
                                                <h3 class="news-card__title">
                                                    <?= $arItem['NAME'] ?>
                                                </h3>
                                                <svg class="arrowRightDefault" width="24" height="18">
                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                                </svg>
                                            </div>
                                        </div>
                                    </a>
                                <? endforeach ?>
                            </div>
                        </div>
                    </div>
                <? endforeach ?>
            </div>
        </div>
        <div class="col col-4 col-xl-4 col-lg-12 col-md-12 hidden-sm-down offset-1 offset-xl-0 offset-lg-0 offset-md-0 offset-sm-0 offset-xs-0">
            <div class="modal-card-wrapper">
                <div class="modal-card">
                    <div class="modal-card__icon-wrapper">
                        <div class="modal-card__icon-bg bg-linen"></div>
                        <svg class="icon__events-list" width="90px" height="90px">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#events-list"></use>
                        </svg>
                    </div>
                    <div class="modal-card__title">Будь всегда в курсе событий</div>
                    <p class="modal-card__description">
                        Только самые полезные и важные статьи о здоровье и доказательной медицине
                    </p>
                    <a class="btn btn_primary modal-open" href="#mailing">
                        <span>Подписаться</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>