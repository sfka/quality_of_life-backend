<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (count($arResult["ITEMS"]) > 0): ?>
    <div class="row section__top-row">
        <div class="col col-12">
            <div class="section__top-row-inner scrl fadeInUp">
                <div class="section-title">Читайте также</div>
            </div>
        </div>
    </div>
    <div class="row scrl fadeInUp">
        <div class="col col-12">
            <div class="grid grid_three-col">
                <? foreach ($arResult["ITEMS"] as $arItem): ?>
                    <a class="news-card news-card_short" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <? if (!empty($arItem['PREVIEW_PICTURE'])): ?>
                            <? $thumb = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array(
                                'width' => $key === 248,
                                'height' => 145
                            ), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, array()); ?>
                            <div class="news-card__img-wrapper">
                                <img class="news-card__img" src="<?= $thumb['src'] ?>" alt="<?= $arItem['NAME'] ?>"/>
                            </div>
                        <? endif ?>
                        <div class="news-card__content">
                            <div class="news-card__col">
                                <span class="news-card__date">
                                    <?= FormatDateFromDB($arItem['DATE_ACTIVE_FROM'], $arParams['ACTIVE_DATE_FORMAT']) ?>
                                </span>
                            </div>
                            <div class="news-card__col">
                                <h3 class="news-card__title">
                                    <?= $arItem["NAME"] ?>
                                </h3>
                                <svg class="arrowRightDefault" width="24" height="18">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                </svg>
                            </div>
                        </div>
                    </a>
                <? endforeach ?>
            </div>
        </div>
    </div>
<? endif ?>