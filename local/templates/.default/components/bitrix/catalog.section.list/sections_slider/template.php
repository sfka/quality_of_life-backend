<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<section class="section s-activities scrl fadeInUp">
    <div class="row section__top-row">
        <div class="col col-12">
            <div class="section__top-row-inner">
                <div class="section-title"><?=$arParams['WRAP_TITLE'] ?></div>
                <div class="activities-slider-controls scrl fadeInUp">
                    <svg class="activities-slider-controls__btn prev" width="32" height="24">
                        <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                    </svg>
                    <svg class="activities-slider-controls__btn next" width="32" height="24">
                        <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                    </svg>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-12">
            <div class="activities-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?foreach ($arResult['SECTIONS'] as $arSection): ?>
                        <div class="swiper-slide">
                            <a class="activity-card" href="<?=$arSection['SECTION_PAGE_URL'] ?>">
                                <div class="activity-card__img-wrapper">
                                    <img class="activity-card__img lazy" data-src="<?=$arSection['PICTURE']['SRC'] ?>" alt="<?=$arSection['NAME'] ?>" />
                                </div>
                                <div class="activity-card__row">
                                    <div class="activity-card__title"><?=$arSection['NAME'] ?></div>
                                    <svg class="arrowRightDefault" width="32" height="24">
                                        <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                    </svg>

                                </div>
                            </a>
                        </div>
                        <?endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>