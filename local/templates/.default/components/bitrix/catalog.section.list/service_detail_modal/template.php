<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \IL\Catalog;
//if ($arResult['SECTION']['DEPTH_LEVEL'] > 1) LocalRedirect($arResult['SECTION']['PATH'][0]['SECTION_PAGE_URL']);
?>

<? if (0 < $arResult["SECTIONS_COUNT"]) : ?>
    <? foreach ($arResult['SECTIONS'] as $arSection): ?>
        <? $arSection['ITEMS'] = Catalog::getElementList($arParams['IBLOCK_ID'], [
            'SECTION_ID' => $arSection["ID"],
            "INCLUDE_SUBSECTIONS" => "Y"
        ]); ?>
        <? if (!empty($arSection['ITEMS'])): ?>
            <div class="sidebar__col">
                <div class="sidebar__title">
                    <?= $arSection['NAME'] ?>
                </div>
                <div class="sidebar__nav sidebar__nav_unique">
                    <? foreach ($arSection['ITEMS'] as $arItem): ?>
                        <a class="sidebar__nav-link" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                            <?= $arItem["NAME"] ?>
                        </a>
                    <? endforeach ?>
                </div>
            </div>
        <? endif ?>
    <? endforeach ?>
<? endif ?>