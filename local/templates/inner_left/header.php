<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/head.php", [], ["SHOW_BORDER" => false]); ?>
    <? $APPLICATION->ShowHead(); ?>
</head>
<body>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/header.php", [], ["SHOW_BORDER" => false]); ?>

    <div class="main-wrapper">
        <div class="main-wrapper__inner">
            <div class="inner-page">
                <div class="inner-page__top">
                    <div class="inner-page__top-inner">
                        <div class="common-container">
                            <div class="row">
                                <div class="col col-3 col-xl-3 col-lg-3">
                                    <div class="return-link-wrapper">
                                        <a class="return-link" href="javascript:void(0);" onclick="checkReffer();">
                                            <svg class="arrowRightDefault" width="20" height="15">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                            </svg>
                                            <span class="return-link__title c-white-aluminum">Вернуться назад</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="page-heading">
                        <div class="common-container">
                            <div class="row">
                                <div class="col col-12">
                                    <div class="page-heading__title">
                                        <?= $APPLICATION->ShowTitle(false) ?>
                                    </div>
                                    <?
                                    $arSectReturn = \IL\Catalog::getParentSectParams($APPLICATION->GetCurDir());
                                    $isLeftMenu = file_exists($_SERVER["DOCUMENT_ROOT"] . $arSectReturn['URL'] . '.left.menu.php');
                                    ?>
                                    <? if ($isLeftMenu): ?>
                                        <? $arSectReturn = \IL\Catalog::getParentSectParams($APPLICATION->GetCurDir()); ?>
                                        <div class="hidden-lg-up">
                                            <button class="show-more">
                                                <div class="show-more__btn">
                                                    <svg class="three-dot" width="2" height="10">
                                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#three-dot"></use>
                                                    </svg>
                                                    <span>Другие страницы <?= mb_strtolower($arSectReturn['NAME']) ?></span>
                                                </div>
                                                <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/left_menu.php", ["TEMPLATE" => "left_mobile"], ["SHOW_BORDER" => false]); ?>
                                            </button>
                                        </div>
                                    <? endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="inner-page__main">
                    <div class="inner-page__container common-container">
                        <div class="inner-page__content">
                            <div class="row">
                                <div class="col col-3 col-xl-3 col-lg-3 hidden-md-down sidebar-col">
                                    <div class="sidebar-wrapper">
                                        <aside class="sidebar" data-simplebar>
                                            <div class="sidebar__inner">
                                                <?
                                                $arSectReturn = \IL\Catalog::getParentSectParams($APPLICATION->GetCurDir());
                                                $isLeftMenu = file_exists($_SERVER["DOCUMENT_ROOT"] . $arSectReturn['URL'] . '.left.menu.php');
                                                ?>
                                                <? if ($isLeftMenu): ?>
                                                    <div class="sidebar__col">
                                                        <div class="sidebar__title">
                                                            <?= $arSectReturn['NAME'] ?>
                                                        </div>
                                                        <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/left_menu.php", ["TEMPLATE" => "left"], ["SHOW_BORDER" => false]); ?>
                                                    </div>
                                                <? endif ?>
                                                <? $APPLICATION->ShowViewContent('left_tags'); ?>
                                            </div>
                                        </aside>
                                    </div>
                                </div>