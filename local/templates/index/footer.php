    <div class="common-container">
    <?// подключение блока о клинике ?>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/main/about.php", ['IBLOCK_ID' => 4], ["SHOW_BORDER" => false]) ?>
    <? // подключение слайдера направлений
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.section.list",
        "sections_slider",
        array(
            "ADD_SECTIONS_CHAIN" => "Y",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "COUNT_ELEMENTS" => "N",
            "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
            "FILTER_NAME" => "sectionsFilter",
            "IBLOCK_ID" => "3",
            "IBLOCK_TYPE" => "services",
            "SECTION_CODE" => "",
            "SECTION_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SECTION_ID" => "",
            "SECTION_URL" => "",
            "SECTION_USER_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SHOW_PARENT_NAME" => "Y",
            "TOP_DEPTH" => "1",
            "VIEW_MODE" => "LINE",
            "WRAP_TITLE" => "Направления деятельности",
            "COMPONENT_TEMPLATE" => "sections_slider"
        ),
        false
    );?>
    <?// подключение блока специалистов ?>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/main/experts_main.php", ['IBLOCK_ID' => 4], ["SHOW_BORDER" => false]) ?>
    <?// подключение меню пациентам ?>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/patients_menu.php", ["TEMPLATE" => "patients_main"], ["SHOW_BORDER" => false]); ?>
    <? // подключение акции
    $res = \CIBlockElement::GetList(['DATE_ACTIVE_FROM' => 'DESC'], ['IBLOCK_ID' => SITE_STOCK_IBLOCK_ID], false, ['nPageSize' => 1], ['ID', 'NAME', 'DATE_ACTIVE_FROM', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'PROPERTY_PREVIEW_PICTURE_SMALL'])->Fetch();
    $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/last_stock_form.php', ['LAST_STOCK' => $res, 'MAIN_PAGE' => 'Y'], ['SHOW_BORDER' => false]) ?>
</div>
</div><!--main-wrapper__inner end-->
</div><!--main-wrapper end-->
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/footer.php", [], ["SHOW_BORDER" => false]); ?>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/popup.php", [], ["SHOW_BORDER" => false]); ?>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/scripts.php", [], ["SHOW_BORDER" => false]); ?>
</body>
</html>