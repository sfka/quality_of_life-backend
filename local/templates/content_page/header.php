<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/head.php", [], ["SHOW_BORDER" => false]); ?>
    <? $APPLICATION->ShowHead(); ?>
</head>
<body>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/system/header.php", [], ["SHOW_BORDER" => false]); ?>

    <div class="main-wrapper">
        <div class="main-wrapper__inner">
            <div class="inner-page">
                <div class="inner-page__top">
                    <div class="inner-page__top-inner">
                        <div class="common-container">
                            <div class="row">
                                <div class="col col-3 col-xl-3 col-lg-3">
                                    <div class="return-link-wrapper">
                                        <a class="return-link" href="#" onclick="checkReffer();">
                                            <svg class="arrowRightDefault" width="20" height="15">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                            </svg>
                                            <span class="return-link__title c-white-aluminum">Вернуться назад</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="page-heading">
                        <div class="common-container">
                            <div class="row">
                                <div class="col col-12">
                                    <div class="page-heading__title">
                                        <?= $APPLICATION->ShowTitle(false) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="inner-page__main">
                    <div class="inner-page__container common-container">
                        <div class="inner-page__content">
                            <div class="row">
                                <div class="col col-8 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-xs-12 offset-2 offset-xl-2 offset-lg-0 offset-md-0 offset-sm-0 offset-xs-0">
                                    <div class="content-container">