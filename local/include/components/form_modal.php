<div class="modal__inner modal_right modal-form mfp-hide" id="<?= $arParams['FORM_ID'] ?>">
    <button class="modal__close-btn js-modal-close">
        <svg class="icon__close" width="20" height="20">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </button>
    <div class="modal__content">
        <div class="modal__content-row">
            <div class="modal__title"><?= $arParams['TITLE'] ?></div>
            <div class="modal__text">* не суммируется с другими специальными предложениями!</div>
        </div>
        <div class="modal__content-row">
            <form class="default-form" action="<?= $arParams['ACTION'] ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="stock" value="<?= $arParams['ELEMENT'] ?>">
                <input type="hidden" name="utm_source" value="<?= $_GET['utm_source'] ?>">
                <input type="hidden" name="utm_medium" value="<?= $_GET['utm_medium'] ?>">
                <input type="hidden" name="utm_campaign" value="<?= $_GET['utm_campaign'] ?>">
                <input type="hidden" name="utm_content" value="<?= $_GET['utm_content'] ?>">
                <input type="hidden" name="utm_term" value="<?= $_GET['utm_term'] ?>">
                <div class="default-form__one-column">
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="text" name="name" required>
                        <span class="default-form__input-placeholder">Ваше имя *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="tel" name="phone" required>
                        <span class="default-form__input-placeholder">Ваш телефон *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                </div>
                <div class="default-form__btn-wrapper">
                    <button class="btn btn_primary">
                        <span>Записаться</span>
                    </button>
                </div>
                <p class="default-form__text default-form__text_one-column">
                    Я даю согласие на обработку,
                    <a href='/terms-of-use/'>персональных данных</a>
                </p>
            </form>
        </div>
    </div>
</div>