<?if ($arParams['LAST_STOCK']['PREVIEW_PICTURE']) $img = CFile::ResizeImageGet($arParams['LAST_STOCK']['PREVIEW_PICTURE'], ['width' => 400, 'height' => 400], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, [])?>
<?if ($arParams['LAST_STOCK']['PROPERTY_PREVIEW_PICTURE_SMALL_VALUE']) $imgSmall = CFile::ResizeImageGet($arParams['LAST_STOCK']['PROPERTY_PREVIEW_PICTURE_SMALL_VALUE'], ['width' => 400, 'height' => 400], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, [])?>
<section class="section s-stock <?=($img ? '' : 'without-photo')?> mb-less scrl fadeInUp">
    <div class="row">
        <div class="<?=($arParams['MAIN_PAGE'] == 'Y' ? 'col col-10 col-md-12 col-sm-12 col-xs-12 offset-1 offset-md-0 offset-sm-0 offset-xs-0' : 'col col-12') ?>">
            <div class="s-stock__row">
                <?if($img): ?>
                    <img class="s-stock__img hidden-sm-down lazy" data-src="<?=$img['src'] ?>" alt="<?=$arParams['LAST_STOCK']['NAME'] ?>">
                <?endif; ?>
                <?if($imgSmall): ?>
                    <img class="s-stock__img hidden-md-up lazy" data-src="<?=$imgSmall['src'] ?>" alt="<?=$arParams['LAST_STOCK']['NAME'] ?>">
                <?endif; ?>
                <div class="s-stock__column">
                    <div class="s-stock__title"><?=$arParams['LAST_STOCK']['NAME'] ?></div>
                    <div class="s-stock__subtitle"><?=$arParams['LAST_STOCK']['PREVIEW_TEXT'] ?></div>
                    <form class="default-form" action="<?=SITE_AJAX_PATH ?>/form_stock.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="utm_source" value="<?= $_GET['utm_source'] ?>">
                        <input type="hidden" name="utm_medium" value="<?= $_GET['utm_medium'] ?>">
                        <input type="hidden" name="utm_campaign" value="<?= $_GET['utm_campaign'] ?>">
                        <input type="hidden" name="utm_content" value="<?= $_GET['utm_content'] ?>">
                        <input type="hidden" name="utm_term" value="<?= $_GET['utm_term'] ?>">
                        <input type="hidden" name="stock" value="<?= $arParams['LAST_STOCK']['ID'] ?>">
                        <div class="default-form__input-wrapper">
                            <input class="default-form__input" type="text" name="name" required>
                            <span class="default-form__input-placeholder">Ваше имя *</span>
                            <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                        </div>
                        <div class="default-form__input-wrapper">
                            <input class="default-form__input" type="tel" name="phone" required>
                            <span class="default-form__input-placeholder">Ваш телефон *</span>
                            <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                        </div>
                        <button class="btn btn_primary">
                            <span>Записаться со скидкой</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>