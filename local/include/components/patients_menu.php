<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->IncludeComponent(
    "bitrix:menu",
    $arParams['TEMPLATE'] ? $arParams['TEMPLATE'] : "",
    [
        "ROOT_MENU_TYPE" => "patients",
        "MAX_LEVEL" => "2",
        "CHILD_MENU_TYPE" => "left",
        "USE_EXT" => "N",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => [],
        "COMPONENT_TEMPLATE" => $arParams['TEMPLATE'] ? $arParams['TEMPLATE'] : ""
    ],
    false
);