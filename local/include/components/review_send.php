<section class="section mb-less scrl fadeInUp">
    <div class="info-block">
        <div class="info-block__inner">
            <div class="info-block__icon-wrapper">
                <div class="info-block__icon-bg bg-linen"></div>
                <svg class="icon__patient-reviews" width="90px" height="90px">
                    <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#patient-reviews"></use>
                </svg>

            </div>
            <div class="info-block__col">
                <div class="info-block__title">Отзывы о специалисте</div>
                <p class="info-block__description">Уделите несколько минут, чтобы оставить отзыв о лечении у специалиста.</p><a class="btn btn_primary modal-open" href="#add-review"><span>Оставить свой отзыв</span></a>
            </div>
        </div>
    </div>
</section>