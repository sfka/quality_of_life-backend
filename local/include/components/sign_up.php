<section class="section mb-less scrl fadeInUp">
    <div class="info-block">
        <div class="info-block__inner">
            <div class="info-block__icon-wrapper">
                <div class="info-block__icon-bg bg-linen"></div>
                <svg class="patient-reviews" width="90px" height="90px">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#patient-reviews"></use>
                </svg>
            </div>
            <div class="info-block__col">
                <div class="info-block__title"><?= $arParams['BLOCK_TITLE'] ?></div>
                <p class="info-block__description mb-less">Также возможно получить консультацию по телефону</p>
                <ul class="info-block__list">
                    <li class="info-block__list-item">
                        <a class="info-block__list-link"
                           href="tel:<?= preg_replace('~\D+~', '', \Bitrix\Main\Config\Option::get("askaron.settings", "UF_CALL_CENTER")) ?>">
                            <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_CALL_CENTER"); ?>
                        </a>
                    </li>
                    <li class="info-block__list-item">
                        <a class="info-block__list-link"
                           href="https://wa.me/<?= preg_replace('~\D+~', '', \Bitrix\Main\Config\Option::get("askaron.settings", "UF_WHATSAPP")) ?>"
                           target="_blank">
                            <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_WHATSAPP"); ?>
                        </a>
                    </li>
                </ul>
                <a class="arrow-link modal-open" href="#<?=($arParams['FORM_ID'] ? $arParams['FORM_ID'] : 'stock') ?>">
                    <span class="arrow-link__title"><?= $arParams['BTN_TITLE'] ?></span>
                </a>
            </div>
        </div>
    </div>
</section>