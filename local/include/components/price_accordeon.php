<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if ($arParams['PRICE_LIST_ARRAY']): ?>
    <section
            class="section mb-less section-price__acordeon scrl fadeInUp" <?= ($arParams['BLOCK_ID'] ? 'id="' . $arParams['BLOCK_ID'] . '"' : false) ?>>
        <div class="row section__top-row">
            <div class="col col-12">
                <div class="section__top-row-inner">
                    <div class="section-title"><?= ($arParams['TITLE'] ? $arParams['TITLE'] : 'Прейскурант') ?></div>
                </div>
                <? if (count($arParams['PRICE_LIST_ARRAY']) > 1): ?>
                    <div class="section__top-row-inner">
                        <div class="category-items">
                            <? foreach ($arParams['PRICE_LIST_ARRAY'] as $arSubSection): ?>
                                <? if (count($arSubSection['ELEMENTS']) > 0): ?>
                                    <div class="category-items__item <?= ($arParams['DOP_FILTER'] == $arSubSection['ID'] ? 'active' : false) ?>"
                                         data-filter-slider="<?= $arSubSection['ID'] ?>">
                                        <span><?= $arSubSection['NAME'] ?></span>
                                    </div>
                                <? endif; ?>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col col-12">
                <div class="catalog-item">
                    <? foreach ($arParams['PRICE_LIST_ARRAY'] as $arSubSection): ?>
                        <? foreach ($arSubSection['ELEMENTS'] as $arItem): ?>
                            <? if (!empty($arParams['DOP_FILTER']) && $arParams['DOP_FILTER'] != $arSubSection['ID']) {
                                continue;
                            } ?>
                            <div class="catalog-item__row <?= ($arItem['~ID'] == $arParams['ELEMENT_ACTIVE_ID'] ? 'active' : false) ?>">
                                <div class="catalog-item__row-inner">
                                    <div class="catalog-item__title"><?= $arItem['NAME'] ?></div>
                                    <svg class="arrowRightDefault" width="20" height="15">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                                    </svg>
                                </div>
                                <div class="catalog-item__hidden">
                                    <div class="catalog-item__text-item hidden-xs-down">
                                        <p class="catalog-item__text">Узнать подробнее про услугу:</p>
                                        <a class="arrow-link" href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
                                            <span class="arrow-link__title"><?= $arItem['NAME'] ?></span>
                                        </a>
                                    </div>
                                    <div class="catalog-item__text-item hidden-sm-up">
                                        <p class="catalog-item__text">Узнать про услугу:</p>
                                        <a class="arrow-link" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                            <span class="arrow-link__title">Подробнее</span>
                                        </a>
                                    </div>
                                    <div class="catalog-item__list">
                                        <? foreach ($arItem['PRICE_LIST'] as $arPrice): ?>
                                            <div class="catalog-item__list-item">
                                                <div class="catalog-item__list-title"><?= $arPrice['NAME'] ?></div>
                                                <div class="catalog-item__price-wrapper">
                                                    <? if ($arPrice['PROPERTY_OLD_PRICE_VALUE']): ?>
                                                        <div class="catalog-item__price catalog-item__price_old"><?= number_format($arPrice['PROPERTY_OLD_PRICE_VALUE'], 0, '.', ' ') . '₽';  ?></div>
                                                        <div class="catalog-item__price catalog-item__price_new"><?= number_format($arPrice['PROPERTY_PRICE_VALUE'], 0, '.', ' ') . '₽';  ?></div>
                                                    <? else: ?>
                                                        <div class="catalog-item__price"><?= number_format($arPrice['PROPERTY_PRICE_VALUE'], 0, '.', ' ') . '₽';  ?></div>
                                                    <? endif; ?>
                                                </div>
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<? endif; ?>