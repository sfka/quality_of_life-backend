<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use IL\Catalog;
use IL\Settings;

$arSolutions = Catalog::getSectionList(Settings::PROBLEMS_SOLUTIONS_IBLOCK_ID);

?>
<? if (count($arSolutions) > 0): ?>
    <section class="section mb-less">
        <div class="row section__top-row">
            <div class="col col-12">
                <div class="section__top-row-inner scrl fadeInUp">
                    <div class="section-title">Диагностика и лечение</div>
                </div>
            </div>
        </div>
        <div class="row scrl fadeInUp">
            <div class="col col-12">
                <div class="masonry-grid">
                    <? foreach ($arSolutions as $arSolution): ?>
                        <div class="solution-card masonry-item">
                            <div class="solution-card__inner">
                                <? if (!empty($arSolution['PICTURE'])): ?>
                                    <img class="solution-card__img" src="<?= CFile::GetPath($arSolution['PICTURE']) ?>" alt="<?= $arSolution['NAME'] ?>"/>
                                <? endif ?>
                                <div class="solution-card__title"><?= $arSolution['NAME'] ?></div>
                                <? if (!empty($arSolution['ITEMS'])): ?>
                                    <ul class="solution-card__list">
                                        <? foreach ($arSolution['ITEMS'] as $arItem): ?>
                                            <li class="solution-card__list-item">
                                                <a class="solution-card__list-link" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                                    <?= $arItem['NAME'] ?>
                                                </a>
                                            </li>
                                        <? endforeach ?>
                                    </ul>
                                <? endif ?>
                            </div>
                        </div>
                    <? endforeach ?>
                </div>
            </div>
        </div>
    </section>
<? endif ?>