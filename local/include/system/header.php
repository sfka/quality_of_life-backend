<header class="header">
    <div class="header__inner">
        <div class="header__top-col">
            <a class="header__logo" href="/">
                <svg class="logo" width="76" height="31">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#logo"></use>
                </svg>
            </a>
            <p class="header__descr">
                <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_SITE_TITLE"); ?>
            </p>
        </div>
        <div class="header__middle-col">
            <button class="menu-btn">
                <div class="menu-btn__lines">
                    <div class="menu-btn__line"></div>
                    <div class="menu-btn__line"></div>
                </div>
                <div class="menu-btn__label">
                    <span class="menu-btn__label-value close">Меню</span>
                    <span class="menu-btn__label-value open">Закрыть</span>
                </div>
            </button>
        </div>
        <div class="header__bottom-col">
            <? if (!CSite::InDir('/appointments/')): ?>
                <a class="btn btn_light" href="/appointments/">
                    <span>Запись на прием</span>
                    <svg class="icon__arrowRightDefault" width="18" height="12">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightDefault"></use>
                    </svg>
                </a>
            <? endif ?>
            <div class="header__contacts-info">
                <a class="header__contacts-info-link" href="tel:<?= preg_replace('~\D+~', '', \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE")) ?>">
                    <svg class="icon__call" width="24" height="24">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#call"></use>
                    </svg>
                    <span>
                        <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE"); ?>
                    </span>
                </a>
                <?/*<a class="header__contacts-info-link" href="mailto:<?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_EMAIL"); ?>">
                    <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_EMAIL"); ?>
                </a>*/?>
            </div>
            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/social.php", [], ["SHOW_BORDER" => false]); ?>
        </div>
    </div>
</header>

<div class="megamenu-wrapper" data-scroll-lock-scrollable>
    <div class="megamenu-wrapper__inner">
        <div class="megamenu-wrapper__bg-col left"></div>
        <div class="megamenu-wrapper__bg-col right"></div>
        <div class="common-container">
            <div class="megamenu-wrapper__row">
                <div class="megamenu-wrapper__col left">
                    <div class="accordion-item">
                        <div class="accordion-item__btn">
                            <div class="accordion-item__btn-label">Направления деятельности</div>
                            <div class="accordion-item__btn-icon">
                                <svg class="arrowRightUnique" width="6" height="15">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightUnique"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="accordion-item__content">
                            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/top_menu.php", ["TEMPLATE" => "header"], ["SHOW_BORDER" => false]); ?>
                        </div>
                    </div>
                </div>
                <div class="megamenu-wrapper__col right">
                    <div class="accordion-item">
                        <div class="accordion-item__btn">
                            <div class="accordion-item__btn-label">Пациентам</div>
                            <div class="accordion-item__btn-icon">
                                <svg class="arrowRightUnique" width="6" height="15">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrowRightUnique"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="accordion-item__content">
                            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/patients_menu.php", ["TEMPLATE" => "patients_top"], ["SHOW_BORDER" => false]); ?>
                        </div>
                    </div>
                    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/bottom_menu.php", ["TEMPLATE" => "top"], ["SHOW_BORDER" => false]); ?>
                    <div class="megamenu-wrapper__mobile-block">
                        <p class="megamenu-wrapper__descr">
                            <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_SITE_TITLE"); ?>
                        </p>
                        <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/social.php", [], ["SHOW_BORDER" => false]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>