<footer class="footer">
    <div class="footer__grid-row">
        <div class="common-container">
            <div class="row hidden-sm-down">
                <div class="col col-12">
                    <div class="footer__heading-row">
                        <div class="row">
                            <div class="col col-3 offset-3">
                                <p class="footer__text">Направления деятельности</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row footer__top-row">
                <div class="col col-3 col-md-3 col-sm-12 footer__info-col">
                    <a class="footer__logo" href="/">
                        <svg class="logo" width="116" height="59">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#logo"></use>
                        </svg>
                    </a>
                    <div class="hidden-md-down">
                        <p class="footer__text footer__text_important">
                            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/footer/contraindications.php", [], ["MODE" => "text"]); ?>
                        </p>
                        <a class="footer__link footer__link_unique" href="/terms-of-use/">
                            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/footer/terms-of-use.php", [], ["MODE" => "text"]); ?>
                        </a>
                    </div>
                </div>
                <div class="col col-9 col-md-9 hidden-sm-down">
                    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/top_menu.php", ["TEMPLATE" => "footer"], ["SHOW_BORDER" => false]); ?>
                </div>
            </div>
            <div class="row hidden-md-up">
                <div class="col col-12">
                    <div class="accordion-items">
                        <div class="accordion-item">
                            <div class="accordion-item__btn">
                                <div class="accordion-item__btn-label">Направления деятельности</div>
                                <div class="accordion-item__btn-icon"></div>
                            </div>
                            <div class="accordion-item__content">
                                <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/top_menu.php", ["TEMPLATE" => "footer"], ["SHOW_BORDER" => false]); ?>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-item__btn">
                                <div class="accordion-item__btn-label">Пациентам</div>
                                <div class="accordion-item__btn-icon"></div>
                            </div>
                            <div class="accordion-item__content">
                                <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/patients_menu.php", ["TEMPLATE" => "patients_bottom"], ["SHOW_BORDER" => false]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hidden-sm-down">
                <div class="col col-12">
                    <div class="footer__heading-row">
                        <p class="footer__text">Пациентам</p>
                    </div>
                </div>
            </div>
            <div class="row footer__main-row">
                <div class="col col-6 col-md-12 hidden-md-down">
                    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/patients_menu.php", ["TEMPLATE" => "patients_bottom"], ["SHOW_BORDER" => false]); ?>
                </div>
                <div class="col col-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer__unique-menu-wrapper">
                        <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/bottom_menu.php", ["TEMPLATE" => "bottom"], ["SHOW_BORDER" => false]); ?>
                    </div>
                </div>
            </div>
            <div class="row hidden-md-up">
                <div class="col col-12">
                    <div class="footer__mobile-text-col">
                        <p class="footer__text footer__text_important">
                            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/footer/contraindications.php", [], ["MODE" => "text"]); ?>
                        </p>
                        <a class="footer__link footer__link_unique" href="/terms-of-use/">
                            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/footer/terms-of-use.php", [], ["MODE" => "text"]); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__grid-row">
        <div class="common-container">
            <div class="row footer__bottom-row">
                <div class="col col-3 col-md-6 col-sm-6 col-xs-12 col-xs-6 pt-0 pb-0">
                    <div class="footer__unique-col">
                        <p class="footer__text footer__text_copyright">
                            © <?= date('Y') ?>
                            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/footer/copy.php", [], ["MODE" => "text"]); ?>
                        </p>
                    </div>
                </div>
                <div class="col col-3 col-md-6 col-sm-6 col-xs-12 offset-6 offset-xl-6 offset-lg-6 offset-md-0 offset-sm-0 offset-xs-0">
                    <p class="footer__text">
                        <a class="footer__link" href="https://ilartech.com/" target='_blank'>ILAR Technology</a>
                        – разработка сайтов
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>