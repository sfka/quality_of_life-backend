<?

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);
/*META*/
Asset::getInstance()->addString('<meta charset="UTF-8">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<meta http-equiv="X-UA-Compatible" content="IE=edge">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<meta name="format-detection" content="telephone=no">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<meta name="HandheldFriendly" content="true">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<meta name="theme-color" content="#000">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<meta name="msapplication-navbutton-color" content="#000">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<meta name="apple-mobile-web-app-status-bar-style" content="#000">', true, 'BEFORE_CSS');
/*FAVICON*/
Asset::getInstance()->addString('<link rel="icon" type="image/x-icon" href="/favicon.ico">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<link rel="shortcut icon" type="image/x-icon" href="/favicon/favicon.ico">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">', true, 'BEFORE_CSS');
Asset::getInstance()->addString('<link rel="manifest" href="/site.webmanifest">', true, 'BEFORE_CSS');
/*CSS*/
Asset::getInstance()->addCss(SITE_STYLE_PATH . "/css/main.min.css");
Asset::getInstance()->addCss(SITE_STYLE_PATH . "/css/backend.css", true);
/*JS*/
Asset::getInstance()->addJs(SITE_STYLE_PATH . "/js/separate-js/jquery.min.js");
Asset::getInstance()->addJs(SITE_STYLE_PATH . "/js/separate-js/ElementQueries.min.js");
Asset::getInstance()->addJs(SITE_STYLE_PATH . "/js/separate-js/ResizeSensor.min.js");
if (CSite::InDir('/index.php')) {
    Asset::getInstance()->addJs(SITE_STYLE_PATH . "/js/index.min.js");
} else {
    Asset::getInstance()->addJs(SITE_STYLE_PATH . "/js/main.min.js");
}
Asset::getInstance()->addJs(SITE_STYLE_PATH . "/js/backend.js");
?>
<style>
    .mfp-hide {
        display: none;
    }
    .preloader,
    .loader-wrapper_full {
        display: none;
    }
</style>
<script data-skip-moving="true">
    let timerStart = Date.now();
</script>
<title><? $APPLICATION->ShowTitle() ?></title>
<div class="preloader">
    <img class="preloader__img" src="<?= SITE_STYLE_PATH ?>/img/content/preloader.gif" alt="preloader">
</div>