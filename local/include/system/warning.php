<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="important-info">
    <div class="important-info__inner">
        <div class="important-info__title">
            Вниманию пациентов из других стран!
        </div>
        <p class="important-info__description">
            Весь предоперационный комплекс анализов должен быть сдан в российском медицинском учреждении.
        </p>
    </div>
</div>