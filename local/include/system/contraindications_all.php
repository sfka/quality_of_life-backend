<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $res = \CIBlockElement::GetList([],['IBLOCK_ID' => SITE_SERVICE_IBLOCK_ID, 'ID' => $arParams['ELEMENT_ID']], false, ['nPageSize' => 1], ['ID', 'NAME', 'PROPERTY_CONTRAINDICATIONS'])->Fetch();
if($res['PROPERTY_CONTRAINDICATIONS_VALUE']): ?>

<section class="section mb-less scrl fadeInUp">
    <div class="row">
        <div class="col col-12">
            <div class="read-more-text item">
                <div class="read-more-text__title">Противопоказания</div>
                <div class="item__text-wrapper">
                    <p class="question-item__question item__text c-gray-umber">
                        <?= $res['PROPERTY_CONTRAINDICATIONS_VALUE']['TEXT'] ?>
                    </p>
                    <span class="question-item__show-all-text item__show-all-text c-carmine-pink">
                        <span>Показать полностью</span>
                        <span>Свернуть</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>
<? endif; ?>