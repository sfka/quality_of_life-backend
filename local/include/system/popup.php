<? // Задать вопрос ?>
<div class="modal__inner modal_right modal-form modal-form_ask-question mfp-hide" id="ask-question">
    <button class="modal__close-btn js-modal-close">
        <svg class="icon__close" width="20" height="20">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </button>
    <div class="modal__content">
        <div class="modal__content-row">
            <div class="modal__title">Задать вопрос</div>
            <div class="modal__text">Уважаемые пациенты, к сожалению, мы не можем заочно в полной мере объективно ответить на все ваши вопросы, поэтому наши ответы носят исключительно информационно–разъяснительный и рекомендательный характер.</div>
        </div>
        <div class="modal__content-row">
            <form class="default-form" action="<?= SITE_AJAX_PATH ?>/form_questions.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="utm_source" value="<?= $_GET['utm_source'] ?>">
                <input type="hidden" name="utm_medium" value="<?= $_GET['utm_medium'] ?>">
                <input type="hidden" name="utm_campaign" value="<?= $_GET['utm_campaign'] ?>">
                <input type="hidden" name="utm_content" value="<?= $_GET['utm_content'] ?>">
                <input type="hidden" name="utm_term" value="<?= $_GET['utm_term'] ?>">
                <div class="default-form__two-column">
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="text" name="name" required>
                        <span class="default-form__input-placeholder">Ваше имя *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="tel" name="phone" required>
                        <span class="default-form__input-placeholder">Ваш телефон *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                </div>
                <div class="default-form__input-wrapper">
                    <textarea class="default-form__input" name="question" required></textarea>
                    <span class="default-form__input-placeholder">Задать вопрос *</span>
                    <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                        <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                    </svg>
                </div>
                <div class="default-form__btn-wrapper">
                    <button class="btn btn_primary">
                        <span>Отправить вопрос</span>
                    </button>
                </div>
                <p class="default-form__text default-form__text_one-column">Я даю согласие на обработку,
                    <a href='/terms-of-use/'>персональных данных</a>
                </p>
            </form>
        </div>
    </div>
</div>
<? // Оставить отзыв ?>
<div class="modal__inner modal_right modal-form modal-form_add-review mfp-hide" id="add-review">
    <button class="modal__close-btn js-modal-close">
        <svg class="icon__close" width="20" height="20">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </button>
    <div class="modal__content">
        <div class="modal__content-row">
            <div class="modal__title">Оставить отзыв</div>
            <div class="modal__text">Важно! Отзывы пациентов публикуются на сайте вместе с приложенными фото! Если вы не хотите, чтобы ваши фото были доступны, обязательно информируйте нас об этом в тексте письма!</div>
        </div>
        <div class="modal__content-row">
            <form class="default-form feedback-form" action="<?= SITE_AJAX_PATH ?>/form_reviews.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="utm_source" value="<?= $_GET['utm_source'] ?>">
                <input type="hidden" name="utm_medium" value="<?= $_GET['utm_medium'] ?>">
                <input type="hidden" name="utm_campaign" value="<?= $_GET['utm_campaign'] ?>">
                <input type="hidden" name="utm_content" value="<?= $_GET['utm_content'] ?>">
                <input type="hidden" name="utm_term" value="<?= $_GET['utm_term'] ?>">
                <div class="default-form__two-column">
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="text" name="name" required>
                        <span class="default-form__input-placeholder">Ваше имя и фамилия *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="tel" name="phone" required>
                        <span class="default-form__input-placeholder">Ваш телефон *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                    <div class="default-form__input-wrapper datepicker-wrapper">
                        <input class="datepicker default-form__input" type="text" name="date">
                        <span class="default-form__input-placeholder">Дата посещения</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                </div>
                <div class="default-form__input-wrapper">
                    <textarea class="default-form__input" name="review" required></textarea>
                    <span class="default-form__input-placeholder">Ваш отзыв *</span>
                    <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                        <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                    </svg>
                </div>
                <div class="file-wrapper">
                    <div class="file-wrapper__grid">
                        <label class="file-wrapper__label">
                            <input type="file" name="photos[]" multiple>Добавить фотографии
                        </label>
                        <p class="file-wrapper__text">Размер одной фотографии должен не превышать 2 Мб<? //= ini_get('upload_max_filesize') ?></p>
                    </div>
                    <? $arTypeFile = \CIBlockProperty::GetByID(5, \IL\Settings::REVIEWS_IBLOCK_ID)->Fetch(); ?>
                    <input type="hidden" name="type_file" value="<?= $arTypeFile['FILE_TYPE'] ?>">
                    <div class="file-wrapper__files"></div>
                </div>
                <div class="default-form__btn-wrapper">
                    <button class="btn btn_primary">
                        <span>Отправить отзыв</span>
                    </button>
                </div>
                <p class="default-form__text default-form__text_one-column">Я даю согласие на обработку,
                    <a href='/terms-of-use/'>персональных данных</a>
                </p>
            </form>
        </div>
    </div>
</div>
<? // Подпишитесь на рассылку ?>
<div class="modal__inner modal_right modal-form mfp-hide" id="mailing">
    <button class="modal__close-btn js-modal-close">
        <svg class="icon__close" width="20" height="20">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </button>
    <div class="modal__content">
        <div class="modal__content-row">
            <div class="modal__title">Подпишитесь на рассылку</div>
            <div class="modal__text">Только самые полезные и важные статьи о здоровье и доказательной медицине</div>
        </div>
        <div class="modal__content-row">
            <form class="default-form" action="<?= SITE_AJAX_PATH ?>/form_mailing.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="utm_source" value="<?= $_GET['utm_source'] ?>">
                <input type="hidden" name="utm_medium" value="<?= $_GET['utm_medium'] ?>">
                <input type="hidden" name="utm_campaign" value="<?= $_GET['utm_campaign'] ?>">
                <input type="hidden" name="utm_content" value="<?= $_GET['utm_content'] ?>">
                <input type="hidden" name="utm_term" value="<?= $_GET['utm_term'] ?>">
                <div class="default-form__one-column">
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="email" name="email" required>
                        <span class="default-form__input-placeholder">Ваш e–mail *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                </div>
                <div class="default-form__btn-wrapper">
                    <button class="btn btn_primary">
                        <span>Отправить</span>
                    </button>
                </div>
                <p class="default-form__text default-form__text_one-column">Я даю согласие на обработку,
                    <a href='/terms-of-use/'>персональных данных</a>
                </p>
            </form>
        </div>
    </div>
</div>
<? // Откликнуться ?>
<div class="modal__inner modal_right modal-form mfp-hide" id="vacancy-respond">
    <button class="modal__close-btn js-modal-close">
        <svg class="icon__close" width="20" height="20">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </button>
    <div class="modal__content">
        <div class="modal__content-row">
            <div class="modal__title">Откликнуться</div>
            <div class="modal__text">Важно! Рассмотрение отклика по вакансии более 3 дней.</div>
        </div>
        <div class="modal__content-row">
            <form class="feedback-form default-form" action="<?= SITE_AJAX_PATH ?>/form_vacancy.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="utm_source" value="<?= $_GET['utm_source'] ?>">
                <input type="hidden" name="utm_medium" value="<?= $_GET['utm_medium'] ?>">
                <input type="hidden" name="utm_campaign" value="<?= $_GET['utm_campaign'] ?>">
                <input type="hidden" name="utm_content" value="<?= $_GET['utm_content'] ?>">
                <input type="hidden" name="utm_term" value="<?= $_GET['utm_term'] ?>">
                <div class="default-form__two-column">
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="text" name="name" required>
                        <span class="default-form__input-placeholder">Ваше имя *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                    <div class="nice-select-wrapper">
                        <div class="nice-select-wrapper__placeholder">Ваша должность</div>
                        <select class="nice-select" name="vacancy" required>
                            <? $res = CIBlockElement::GetList(['ID' => 'DESC'], [
                                'IBLOCK_ID' => SITE_VACANCIES_IBLOCK_ID,
                                'ACTIVE' => 'Y',
                            ], false, false, [
                                'ID',
                                'NAME',
                            ]); ?>
                            <? while ($ob = $res->GetNextElement()): ?>
                                <? $arFields = $ob->GetFields() ?>
                                <option value="<?= $arFields['NAME'] ?>"><?= $arFields['NAME'] ?></option>
                            <? endwhile; ?>
                        </select>
                    </div>
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="tel" name="phone" required>
                        <span class="default-form__input-placeholder">Ваш телефон *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                </div>
                <div class="file-wrapper">
                    <div class="file-wrapper__grid">
                        <label class="file-wrapper__label">
                            <input type="file" name="resume">Прикрепить резюме
                        </label>
                        <p class="file-wrapper__text">Размер не должен превышать 2 Мб</p>
                    </div>
                    <? $arTypeFile = \CIBlockProperty::GetByID(93, \IL\Settings::VACANCY_FORM_ID)->Fetch(); ?>
                    <input type="hidden" name="type_file" value="<?= $arTypeFile['FILE_TYPE'] ?>">
                    <div class="file-wrapper__files"></div>
                </div>
                <div class="default-form__btn-wrapper">
                    <button class="btn btn_primary">
                        <span>Отправить</span>
                    </button>
                </div>
                <p class="default-form__text default-form__text_one-column">Я даю согласие на обработку,
                    <a href='/terms-of-use/'>персональных данных</a>
                </p>
            </form>
        </div>
    </div>
</div>
<? // Обратный звонок ?>
<div class="modal__inner modal_right modal-form mfp-hide" id="callback">
    <button class="modal__close-btn js-modal-close">
        <svg class="icon__close" width="20" height="20">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </button>
    <div class="modal__content">
        <div class="modal__content-row">
            <div class="modal__title">Обратный звонок</div>
            <div class="modal__text">Администратор клиники свяжется с вами в ближайшее время</div>
        </div>
        <div class="modal__content-row">
            <form class="default-form" action="<?= SITE_AJAX_PATH ?>/form_callback.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="utm_source" value="<?= $_GET['utm_source'] ?>">
                <input type="hidden" name="utm_medium" value="<?= $_GET['utm_medium'] ?>">
                <input type="hidden" name="utm_campaign" value="<?= $_GET['utm_campaign'] ?>">
                <input type="hidden" name="utm_content" value="<?= $_GET['utm_content'] ?>">
                <input type="hidden" name="utm_term" value="<?= $_GET['utm_term'] ?>">
                <div class="default-form__one-column">
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="text" name="name" required>
                        <span class="default-form__input-placeholder">Ваше имя *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                    <div class="default-form__input-wrapper">
                        <input class="default-form__input" type="tel" name="phone" required>
                        <span class="default-form__input-placeholder">Ваш телефон *</span>
                        <svg width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                        </svg>
                    </div>
                </div>
                <div class="default-form__btn-wrapper">
                    <button class="btn btn_primary">
                        <span>Отправить заявку</span>
                    </button>
                </div>
                <p class="default-form__text default-form__text_one-column">Я даю согласие на обработку,
                    <a href='/terms-of-use/'>персональных данных</a>
                </p>
            </form>
        </div>
    </div>
</div>
<? // Спасибо! ?>
<div class="modal__inner modal_right mfp-hide modal_success" id="popup-thx">
    <button class="modal__close-btn js-modal-close">
        <svg class="icon__close" width="20" height="20">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </button>
    <div class="modal__content">
        <div class="modal__content-row">
            <div class="modal__title"></div>
        </div>
        <div class="modal__content-row modal__content-row_unique"></div>
        <div class="modal__content-row">
            <div class="modal__subtitle">По срочным вопросам можно позвонить, написать и присоединиться к нам</div>
            <div class="header__contacts-info">
                <a class="header__contacts-info-link" href="tel:<?= preg_replace('~\D+~', '', \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE")) ?>">
                    <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE"); ?>
                </a>
                <a class="header__contacts-info-link" href="mailto:<?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_EMAIL"); ?>">
                    <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_EMAIL"); ?>
                </a>
            </div>
            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/social.php", [], ["SHOW_BORDER" => false]); ?>
        </div>
    </div>
</div>
<? // Ошибка! ?>
<div class="modal__inner modal_right mfp-hide modal_success" id="popup-error">
    <button class="modal__close-btn js-modal-close">
        <svg class="icon__close" width="20" height="20">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </button>
    <div class="modal__content">
        <div class="modal__content-row">
            <div class="modal__title">Ошибка!</div>
        </div>
        <div class="modal__content-row modal__content-row_unique"></div>
        <div class="modal__content-row">
            <div class="modal__subtitle">По срочным вопросам можно позвонить, написать и присоединиться к нам</div>
            <div class="header__contacts-info">
                <a class="header__contacts-info-link" href="tel:<?= preg_replace('~\D+~', '', \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE")) ?>">
                    <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE"); ?>
                </a>
                <a class="header__contacts-info-link" href="mailto:<?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_EMAIL"); ?>">
                    <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_EMAIL"); ?>
                </a>
            </div>
            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . "/components/social.php", [], ["SHOW_BORDER" => false]); ?>
        </div>
    </div>
</div>