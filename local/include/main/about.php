<section class="section s-about">
    <div class="row">
        <div class="col col-6 col-xs-12 scrl fadeInUp">
            <div class="s-about__img-wrapper">
                <? $img = CFile::ResizeImageGet( \Bitrix\Main\Config\Option::get("askaron.settings", "UF_CLINIC_IMG"), ['width' => 600, 'height' => 600], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, []) ?>
                <img class="s-about__img s-about__background lazy" data-src="<?=$img['src'] ?>" alt="">
                <img class="s-about__img s-about__foreground lazy" data-src="<?=$img['src'] ?>" alt="">
            </div>
        </div>
        <div class="col col-6 col-xs-12">
            <div class="section__col scrl fadeInUp">
                <div class="s-about__logo ml-auto">
                    <svg class="logo-path" width="120" height="92">
                        <use xlink:href="<?=SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#logo-path"></use>
                    </svg>

                </div>
                <h3 class="section__subtitle">Коротко о клинике</h3>
                <div class="section__paragraph-wrapper">
                    <p class="c-white-aluminum">Качество Жизни — клиника антивозрастной, эстетической и регенеративной медицины европейского уровня в Казани.</p>
                    <p class="c-white-aluminum">Сохраняйте и возвращайте здоровье и красоту с помощью современных методик и медицинских технологий.</p>
                </div><a class="arrow-link" href="/about/"><span class="arrow-link__title">Подробнее</span></a>
            </div>
        </div>
    </div>
</section>