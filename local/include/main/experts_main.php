<? CModule::IncludeModule("iblock");
$arFilter = ["IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y"];
$res_count = CIBlockElement::GetList([], $arFilter, [], false, []);
?>
<section class="section s-specialists scrl fadeInUp">
    <div class="s-specialists__img-wrapper">
        <div class="row">
            <div class="col col-10 col-sm-12 col-xs-12 offset-1 offset-sm-0 offset-xs-0 p-0">
                <img class="lazy" data-src="<?= CFile::GetPath(\Bitrix\Main\Config\Option::get( "askaron.settings", "UF_MAIN_EXPERTS_IMAGE"));?>" alt="" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-10 col-sm-12 col-xs-12 offset-1 offset-sm-0 offset-xs-0">
            <div class="row">
                <div class="s-specialists__col s-specialists__col_left col col-6 col-xs-12">
                    <div class="section-title scrl fadeInUp"><?=$res_count . ' ' . \IL\Utilities::getWord($res_count, ['специалист', 'специалиста', 'специалистов']) ?></div>
                    <p class="small-text scrl fadeInUp">Мы ценим каждого сотрудника</p>
                </div>
                <div class="s-specialists__col s-specialists__col_right col col-6 col-xs-12 scrl fadeInUp">
                    <h3 class="section__subtitle scrl fadeInUp">Все дело в людях</h3>
                    <div class="section__paragraph-wrapper scrl fadeInUp">
                        <p class="c-white-aluminum">Вы можете быть уверены, что доверяете своё здоровье и здоровье будущего в опытные и надежные руки</p>
                    </div>
                    <div class="scrl fadeInUp"><a class="arrow-link" href="/experts/"><span class="arrow-link__title">Выбрать врача</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
