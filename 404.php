<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
global $APPLICATION;
$APPLICATION->RestartBuffer();
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");
@define("404", true);
require($_SERVER["DOCUMENT_ROOT"] . "/local/templates/404/header.php");
$APPLICATION->SetTitle("404 Ошибка");
?>
    <div class="nfp__title c-gray-umber">Страница не найдена</div>
    <div class="nfp__description c-gray-umber">Результаты отображения страницы отрицательные</div>
    <a class="arrow-link" href="<?= SITE_DIR ?>">
        <span class="arrow-link__title">Вернуться на главную</span>
    </a>
    <svg class="nfp__error-code" width="284" height="770">
        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#404"></use>
    </svg>
<? require($_SERVER["DOCUMENT_ROOT"] . "/local/templates/404/footer.php"); ?>
<? die(); ?>