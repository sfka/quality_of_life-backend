<?
$aMenuLinks = [
    [
        "Отзывы",
        "/patients/reviews/",
        [],
        ["TITLE" => "Отзывы пациентов", "DESCRIPTION" => "Получить ответы на часто задаваемые вопросы", "ICO" => "icon__patient-reviews"],
        "",
    ],
    [
        "До и после",
        "/patients/before-and-after/",
        [],
        ["TITLE" => "До и после", "DESCRIPTION" => "Получить ответы на часто задаваемые вопросы", "ICO" => "icon__before-after"],
        "",
    ],
    [
        "Вопросы и ответы",
        "/patients/questions-and-answers/",
        [],
        ["TITLE" => "Вопросы и ответы", "DESCRIPTION" => "Получить ответы на часто задаваемые вопросы", "ICO" => "icon__question-answer"],
        "",
    ],
    [
        "Подготовка к процедурам",
        "/patients/preparation-for-procedures/",
        [],
        ["TITLE" => "Подготовка к процедурам", "DESCRIPTION" => "Получить ответы на часто задаваемые вопросы", "ICO" => "icon__preparation-for-procedures"],
        "",
    ],
];