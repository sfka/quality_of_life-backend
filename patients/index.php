<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);
define('NOT_CHECK_PERMISSIONS', true);
LocalRedirect("/", 301);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");